$(document).ready(function(){  
    $('.modal').appendTo("body")

    if ($(".select2").length > 0){
    	$(".select2").select2();
	}

	if ($("#services_tbl").length > 0){
    	$('#services_tbl').dataTable();
    }

    $('#form-add').submit(function(e){
            e.preventDefault();


            $.ajax( {
                type: "POST",
                url: $(this).attr( 'action' ),
                data: $(this).serialize(),
                success: function( response ) {
                    if (response.data.html=='saved'){
                        swal({
                            title: "Saved!",
                            text: response.data.msg,
                            type: "success"
                        });
                        setInterval(function() {
                            window.location.href=response.data.redirectTo;
                        }, 1000);
                        
                        
                    }
                    if (response.data.html=='failed'){
                        swal({
                            title: "Failed saving Data!",
                            text: response.data.errormsg,
                            type: "error"
                        });
                       

                    }
                 }
            });

            //redirect
            
        })
});