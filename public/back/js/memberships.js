$(document).ready(function(){  
    $('.modal').appendTo("body")

    /*$('#booking_date').datepicker({ });*/

    if ($('#mems_tbl').length > 0){
    	$('#mems_tbl').dataTable();
	}


   var adjustment;
  	if ($("ul.membershipItems").length > 0 ){

  		$( "#shop_services_list li" ).draggable({
	      appendTo: "body",
	      helper: "clone"
	    });
	    $( "#final_output_list" ).droppable({
	      activeClass: "ui-state-default",
	      hoverClass: "ui-state-hover",
	      accept: ":not(.ui-sortable-helper)",
	      drop: function( event, ui ) {
	        $( this ).find( ".placeholder" ).remove();
	        /*$( "<li></li>" ).text( ui.draggable.text() ).appendTo( this );*/
	        var jsonString = ui.draggable.attr('data-id');
		    submitList(jsonString);
	      }
	    }).sortable({
	      items: "li:not(.placeholder)",
	      sort: function() {
	        // gets added unintentionally by droppable interacting with sortable
	        // using connectWithSortable fixes this, but doesn't allow you to customize active/hoverClass options
	        $( this ).removeClass( "ui-state-default" );
	      }
	    });

		/*var group = $(".membershipItems").sortable({
		  group: 'membershipItems',
		  pullPlaceholder: false,
		  drop :false,
		  // animation on drop
		  onDrop: function  ($item, container, _super) {*/
		  	
		    /*var $clonedItem = $('<li/>').css({height: 0});
		    $clonedItem.detach();*/
		    /*$item.before($clonedItem);
		    $clonedItem.animate({'height': $item.height()});

		    $item.animate($clonedItem.position(), function  () {
		      $clonedItem.detach();
		      _super($item, container);
		    });*/
		   
      		
      		/*var jsonString = JSON.stringify($item.attr('data-id'), null);*/

		   /* $('#serialize_output').text(jsonString);*/
		    /*_super($item, container);*/

		   /* submitList(jsonString);*/
		 /* },

		  // set $item relative to cursor position
		  onDragStart: function ($item, container, _super) {
		    var offset = $item.offset(),
		        pointer = container.rootGroup.pointer;

		    adjustment = {
		      left: pointer.left - offset.left,
		      top: pointer.top - offset.top
		    };

		    _super($item, container);
		  },
		  onDrag: function ($item, position) {
		    $item.css({
		      left: position.left - adjustment.left,
		      top: position.top - adjustment.top
		    });
		  }
		});*/
	} /*endif*/

	/*on page load, pull services via ajax*/
	submitList(0);



	function submitList(jsonString)
	{
		var membership_id = $('#final_output_list').attr('data');
		

		/*$('#final_output_list').html(jsonString);*/
			$.ajax( {
                type: "POST",
                dataType: 'json',
                url: '/memberships/saveitems',
                data: {mem_id: membership_id,data: jsonString,_token: $('[name="_token"]').val()},
                success: function( response ) {
                	$('#final_output_list').html('');
                    $('#final_output_list').html(response.data.html);

                    //add submit form function for saving changes
                    $('#form-memItems').submit(function(e){
                    	e.preventDefault();
                    	$.ajax( {
			                type: "POST",
			                url: $(this).attr( 'action' ),
			                data: $(this).serialize(),
			                success: function( response ) {
			                	if (response.data.msg=='ok'){
			                		swal({
			                            title: "Saved!",
			                            text: 'Successfully saved changes!',
			                            type: "success"
			                        });
			                	}
			                 }
            			});

                    });

                    $('.btn_deleteitem').click(function(e){
    					e.preventDefault();
    					var url = $(this).attr('href');

    					$.ajax( {
			                type: "GET",
			                url: url,			                
			                success: function( response ) {
			                	if (response.data.msg=='ok'){
			                		swal({
			                            title: "Deleted!",
			                            text: 'Successfully deleted an item!',
			                            type: "warning"
			                        });
			                        document.location.reload;
			                	}
			                 }
            			});


    				});
                 }
            });
      
	}
});