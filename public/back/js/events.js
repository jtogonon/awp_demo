$(function() {
  //===== Calendar =====//
  
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  
  $('#calendar').fullCalendar({
    header: {
      left: 'prev,next',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    
    loading: function(bool) {
        if (bool) $('#loading').show();
        else $('#loading').hide();
      },
    editable: true,//REMEMBER O/ZERO IS JANUARY for the month
    events: calEvents,
    eventClick: function(calEvent, jsEvent, view) {     
      moment('MMMM Do YYYY, h:mm:ss a');
      if (calEvent.eventtype=='job'){
        var url = '/jobs/view/' + calEvent.id;
      } else {
        var url = '/events/view/' + calEvent.id;
      }
      var msg = "<strong>Event:</strong> " + calEvent.title + "<br/><strong>Event Type</strong>: "+ calEvent.eventtype + "<br/> <strong>Event Date / Time:</strong> " + moment(calEvent.start).format('MMM DD, YYYY hh:mm a') + "<br/><strong>Customer:</strong> " + calEvent.customer;

      swal({   
        title: calEvent.title,   
        text: msg,   
        html: true,
        showCancelButton: true,
        confirmButtonText: 'View Event'
      },
      function(isConfirm){   
        if (isConfirm) {     
          window.location.href = url;
        } 
      }
      );

      },
      dayClick: function(date, jsEvent, view) {
      var url = '/events/new/' + date.format();
      $.prompt("Do you want to create an event on the selected date?", {        
        title: "Create Event?",
        buttons: { "Yes": true, "Cancel": false },
        submit:function(e,v,m,f){
          if(v){
            e.preventDefault();
            window.location.href=url;           
          }
          $.prompt.close();
        }
      });
    }
    
  });
});        