$(document).ready(function(){  
    $('.modal').appendTo("body")
    var $modal = $('#modal-add-user');      

    /*$('#booking_date').datepicker({ });*/

    if ( $( "#users_tbl" ).length ) {
        $('#users_tbl').dataTable({
            columnDefs: [
                  { orderable: false, targets: -1 }
            ]
        });
    }

    if ($(".select2").length > 0){
        $(".select2").select2();
    }


    $('.btn_add').click(function(e){
    	e.preventDefault();

    	$modal.modal('show');
        $('.datepicker').datepicker({});

    	$('#form-add').submit(function(e){
    		e.preventDefault();


    		$.ajax( {
                type: "POST",
                url: $(this).attr( 'action' ),
                data: $(this).serialize(),
                success: function( response ) {
                	if (response.data.html=='saved'){
			    		swal({
			                title: "Saved!",
			                text: response.data.msg,
			                type: "success"
			            });
			            setInterval(function() {
						    window.location.href=response.data.redirectTo;
						}, 1000);
			            
			            $modal.modal('hide');
		    		}
                    if (response.data.html=='failed'){
                        swal({
                            title: "Failed saving Data!",
                            text: response.data.errormsg,
                            type: "error"
                        });
                       

                    }
		         }
            });

            //redirect
            
    	})
    	

    });

    $('.btn_delete').click(function(e){
        e.preventDefault();
        var btn = $(this);

        swal({   title: "Are you sure?",   
                 text: "You will not be able to recover this data!",   
                 type: "warning",   showCancelButton: true,   
                 confirmButtonColor: "#DD6B55",   
                 confirmButtonText: "Yes, delete it!",   
                 closeOnConfirm: false 
             }, 
             function(){   
                $.ajax( {
                type: "GET",
                url: btn.attr( 'href' ),                
                success: function( response ) {
                    if (response.data.html=='deleted'){
                        swal("Deleted!", response.data.msg, "success"); 
                    }
                    if (response.data.html=='failed'){
                        swal({
                            title: "Failed deleting Data!",
                            text: response.data.errormsg,
                            type: "error"
                        });                       

                    }
                    setInterval(function() {
                    window.location.href=response.data.redirectTo;
                    }, 1000);
                }
                
            });
                
        });

    });/*end btn_delete*/


    $('.form_details').submit(function(e){
        e.preventDefault();

        $.ajax( {
                type: "POST",
                url: $(this).attr( 'action' ),
                data: $(this).serialize(),
                success: function( response ) {
                    if (response.data.html=='saved'){
                        swal({
                            title: "Saved!",
                            text: response.data.msg,
                            type: "success"
                        });
                        setInterval(function() {
                            window.location.href=response.data.redirectTo;
                        }, 1000);
                        
                        $modal.modal('hide');
                    }
                    if (response.data.html=='failed'){
                        swal({
                            title: "Failed saving Data!",
                            text: response.data.errormsg,
                            type: "error"
                        });
                       

                    }
                 }
            });
    });

});