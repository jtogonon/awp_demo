$(document).ready(function(){  
    $('.modal').appendTo("body")


    if ($('#compose-textarea').length > 0){
    	$("#compose-textarea").summernote({
    		height: 200,                 // set editor height
  			minHeight: null,             // set minimum height of editor
  			maxHeight: null,             // set maximum height of editor  			
    	});    	
    }

    $('.note-editable').on("blur", function(){
       /*$('textarea[name="emailBody"]').html($('#compose-textarea').code());*/
    });

    if ($('#sendto_spec').length > 0){
    	$('#sendto_spec').click(function(){
    		if ($(this).prop('checked', true)){
    			$('#to_field').show();
				$('#to_field2').hide();
    		} 
    		
    	})

    }

    if ($('#sendto_all').length > 0){
    	$('#sendto_all').click(function(){
	    	if ($(this).prop('checked',true)){
	    		$('#to_field').hide();
	    		$('#to_field2').hide();
	    	}
    	})
    }

    if ($('#sendto_email').length > 0){
    	$('#sendto_email').click(function(){
	    	if ($(this).prop('checked',true)){
	    		$('#to_field').hide();
	    		$('#to_field2').show();
	    	}
    	})
    }

    $('#form-sendmail').submit(function(e){
    	e.preventDefault();
        var el = $(this);

        if ($('#email_subject').val()==''){
            swal({
                title: "Missing Email Subject!",
                text: 'Please enter the Email subject.',
                type: "error"
            });
            return false;
        }

    	if ($('#sendto_all').is(':checked')){
    		swal({
                title: "Send to All?",
			    text: "Are you sure? You will not be able to cancel it!",
			    type: "warning",
			    showCancelButton: true,
			    confirmButtonColor: '#DD6B55',
			    confirmButtonText: 'Yes, I am sure!',
			    cancelButtonText: "No, cancel it!",
			    closeOnConfirm: true,
			    closeOnCancel: true
            },
             function(isConfirm){

			   if (isConfirm){
                   sendAjax(el);
                   return;

			    } else {
			      return false;
			      e.preventDefault();
			    }
			 });
            return;
    	}


    	if ( $('#sendto_spec').is(':checked') ){
    		if ($('#customers_dp').val()==''){
    			 swal({
                    title: "No selected Customers!",
                    text: 'Select Customers to send this Email to!',
                    type: "error"
                });
    			 return false;
    		} else {
                sendAjax(el);
            }
    	}

        if ( $('#sendto_email').is(':checked') ) {
            if ($('#emailto').val()=='') {
                swal({
                    title: "Missing Recipient!",
                    text: 'Please enter valid email address!',
                    type: "error"
                });
                return false;
            } else {
                sendAjax(el);
            }
        }



        function sendAjax(el) {
            var token =  $("input[name=_token]").val();
            var send_to = 0;

            if ( $('#sendto_all').is(':checked') ){
                send_to = 1;
            }

            if ( $('#sendto_spec').is(':checked') ){
                send_to = 2;
            }

            if ( $('#sendto_email').is(':checked') ){
                send_to = 3;
            }

            $.ajax({
                type: "POST",
                url: el.attr('action'),
                data: {emailBody:$('#compose-textarea').summernote("code"), _token:token,
                        customers:$('#emailto').val(),
                        emailSubject:$('#email_subject').val(),
                        sendto:send_to
                    },
                success: function (response) {
                    if (response.data.html == 'sent') {
                        swal({
                            title: "Email Sent!",
                            text: 'sent',
                            type: "success"
                        });
                        setInterval(function () {
                            window.location.href = response.data.redirectTo;
                        }, 1000);


                    }
                    if (response.data.html == 'failed') {
                        swal({
                            title: "Error!",
                            text: response.data.errorMsg,
                            type: "error"
                        });


                    }
                }
            });
        }

    })

});