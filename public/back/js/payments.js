$(document).ready(function(){  
    $('.modal').appendTo("body")

    $('.datepicker').datepicker();

    if ($(".select2").length > 0){
        $(".select2").select2();
    }

    if ($("#payments_tbl").length > 0){
        $('#payments_tbl').dataTable({
            columnDefs: [
                  { orderable: false, targets: -1 }
            ],
             "order": [[ 3, "desc" ]]
        });
    }

    if ($(".timepicker").length > 0){
        $(".timepicker").timepicker({
          showInputs: false
        });
    }

    

    $('#form-add').submit(function(e){
            e.preventDefault();


            $.ajax( {
                type: "POST",
                url: $(this).attr( 'action' ),
                data: $(this).serialize(),
                success: function( response ) {
                    if (response.data.html=='saved'){
                        swal({
                            title: "Saved!",
                            text: response.data.msg,
                            type: "success"
                        });
                        setInterval(function() {
                            window.location.href=response.data.redirectTo;
                        }, 1000);
                        
                        
                    }
                    if (response.data.html=='failed'){
                        swal({
                            title: "Failed saving Data!",
                            text: response.data.errormsg,
                            type: "error"
                        });
                       

                    }
                 }
            });

            //redirect
            
        })

   $( "#user_id" ).change(function() {
        var customer_id = $(this).val();
        $(document).ajaxStart(function() { Pace.restart(); });
        //make ajax request to pull invoices of selected customer
        if (customer_id > 0){
            $.ajax({
                url: '/invoices/getInvoicesByUserId/' + customer_id,
                type: 'GET',                                
                success: function(response) {
                    $('.ajax_invoice_list').empty();
                     if (response.html=='noresult'){
                        $('.ajax_invoice_list').addClass('alert alert-danger alert-dismissible');
                        $('.ajax_invoice_list').append('No Outstanding Invoices Found');
                     } else {
                        $('.ajax_invoice_list').removeClass('alert alert-danger alert-dismissible');
                        $('.ajax_invoice_list').append(response.html);
                     }
                 }
            });
        }//endif
    });


    jQuery.fn.extend({
        load_job_timeline: function () {
            var el = $('#timeline');
            el.empty();
            var job_id = $('#job_id').val();

            $(document).ajaxStart(function() { Pace.restart(); });

            $.ajax({url: '/jobs/getTimeline/'+job_id, success: function(result){
                el.html(result.html);
            }});
        }
    });

    if ($('#view_job_timeline').length > 0){
        $('#view_job_timeline').load_job_timeline();
    }

     $(".ajax_invoice_list .btn_apply_payment").on('click', function(e){        

        var total = $(this).attr('data');
        alert(total);
        $('#payment_received').val(total);
    });

});