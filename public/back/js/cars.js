$(document).ready(function(){  
    $('.modal').appendTo("body")

    if ($(".select2").length > 0){
    	$(".select2").select2();
	}

    $('#cars_tbl').dataTable();


    jQuery.fn.extend({
	    load_timeline: function () {
	        var el = $('#timeline');
			el.empty();
			var car_id = $('#car_id').val();

			$(document).ajaxStart(function() { Pace.restart(); });

			$.ajax({url: '/cars/getTimeline/'+car_id, success: function(result){
	            el.html(result.html);
	        }});
	    }
	});

    if ($('#view_car_timeline').length > 0){
    	$('#view_car_timeline').load_timeline();
    }

    if ($('#plate_number').length > 0){
    	var plate = $('#plate_number');
	    $(plate).bind('keyup change', function(e) {

		    var str = $.trim( $(this).val() );
		    if( str != "" ) {
		      if (! str.match(/^[a-zA-Z0-9]+$/)){
		        alert('Please enter Aplhanumeric only.');
		        $('#plate_number').val('');
		        newVal = str.slice(0,-1)
		        $(plate).val(newVal);
		        $(plate).focus();
		      }



		      $(plate).val(($(plate).val()).toUpperCase());
		    }
		    
		});
	}

});