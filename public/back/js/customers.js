$(document).ready(function(){  
    $('.modal').appendTo("body")
    var $modal = $('#modal-add-user');      

    /*$('#booking_date').datepicker({ });*/

    if ($('#users_tbl').length > 0){
    	$('#users_tbl').dataTable();
	}

	if ($('#cars_tbl').length > 0){
		$('#cars_tbl').dataTable();
	}


	jQuery.fn.extend({
	    load_user_timeline: function () {
	        var el = $('#timeline');
			el.empty();
			var user_id = $('#user_id').val();

			$(document).ajaxStart(function() { Pace.restart(); });

			$.ajax({url: '/users/getTimeline/'+user_id, success: function(result){
	            el.html(result.html);
	        }});
	    }
	});

    if ($('#view_profile_timeline').length > 0){
    	$('#view_profile_timeline').load_user_timeline();
    }


    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = '/files/upload';

    if ($('#fileupload').lenth > 0) {

        $('#fileupload').fileupload({
            url: url,
            dataType: 'json',
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    $('<p/>').text(file.name).appendTo('#files');
                });
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                    'width',
                    progress + '%'
                );
            },
             success: function( response ) {
            	window.location.href=response.data.redirectTo;
            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    }

    if ($('#box_membership_record').length > 0) {
        var user_id = $('#user_id').val();
        console.log('getting membership record');
        $.ajax( {
                type: "GET",
                url: '/users/get_membership_record/' + user_id,
                data: {},
                success: function( response ) {
                    /*console.log(response.data.html);*/
                    $('#box_membership_record').html(response.data.html);
                 }
            });
    }
});