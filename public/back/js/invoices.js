$(document).ready(function(){  
    $('.modal').appendTo("body")

    $('.datepicker').datepicker();

    if ($(".select2").length > 0){
        $(".select2").select2();
    }

    if ($("#invoice_tbl").length > 0){
        $('#invoice_tbl').dataTable({
            columnDefs: [
                  { orderable: false, targets: -1 }
            ]
        });
    }

    if ($(".timepicker").length > 0){
        $(".timepicker").timepicker({
          showInputs: false
        });
    }

    $("#user_id").change(function() {
        $("#uniform-car_id span").text('-select-');
        var value = $(this).val();
        var txt = $("#user_id option:selected").text();

        
        $.ajax({
            type: "GET",   
            dataType: 'html',     
            url: '/jobs/getcars/'+value,            
            success: function(response){
                $("#car_id option")
                    .not(":first").remove();

                parsed = $.parseJSON(response);
                if (parsed instanceof Array){
                    $.each(parsed, function(key, value) {
                        $("#car_id")
                            .append($("<option></option>")
                            .attr("value",value.id)
                            .text(value.name));
                    });
                    
                    //write the name on the textbox
                    
                } else  {
                    $("#uniform-car_id span").text('None');
                }
                $('#job_name').val(txt + ' - ') ;
            }
        });
    });

    $("#user_id").change(function() {
        $('#job_name').val($(this).val());
    });

    $('#form-add').submit(function(e){
            e.preventDefault();


            $.ajax( {
                type: "POST",
                url: $(this).attr( 'action' ),
                data: $(this).serialize(),
                success: function( response ) {
                    if (response.data.html=='saved'){
                        swal({
                            title: "Saved!",
                            text: response.data.msg,
                            type: "success"
                        });
                        setInterval(function() {
                            window.location.href=response.data.redirectTo;
                        }, 1000);
                        
                        
                    }
                    if (response.data.html=='failed'){
                        swal({
                            title: "Failed saving Data!",
                            text: response.data.errormsg,
                            type: "error"
                        });
                       

                    }
                 }
            });

            //redirect
            
        })

});