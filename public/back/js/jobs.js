$(document).ready(function(){  
    $('.modal').appendTo("body")

    $('.datepicker').datepicker();

    var $modal = $('#modal-add-user');   
    var $modalForCar = $('#modal-add-car');   

    if ($(".select2").length > 0){
        $(".select2").select2();
    }

    if ($("#jobs_tbl").length > 0){
        $('#jobs_tbl').dataTable({
            columnDefs: [
                  { orderable: false, targets: -1 }
            ],
             "order": [[ 4, "desc" ]]
        });
    }

    if ($(".timepicker").length > 0){
        $(".timepicker").timepicker({
          showInputs: false
        });
    }

    $("#user_id").change(function() {
        $("#uniform-car_id span").text('-select-');
        var value = $(this).val();
        var txt = $("#user_id option:selected").text();

        
        $.ajax({
            type: "GET",   
            dataType: 'html',     
            url: '/jobs/getcars/'+value,            
            success: function(response){
                $("#car_id option")
                    .not(":first").remove();

                parsed = $.parseJSON(response);
                if (parsed instanceof Array){
                    $.each(parsed, function(key, value) {
                        $("#car_id")
                            .append($("<option></option>")
                            .attr("value",value.id)
                            .text(value.name));
                    });
                    
                    //write the name on the textbox
                    
                } else  {
                    $("#uniform-car_id span").text('None');
                }
                $('#job_name').val(txt + ' - ') ;
            }
        });
    });

    $("#user_id").change(function() {
        $('#job_name').val($(this).val());
    });

    $('#form-add').submit(function(e){
            e.preventDefault();


            $.ajax( {
                type: "POST",
                url: $(this).attr( 'action' ),
                data: $(this).serialize(),
                success: function( response ) {
                    if (response.data.html=='saved'){
                        swal({
                            title: "Saved!",
                            text: response.data.msg,
                            type: "success"
                        });
                        setInterval(function() {
                            window.location.href=response.data.redirectTo;
                        }, 1000);
                        
                        
                    }
                    if (response.data.html=='failed'){
                        swal({
                            title: "Failed saving Data!",
                            text: response.data.errormsg,
                            type: "error"
                        });
                       

                    }
                 }
            });

            //redirect
            
        })

    $('a.btn_changeStat').click(function(e){
        e.preventDefault();

        var job_id = $(this).attr('data');
        var url = $(this).attr('href');
        $.ajax( {
                type: "GET",
                url: url,                
                success: function( response ) {
                    if (response.data.html=='saved'){
                        swal({
                            title: "Status Changed!",
                            text: response.data.msg,
                            type: "success"
                        });
                        setInterval(function() {
                            window.location.href=response.data.redirectTo;
                        }, 1000);
                    }
                    if (response.data.html=='failed'){
                        swal({
                            title: "Failed changing status!",
                            text: response.data.msg,
                            type: "error"
                        });                       

                    }
                 }
            });
    });


    jQuery.fn.extend({
        load_job_timeline: function () {
            var el = $('#timeline');
            el.empty();
            var job_id = $('#job_id').val();

            $(document).ajaxStart(function() { Pace.restart(); });

            $.ajax({url: '/jobs/getTimeline/'+job_id, success: function(result){
                el.html(result.html);
            }});
        }
    });

    jQuery.fn.extend({
        load_job_messages: function () {
            var el = $('#view_job_messages');
            el.empty();
            var job_id = $('#job_id').val();

            $(document).ajaxStart(function() { Pace.restart(); });

            $.ajax({url: '/jobs/getMessages/'+job_id, success: function(result){
                el.html(result.html);
            }});
        }
    });

    if ($('#view_job_timeline').length > 0){
        $('#view_job_timeline').load_job_timeline();
    }

    if ($('#view_job_messages').length > 0){
        $('#view_job_messages').load_job_messages();
    }


    /*this is the form in the job create page, user is created from there*/
    $('#form-add-user').submit(function(e){
            e.preventDefault();


        $.ajax( {
            type: "POST",
            url: $(this).attr( 'action' ),
            data: $(this).serialize(),
            success: function( response ) {
               setInterval(function() {
                window.location.href=response.data.redirectTo;
            }, 1000);
             }
        });
    });

   
    var requestSent = false;
    $('#job_message_form').submit(function(e){
        console.log('sending private message');
        e.preventDefault();
        if(!requestSent) {
            requestSent = true;
            $.ajax( {
                type: "POST",
                url: $(this).attr( 'action' ),
                data: $(this).serialize(),
                success: function( response ) {
                    console.log(response.data.html);
                    $('#view_job_messages').load_job_messages();
                 },
                complete: function() {            
                    requestSent = false;
                },

            });
        }
    });


    $('.btn_add_car_to_user').click(function(e){
        e.preventDefault();

        $modalForCar.modal('show');
    });

    $('#form-add-car').submit(function(e){
        e.preventDefault();

        $.ajax( {
            type: "POST",
            url: $(this).attr( 'action' ),
            data: $(this).serialize(),
            success: function( response ) {
                
             }
        });
    });

});