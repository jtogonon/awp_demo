<?php
use App\Helpers\Helper;
?>
@extends('master')

@section('content')
  	 <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Expenses Categories
        </h1>
        <div class="pull-right" style="padding:10px;">
          <a href="" class="btn btn-block btn-success btn_add" id="btn_addcat">Add New Expense Category</a>
        </div>
      </section>

      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">List</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table id="cars_tbl" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>Expenses Category</th>
                        <th>Expenses Made</th>
                        
                        <th>Actions</th>   
                      </tr>
                      </thead>
                      <tbody>
                        <?php 
                        foreach ($expCategories as $cat): ?>
                        <tr>
                          <td><?=$cat->name?></td>                         
                          
                          <td><?=$cat->getTotalUnderCategory()?></td>
                          
                          <td><!-- <a  class="btn btn-flat" title="Edit">
                                <i class="fa fa-edit"></i>
                              </a> -->
                              <a href="/expenses_categories/delete/<?=$cat->id?>" class="btn btn-flat btn_delete">
                                <i class="fa fa-remove"></i>
                              </a>

                          </td>
                         
                        </tr>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
            </div><!-- end box -->
          </div><!-- end col-xs-12 -->
        </div><!-- end row -->

      </section>



      <div class="modal fade" tabindex="-1" role="dialog" id="modal-add-user">
        <div class="modal-dialog">
          {!! Form::model(new App\Models\ExpenseCategory, array('url' => 'expenses_categories/save', 'method' => 'post','class'=>'form-horizontal','id'=>'form-add')) !!}
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Add New Category of Expenses</h4>
            </div>
            <div class="modal-body">
              
                  <div class="box-body">
                    <div class="form-group">
                       {!! Form::label('name', 'Category Name',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-8">                            
                            {!! Form::text('name', '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div>                                      
                    
                   
                  </div>

              
            </div>
            <div class="modal-footer">
             
              <button type="submit" class="btn btn-primary">Save New Category</button>
            </div>
          </div>
          {!! Form::close() !!}
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

@endsection