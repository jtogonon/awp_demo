<?php
use App\Helpers\Helper;
?>
<ul class="todo-list">
  <?php
  if (count($todos)> 0): 
   foreach ($todos as $todo) : 
    $class = ($todo->finished=='0000-00-00 00:00:00') ?  '': 'done';
    $checked = ($todo->finished=='0000-00-00 00:00:00') ?  '': 'checked';
    ?>
  <li class="<?=$class?>">
    <!-- drag handle -->
        <span class="handle">
          <i class="fa fa-ellipsis-v"></i>
          <i class="fa fa-ellipsis-v"></i>
        </span>
    <!-- checkbox -->
    <input type="checkbox" value="<?=$todo->id?>" name="" class="checkTodoItem" <?=$checked?>>
    <!-- todo text -->
    <span class="text"><?=$todo->todo?></span>
    <!-- Emphasis label -->
    <small class="label label-danger"><i class="fa fa-clock-o"></i> <?=Helper::formatDate($todo->created_at,5)?></small>
    <!-- General tools such as edit or delete-->
    <div class="tools">
      <i class="fa fa-edit"></i>
      <i class="fa fa-trash-o"></i>
    </div>
  </li>
<?php endforeach; endif;?>
  
</ul>