<?php
use App\Helpers\Helper;
?>
<table id="jobs_tbl" class="table table-bordered table-striped">
  <thead>
  <tr>
    <th width="10%">Job ID</th>
    <th>Customer</th>
    <th>Car</th>
    <th>Status</th>
    <th></th>
    
  </tr>
  </thead>
  <tbody>
    <?php 
    foreach ($jobs as $job): 
      $class = App\Models\JobStatus::getStatusClass($job->status_id);
      $timeago = $job->created_at->diffForHumans();
      ?>
    <tr>
      <td><a href="/jobs/view/<?=$job->id?>"><?=$job->id?></a></td>
      <td><a href="/users/view/<?=$job->user_id?>"><?=$job->getCustomer()?></a></td>
      <td><a href="/cars/view/<?=$job->car_id?>"><?=$job->getCar()?></a></td>
      <td align="center"><span class="label label-<?=$class?>"><?=$job->getStatus()?></span></td>
      <td><?=$timeago?></td>         
      
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>
