
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{$metaTitle}} - Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="/back/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
  <!-- Ionicons -->
  <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
  <!-- Theme style -->
  <link rel="stylesheet" href="/back/css/AdminLTE.css">
    <link rel="stylesheet" href="/back/css/custom.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->


  <link rel="stylesheet" href="/back/css/skins/_all-skins.min.css">

  <!-- Sweet Alert -->
  <link rel="stylesheet" type="text/css" href="/plugins/sweetalert/dist/sweetalert.css">
  <!-- Pace style -->
  <link rel="stylesheet" href="/plugins/pace/pace.min.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  {{ Asset::css('headerCss') }}
</head>
<body onload="window.print();">
<div class="wrapper">

        @yield('content')    

  
</div>
<!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="/plugins/jQueryUI/jquery-ui.min.js"></script> 

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<!-- Bootstrap 3.3.5 -->
<script src="/back/bootstrap/js/bootstrap.min.js"></script>



<!-- AdminLTE App -->
<script src="/back/js/app.min.js"></script>

<!-- Slimscroll -->
<script src="/plugins/slimScroll/jquery.slimscroll.min.js"></script>

<!-- Common JS functions here -->
<script src="/back/js/common.js"></script>


{{ Asset::js('footer') }}
</body>
</html>
