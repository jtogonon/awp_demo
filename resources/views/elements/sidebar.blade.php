<?php
$class_Customers = '';
$class_Cars = '';
$class_Membership = '';

$class_Event = '';
$class_Jobs = '';

$class_Invoice ='';
$class_Payment = '';
$class_Expense = '';
$class_ExpCat ='';
$class_Vendor ='';

$class_Package ='';
$class_Service = '';

$class_Product ='';
$class_ProdMan ='';

$AccountType = $userauth->account_type;
?>
<!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{$userprofilepic}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{$userfullname}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i><?php echo ucfirst($AccountType); ?> Account</a>
        </div>
      </div>
      <!-- search form -->
      <form action="/search" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat" value='1'><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="<?=($controllerName=='DashboardController')? 'active': '';?> treeview">
          <a href="/dashboard">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span> </i>
          </a>
        </li>
        <li class="<?=($controllerName=='UsersController' || $controllerName=='CarsController' || $controllerName=='MembershipsController')? 'active': '';?> treeview">
          <?php
            if ($controllerName=='UsersController'):
                $class_Customers = 'active';
            endif;
            if ($controllerName=='CarsController'):
                $class_Cars = 'active';
            endif;
            if ($controllerName=='MembershipsController'):
                $class_Membership = 'active';
            endif;
          ?>
          <a href="#">
            <i class="fa fa-users"></i>
            <span>People</span>
           <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?=$class_Customers?>"><a href="/users"><i class="fa fa-circle-o"></i> Customers</a> <i class="fa fa-angle-left pull-right"></i></li>
            <li class="<?=$class_Cars?>"><a href="/cars"><i class="fa fa-circle-o"></i> Cars</a></li>
            <li class="<?=$class_Membership?>"><a href="/memberships"><i class="fa fa-circle-o"></i> Memberships</a></li>
          
          </ul>
        </li>
       
        <li class="<?=($controllerName=='JobsController' || $controllerName=='EventsController')? 'active': '';?> treeview">
          <?php
            if ($controllerName=='JobsController'):
                $class_Jobs = 'active';
            endif;
            if ($controllerName=='EventsController'):
                $class_Event = 'active';
            endif;
            
          ?>
          <a href="/calendar">
            <i class="fa fa-calendar"></i>
            <span>Calendar</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?=$class_Event?>"><a href="/calendar"><i class="fa fa-circle-o"></i> Schedules</a></li>
            <li class="<?=$class_Jobs?>"><a href="/jobs"><i class="fa fa-circle-o"></i> Jobs</a></li>
            
          </ul>
        </li>
        
       
        <li class="<?=($controllerName=='InvoicesController' || $controllerName=='PaymentsController' || $controllerName=='ExpensesController' || $controllerName=='ExpensesCategoriesController' || $controllerName=='VendorsController')? 'active': '';?> treeview">
          <a href="#">
            <i class="fa  fa-money"></i> <span>Transactions</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <?php
            if ($controllerName=='InvoicesController'):
                $class_Invoice = 'active';
            endif;
            if ($controllerName=='PaymentsController'):
                $class_Payment = 'active';
            endif;
            if ($controllerName=='ExpensesController'):
                $class_Expense = 'active';
            endif;
            if ($controllerName=='ExpensesCategoriesController'):
                $class_ExpCat = 'active';
            endif;
            if ($controllerName=='VendorsController'):
                $class_Vendor = 'active';
            endif;
          ?>
          <ul class="treeview-menu">
            <li class="<?=$class_Invoice?>"><a href="/invoices"><i class="fa fa-circle-o"></i> Invoices</a></li>
            <li class="<?=$class_Payment?>"><a href="/payments"><i class="fa fa-circle-o"></i> Payments</a></li>
            <li class="<?=$class_Expense?>"><a href="/expenses"><i class="fa fa-circle-o"></i> Expenses</a></li>
            <li class="<?=$class_ExpCat?>"><a href="/expenses_categories"><i class="fa fa-circle-o"></i> Expenses Categories</a></li>
            <li class="<?=$class_Vendor?>"><a href="/vendors"><i class="fa fa-circle-o"></i> Vendors</a></li>
          </ul>
        </li>

        <li class="<?=($controllerName=='EmailsController')? 'active': '';?> treeview">
          <?php
            if ($controllerName=='PackagesController'):
                $class_Package = 'active';
            endif;
            
          ?>
          <a href="#">
            <i class="fa fa-envelope"></i> <span>Email Marketing</span>
           @if ($AccountType=='free')<small class="label pull-right bg-yellow">Upgrade</small> @endif
            
           @if ($AccountType=='standard' || $AccountType=='pro')
             <i class="fa fa-angle-left pull-right"></i>
           @endif
          </a>
          <ul class="treeview-menu">
            @if ($AccountType=='free' )
              <li class="<?=$class_Service?>"><a href="/account/upgrade"><i class="fa fa-circle-o"></i> Upgrade Account <small class="label pull-right bg-green">Standard</small></a></li> 
            @endif
            @if ($AccountType=='standard' )
              <li class="<?=$class_Service?>"><a href="/emails/compose"><i class="fa fa-circle-o"></i> Send Email</a></li> 
            @endif

            @if ($AccountType=='free' || $AccountType=='standard')
            <li class="<?=$class_Service?>"><a href="/account/upgrade/pro"><i class="fa fa-circle-o"></i> Advanced Features
            @if ($AccountType!='pro')<small class="label pull-right bg-yellow">PRO</small>@endif</a>
              
            </li> 
            @endif
            
          </ul>
        </li>

        <li class="<?=($controllerName=='PackagesController' || $controllerName=='ServicesController')? 'active': '';?> treeview">
          <?php
            if ($controllerName=='PackagesController'):
                $class_Package = 'active';
            endif;
            if ($controllerName=='ServicesController'):
                $class_Service = 'active';
            endif;
            
          ?>
          <a href="#">
            <i class="fa fa-wrench"></i> <span>Services</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?=$class_Service?>"><a href="/services"><i class="fa fa-circle-o"></i> Services</a></li>
            <!-- <li class="<?=$class_Package?>"><a href="/packages"><i class="fa fa-circle-o"></i> Packages</a></li> -->            
          </ul>
        </li>

        <li class="<?=($controllerName=='ProductManufacturersController' || $controllerName=='ProductsController')? 'active': '';?> treeview">
          <?php
            if ($controllerName=='ProductManufacturersController'):
                $class_ProdMan = 'active';
            endif;
            if ($controllerName=='ProductsController'):
                $class_Product = 'active';
            endif;            
          ?>
          <a href="#">
            <i class="fa fa-exchange"></i> <span>Inventory</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?=$class_Product?>"><a href="/products"><i class="fa fa-circle-o"></i> Products</a></li>
            <li class="<?=$class_ProdMan?>"><a href="/product_manufacturers"><i class="fa fa-circle-o"></i> Manufacturers</a></li>            
          </ul>
        </li>
      </ul>