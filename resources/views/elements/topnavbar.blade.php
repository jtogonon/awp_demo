<ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">{{$newUnOpenedEmails}}</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have {{$newUnOpenedEmails}} messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">{{$newUnReadNotifications}}</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have {{$newUnReadNotifications}} notifications</li>
              <li>
                
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="user user-menu">
            <a href="/my_account">
              <img src="{{$userprofilepic}}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{$userfullname}}</span>
            </a>
            
          </li>
          <li>
            <a href="/auth/logout" class="">
              <i class="fa fa-power-off"></i>             
            </a>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>