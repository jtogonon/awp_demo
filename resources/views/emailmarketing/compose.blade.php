<?php
use App\Models\Country;
use App\Helpers\Helper;
?>
@extends('master')
@section('headerCss')
    <link rel="stylesheet" href="/plugins/summernote/summernote.css" />
    <link rel="stylesheet" href="/plugins/select2/select2.min.css" />
@endsection

@section('content')
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Send Email
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/dashboard">Dashboard</a></li>
        <li class="active">My Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">
          <a href="mailbox.html" class="btn btn-primary btn-block margin-bottom">Back to Inbox</a>

          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Folders</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="mailbox.html"><i class="fa fa-inbox"></i> Inbox
                  <span class="label label-primary pull-right">12</span></a></li>
                <li><a href="#"><i class="fa fa-envelope-o"></i> Sent</a></li>
                <li><a href="#"><i class="fa fa-file-text-o"></i> Drafts</a></li>
                <li><a href="#"><i class="fa fa-filter"></i> Junk <span class="label label-warning pull-right">65</span></a>
                </li>
                <li><a href="#"><i class="fa fa-trash-o"></i> Trash</a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Lists</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <?php if ($user->account_type=='free' || $user->account_type=='standard') : ?>
                <li><a href="/account/upgrade/pro"><i class="fa fa-circle-o text-red"></i> Upgrade to <small class="label bg-yellow">PRO</small> to create List</a></li>
                <?php else:?>

                <li><a href="#"><i class="fa fa-circle-o text-red"></i> Important</a></li>
                <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> Promotions</a></li>
                <li><a href="#"><i class="fa fa-circle-o text-light-blue"></i> Social</a></li>
                <?php endif;?>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Compose New Message</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body content">
              {!! Form::model(new App\Models\Email, array('url' => 'emails/send', 'method' => 'post','class'=>'','id'=>'form-sendmail')) !!}
              <?php if ($user->account_type=='free' || $user->account_type=='standard') : ?>
              <div class="form-group">
                  <div class="radio">
                    <label>
                      <input class="checkbox" type="radio" checked value="1" name="sendto" id="sendto_all"> Send to All
                    </label>
                  </div>  
                  <div class="radio">
                    <label><input class="checkbox" type="radio" value="2" name="sendto" id="sendto_spec"> Select Customer by Name</label>
                  </div>

                  <div class="radio">
                    <label><input class="checkbox" type="radio" value="3" name="sendto" id="sendto_email"> Enter Email</label>
                  </div>
                
              </div>
              <?php endif; ?>

              <div class="form-group" id="to_field" style="display:none">
                {!! Form::select('customers[]', $customers, '', ['class'=>'form-control select2','style'=>'width:100%;','placeholder'=>'To:','multiple'=>true,'id'=>'customers_dp']) !!}
              </div>
              <div class="form-group" id="to_field2" style="display:none">
                {!! Form::text('emailto', '', ['class'=>'form-control','style'=>'width:100%;','placeholder'=>'To:','id'=>'emailto']) !!}
              </div>
              <div class="form-group">
                {!! Form::text('emailsubject', '', ['class'=>'form-control','style'=>'width:100%;','placeholder'=>'Subject:','id'=>'email_subject']) !!}       
              </div>
              <div class="form-group">
                    <textarea name="emailBody" id="compose-textarea" class="form-control" style="height: 300px;">
                     
                    </textarea>
              </div>
              <!-- <div class="form-group">
                <div class="btn btn-default btn-file">
                  <i class="fa fa-paperclip"></i> Attachment
                  <input type="file" name="attachment">
                </div>
                <p class="help-block">Max. 32MB</p>
              </div> -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <button type="button" class="btn btn-default"><i class="fa fa-pencil"></i> Draft</button>
                <button type="submit" class="btn btn-success"><i class="fa fa-envelope-o"></i> Send Email</button>
              </div>
              <button type="reset" class="btn btn-danger"><i class="fa fa-times"></i> Discard</button>
            </div>
            <!-- /.box-footer -->
             {!! Form::close() !!}
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->

@endsection

@section('footer')

<script type="text/javascript" src="/plugins/summernote/summernote.min.js"></script>
<script type="text/javascript" src="/back/js/emails.js"></script>
@endsection