<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	

	<title>{{$title}}</title>
   <meta name="description" content="<?=$metaDesc?>" />
        <meta name="keywords" content="auto repair management, auto shop scheduling software, auto shop management software, car wash management software, auto shop billing software, auto shop accounting software, car wash billing software, car wash business plan, car wash inventory management software" />
	 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<link href="{{ asset('/css/front/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/front/style.css') }}" rel="stylesheet">

	<link href="{{ asset('/css/front/animate.min.css') }}" rel="stylesheet">
            
    <link href="{{ asset('/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">




	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="<?=strtolower($controllerName)?>">
	<div id="wrapper">
		<div class="navbar-wrapper">
		        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		            <div class="container">
		                <div class="navbar-header page-scroll">
		                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		                        <span class="sr-only">Toggle navigation</span>
		                        <span class="icon-bar"></span>
		                        <span class="icon-bar"></span>
		                        <span class="icon-bar"></span>
		                    </button>
		                    <a class="navbar-brand" href="/"><img src="/img/awp2-logo-white.png"></a>
		                </div>
		                <div id="navbar" class="navbar-collapse collapse">
		                    <ul class="nav navbar-nav navbar-right">
		                        <li><a class="page-scroll" href="/#page-top">Home</a></li>
		                        <li><a class="page-scroll" href="/#features">Features</a></li>
		                        <li><a class="page-scroll" href="/#testimonials">Testimonials</a></li>
		                        <li><a class="page-scroll" href="/#pricing">Pricing</a></li>
		                        <li><a class="" href="/blog">Blog</a></li>
		                        <li><a class="page-scroll" href="/#contact">Contact</a></li>
		                        <li><a class="page-scroll" href="/auth/login">Login</a></li>
		                    </ul>
		                </div>
		            </div>
		        </nav>
		</div>


		<?php if ($controllerName=='AuthController' || $controllerName=='PasswordController') : ?>
		<div class="clear clearfix" style="margin-top:100px;"></div>
		<?php else: ?>
		<div id="inSlider" class="carousel slide carousel-fade" data-ride="carousel">
		    <ol class="carousel-indicators">
		        <li data-target="#inSlider" data-slide-to="0" class="active"></li>
		        <li data-target="#inSlider" data-slide-to="1"></li>
		    </ol>
		    <div class="carousel-inner" role="listbox">
		        <div class="item active" id="slide_1">
		            <div class="container">
		                <div class="carousel-caption">
		                    <h1>Shop Management Software,<br/>
		                        for auto detailers, car wash<br/>
		                        or Auto Repair Shop
		                       </h1>
		                    <p>Web based Software to help you grow your automotive business.</p>
		                    <p>
		                        <a class="btn btn-lg btn-primary" href="/auth/register" role="button">Sign up Now - it's FREE!</a>
		                        
		                    </p>
		                </div>
		                <div class="carousel-image wow zoomIn">
		                    <img src="img/laptop.png" alt="laptop"/>
		                </div>
		            </div>
		            <!-- Set background for slide in css -->
		            <div class="header-back one"></div>

		        </div>
		        <div class="item" id="slide_2">
		            <div class="container">
		                <div class="carousel-caption blank">
		                    <h1>Beautiful app.<br/> Interface that inspire.</h1>
		                    <p>Manage your automotive business using your mobile phone, tablet or desktop.</p>
		                    <p> <a id="btn_signup" class="btn btn-lg btn-primary" href="/auth/register" >Sign up Now!</a></p>
		                </div>
		            </div>
		            <!-- Set background for slide in css -->
		            <div class="header-back two"></div>
		        </div>
		    </div>
		    <a class="left carousel-control" href="#inSlider" role="button" data-slide="prev">
		        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		        <span class="sr-only">Previous</span>
		    </a>
		    <a class="right carousel-control" href="#inSlider" role="button" data-slide="next">
		        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		        <span class="sr-only">Next</span>
		    </a>
		</div>

		<?php endif; ?>
	

	@yield('content')


	</div>
	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>

	<script src="/js/front/jquery-2.1.1.js"></script>
	<script src="/js/plugins/validate/jquery.validate.min.js"></script>
	<script src="/js/front/pace.min.js"></script>
	<script src="/js/front/bootstrap.min.js"></script>
	<script src="/js/front/classie.js"></script>
	<script src="/js/front/cbpAnimatedHeader.js"></script>
	<script src="/js/front/wow.min.js"></script>
	<script src="/js/front/inspinia.js"></script>

	<?php if(!App::isLocal()) : ?>
	<script>
		// Include the UserVoice JavaScript SDK (only needed once on a page)
		UserVoice=window.UserVoice||[];(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/ycFQp6NxxkzvO6HLYuhjQ.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})();

		//
		// UserVoice Javascript SDK developer documentation:
		// https://www.uservoice.com/o/javascript-sdk
		//

		// Set colors
		UserVoice.push(['set', {
		  accent_color: '#e2753a',
		  trigger_color: 'white',
		  trigger_background_color: 'rgba(46, 49, 51, 0.6)'
		}]);

		// Identify the user and pass traits
		// To enable, replace sample data with actual user traits and uncomment the line
		UserVoice.push(['identify', {
		  //email:      'john.doe@example.com', // User’s email address
		  //name:       'John Doe', // User’s real name
		  //created_at: 1364406966, // Unix timestamp for the date the user signed up
		  //id:         123, // Optional: Unique id of the user (if set, this should not change)
		  //type:       'Owner', // Optional: segment your users by type
		  //account: {
		  //  id:           123, // Optional: associate multiple users with a single account
		  //  name:         'Acme, Co.', // Account name
		  //  created_at:   1364406966, // Unix timestamp for the date the account was created
		  //  monthly_rate: 9.99, // Decimal; monthly rate of the account
		  //  ltv:          1495.00, // Decimal; lifetime value of the account
		  //  plan:         'Enhanced' // Plan name for the account
		  //}
		}]);

		// Add default trigger to the bottom-right corner of the window:
		UserVoice.push(['addTrigger', {mode: 'contact', trigger_position: 'bottom-right' }]);

		// Or, use your own custom trigger:
		//UserVoice.push(['addTrigger', '#id', { mode: 'contact' }]);

		// Autoprompt for Satisfaction and SmartVote (only displayed under certain conditions)
		UserVoice.push(['autoprompt', {}]);
	</script>
	<?php endif; ?>
</body>
</html>
