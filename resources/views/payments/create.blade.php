<?php
use App\Helpers\Helper;
?>
@extends('master')

@section('content')
  	 <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Payments          
        </h1>
        <div class="pull-right" style="padding:10px;">
          <a href="/payments" class="btn btn-block btn-success" id="btn_addpayment">Back to Payment</a>
        </div>
      </section>

      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">New Payment</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="box-body">
                      {!! Form::model(new App\Models\Payment, array('url' => 'payments/save', 'method' => 'post','class'=>'form-horizontal','id'=>'form-add')) !!}
                        @if($invoice_id>0)
                          {!! Form::hidden('user_id', $invoice->user_id, ['class'=>'form-control']) !!}
                          {!! Form::hidden('invoice_id', $invoice->invoice_id, ['class'=>'form-control']) !!}                            

                        @endif
                        <div class="form-group">
                             {!! Form::label('name', 'Customer:',['class'=>'col-sm-3 control-label']) !!}
                              <div class="col-md-4">
                                  @if($invoice_id==0)
                                    {!! Form::select('user_id', $customers, $invoice->user_id, ['class'=>'form-control select2','id'=>'user_id']) !!}                            
                                  @else
                                    {!! Form::text('user', $invoice->getCustomer(), ['class'=>'form-control','disabled'=>true]) !!}
                                  @endif
                              </div>                          
                        </div>

                        <div class="form-group">
                          @if($invoice_id>0)
                           {!! Form::label('invoicenumber', 'Invoice #',['class'=>'col-sm-3 control-label']) !!}
                            <div class="col-md-4">
                                {!! Form::text('invoicenumber', $invoice->invoicenumber, ['class'=>'form-control','disabled'=>true]) !!}                 
                            </div>
                          @else
                            <div class="ajax_invoice_list" style="min-height:50px;"></div>
                          @endif
                        </div>  


                        <div class="form-group">
                           {!! Form::label('payment_method_id', 'Payment Method:',['class'=>'col-sm-3 control-label']) !!}
                            <div class="col-md-4">
                                {!! Form::select('payment_method_id', $paymentMethods,1, ['class'=>'form-control select2']) !!}                 
                            </div>
                        </div>  

                        <div class="form-group">
                           {!! Form::label('payment_received_on', 'Payment Date:',['class'=>'col-sm-3 control-label']) !!}
                            <div class="col-md-4">
                                {!! Form::text('payment_received_on', date('m/d/Y'), ['class'=>'form-control datepicker']) !!}                 
                            </div>
                        </div>   

                        <div class="form-group">
                           {!! Form::label('payment_received', 'Amount:',['class'=>'col-sm-3 control-label']) !!}
                            <div class="col-md-4">
                                {!! Form::text('payment_received', $invoice->balance_due, ['class'=>'form-control','id'=>'payment_received']) !!}                 
                            </div>
                        </div>                    

                        
                        <div class="form-group">
                             {!! Form::label('description', 'Notes:',['class'=>'col-sm-3 control-label']) !!}
                              <div class="col-md-4">                            
                                  {!! Form::textarea('notes', '', ['class'=>'form-control','rows'=>'3']) !!}                            
                              </div>
                        </div> 
                        <div class="form-group">
                             {!! Form::label('', '',['class'=>'col-sm-3 control-label']) !!}
                              <div class="col-md-4">                            
                                  <button type="submit"  value="" class="btn btn-success">Save New Payment</button>&nbsp; <a href="/payments" class="btn btn-default">Cancel</a>
                              </div>
                        </div>

                        {!! Form::close() !!}
                        
                    </div>
                  </div>
            </div><!-- end box -->
          </div><!-- end col-xs-12 -->
        </div><!-- end row -->

      </section>

@endsection