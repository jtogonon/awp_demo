@extends('master')

@section('content')

	<section class="content-header">
        <h1>
          Users / Customers
          <small>Control panel</small>
        </h1>
        <div class="pull-right" style="padding:10px;">
          <a href="/events/new/<?=date('Y-m-d')?>" class="btn btn-block btn-success" id="btn_adduser">Add New Event</a>
        </div>
    </section>

    <section class="content">
        <div class="row">
          	<div class="col-xs-12">
          		<div class="box box-primary">
		            <div class="box-body no-padding">
		              <!-- THE CALENDAR -->
		              <div id="calendar"></div>
		            </div>
		            <!-- /.box-body -->
		         </div>
          	</div>
         </div>
    </section>

    <script>
    var calEvents = [
    <?php foreach ($events as $event) { 
        $backgroundcolor = ($event->event_type=='job') ? '#5BA1DE': '#DE915B';
      ?>
      {id: <?=$event->id?>,
        urlsegment: '<?=($event->event_type == 'job')? 'jobs':'calendar/event';?>',
        color: '<?=$backgroundcolor?>',
        title: '<?=$event->event_name?>',   
        start: '<?=$event->start_date?>T<?=$event->start_time?>',       
        end: '<?=$event->end_date?>T<?=$event->end_time?>',
        allDay: false,
        eventtype: '<?=$event->event_type?>',
        customer: '<?=$event->customer?>',  
      },
      <?php }  ?>
      ];
    </script>

@endsection


