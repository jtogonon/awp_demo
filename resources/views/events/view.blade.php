<?php
use App\Helpers\Helper;
?>
 @extends('master')

@section('content')
  	 <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Event Details
        </h1>
        <div class="pull-right" style="padding:10px;">
          <a href="/calendar" class="btn btn-block btn-default " id="btn_addexpense">Back to Calendar</a>
        </div>
      </section>

      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title">View Event / Appointment</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                     {!! Form::model(new App\Models\Service, array('url' => 'events/save', 'method' => 'post','class'=>'form-horizontal','id'=>'form-add')) !!}
                      {!! Form::hidden('event_id', $event->id, ['class'=>'form-control','id'=>'event_id']) !!}                            
                      <div class="form-group">
                       {!! Form::label('user_id', 'Customer:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-md-4">                            
                            {!! Form::select('user_id', $customers, $event->user_id, ['class'=>'form-control select2','placeholder'=>'Select Customer']) !!}                            
                        </div>
                        
                      </div> 

                     

                      <div class="form-group">
                       {!! Form::label('event_name', 'Event Name:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-md-4">                            
                            {!! Form::text('event_name', $event->event_name, ['class'=>'form-control','id'=>'job_name']) !!}                            
                        </div>                        
                      </div> 

                     

                      <div class="form-group">
                       {!! Form::label('start_date', 'Start Date:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="input-group col-md-4" style="padding-left:14px;">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>                          
                          {!! Form::text('start_date', Helper::formatDate($event->start_date,3), ['class'=>'form-control datepicker','id'=>'']) !!}    
                        </div>                        
                      </div> 

                      <div class="form-group ">
                       {!! Form::label('start_date', 'Start Time:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="input-group col-md-4 bootstrap-timepicker" style="padding-left:14px;">
                          <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                          </div>                          
                          {!! Form::text('start_time', Helper::formatTime($event->start_time,3), ['class'=>'form-control timepicker','id'=>'']) !!}    
                        </div>                        
                      </div> 

                      <div class="form-group">
                       {!! Form::label('notes', 'Notes:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-md-6">                            
                            {!! Form::textarea('notes', $event->notes, ['class'=>'form-control','rows'=>3]) !!}
                        </div>                        
                      </div> 

                      <div class="form-group">

                        <label class="col-sm-3 control-label">&nbsp;</label>
                          <div class="col-md-6 ">
                            <button type="submit"  value="" class="btn btn-success">Save Event</button>&nbsp; <a href="/calendar" class="btn btn-default">Cancel</a>
                        </div>
                      </div>


                     {!! Form::close() !!}
                  </div>
            </div><!-- end box -->
          </div><!-- end col-xs-12 -->
        </div><!-- end row -->

      </section>

@endsection