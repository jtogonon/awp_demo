<?php
use App\Helpers\Helper;
?>
@extends('master')

@section('content')
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Membership Level Details
      </h1>
      <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/memberships">Membership Levels</a></li>
        <li class="active">Membership Details</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-success">
            <div class="box-body box-profile">
              

              <h3 class="profile-username text-center"><?php echo $membership->membership_name;?></h3>

              <p class="text-muted text-center"><?php echo $membership->frequency;?></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Date Created</b> <a class="pull-right"><?=Helper::formatDate($membership->created_at,5)?></a>
                </li>
                <li class="list-group-item">
                  <b>Members</b> <a class="pull-right"><?=$membersCount;?></a>
                </li>                
                
              </ul>

             <!-- <a href="/membership/addmember//<?=$membership->id?>" class="btn btn-success btn-block"><b>Add Member</b></a> -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">              
              <li class="active"><a href="#benefits" data-toggle="tab">Benefits</a></li>
              <li><a href="#details" data-toggle="tab">Details</a></li>
            </ul>
            <div class="tab-content">
              
              <div class="tab-pane active" id="benefits">
                 @include('memberships/_view_benefits')
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="details">
                 {!! Form::model(new App\Models\Membership, array('url' => '/memberships/save', 'method' => 'post','class'=>'form-horizontal form_details','id'=>'form-membership')) !!}
                    {!! Form::hidden('id', $membership->id,['id'=>'membership_id']) !!}
                    @include('memberships/_view_details')
                  {!! Form::close() !!}
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->

@endsection