<?php
use App\Models\Membership;
?>
@extends('master')

@section('content')
  	 <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Memberships
          <small>Details</small>
        </h1>
        <div class="pull-right" style="padding:10px;">
          <a href="" class="btn btn-block btn-success btn_add" id="btn_addservice">Add New Membership Level</a>
        </div>
      </section>

      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">List</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table id="mems_tbl" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>Membership Name</th>
                        <th>Members</th>
                        <th>Fee</th>                        
                        <th>Actions</th>   
                      </tr>
                      </thead>
                      <tbody>
                        <?php 
                        foreach ($memberships as $mem): 
                          $membersCount = Membership::getMembers($auth->shop_id, $mem->id, false)?>
                        <tr>
                          <td><a href="/memberships/view/<?=$mem->id?>"><?=$mem->membership_name?></a></td>
                          <td><?php echo $membersCount;?></td>
                          <td>{{$shopcurrencysymbolhtml}}<?=number_format($mem->fee)?></td>
                          <td><a  href="/memberships/view/<?=$mem->id?>" class="btn btn-flat" title="Edit">
                                <i class="fa fa-edit"></i>
                              </a>
                              <a href="/memberships/delete/<?=$mem->id?>" class="btn btn-flat btn_delete">
                                <i class="fa fa-remove"></i>
                              </a>

                            </td>
                         
                        </tr>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
            </div><!-- end box -->
          </div><!-- end col-xs-12 -->
        </div><!-- end row -->

      </section>

      <div class="modal fade" tabindex="-1" role="dialog" id="modal-add-user">
        <div class="modal-dialog">
          {!! Form::model(new App\Models\Membership, array('url' => 'memberships/save', 'method' => 'post','class'=>'form-horizontal','id'=>'form-add')) !!}
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">New Membership Level</h4>
            </div>
            <div class="modal-body">
              
                  <div class="box-body">
                    <div class="form-group">
                       {!! Form::label('membership_name', 'Membership Name:',['class'=>'col-sm-4 control-label']) !!}
                        <div class="col-sm-6">                            
                            {!! Form::text('membership_name', '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div> 
                  </div>
                  <div class="box-body">
                    <div class="form-group">
                       {!! Form::label('frequency', 'Frequency:',['class'=>'col-sm-4 control-label']) !!}
                        <div class="col-sm-6">                            
                            {!! Form::select('frequency', array('monthly'=>'Monthly','annually'=>'Annually'), '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div> 
                  </div>
                  <div class="box-body">
                    <div class="form-group">
                       {!! Form::label('fee', 'Fee ($):',['class'=>'col-sm-4 control-label']) !!}
                        <div class="col-sm-6">                            
                            {!! Form::text('fee', '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div> 
                  </div>

              
            </div>
            <div class="modal-footer">
             
              <button type="submit" class="btn btn-primary">Save New Membership Level</button>
            </div>
          </div>
          {!! Form::close() !!}
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

@endsection