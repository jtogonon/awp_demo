  <div class="form-group">
    <label for="name" class="col-sm-2 control-label">Membership Name</label>
    <div class="col-sm-10">
      {!! Form::text('membership_name', $membership->membership_name, ['class'=>'form-control','id'=>'membership_name']) !!}
    </div>
  </div>
 

 <div class="form-group">
    <label for="name" class="col-sm-2 control-label">Fee</label>
    <div class="col-sm-10">
      {!! Form::text('fee', $membership->fee, ['class'=>'form-control','id'=>'membership_name']) !!}
    </div>
  </div>

  <div class="form-group">
    <label for="name" class="col-sm-2 control-label">Frequency</label>
    <div class="col-sm-10">
      {!! Form::select('frequency', $frequencies, $membership->frequency, ['class'=>'form-control','id'=>'membership_name']) !!}
    </div>
  </div>
 
  
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-success">Save Changes</button>
    </div>
  </div>