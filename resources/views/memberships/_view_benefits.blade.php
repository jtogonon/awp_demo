<div class="row content">
	<div class="row">
		

		<div class="col-md-12">
			<p class="text-green">Services your customers will enjoy on this Membership.</p>
			<div class="box box-warning direct-chat direct-chat-warning collapsed-box">
				<div class="box-header with-border">					
					<h3 class="box-title">Services Available</h3>
					<div class="box-tools pull-right">		                
		                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
		                </button>		                		                
		              </div>
				</div>
				<div class="box-body" style="display:none">
					{!! Form::model(new App\Models\Membership, array('url' => 'memberships/saveItems', 'method' => 'post','class'=>'form-horizontal','id'=>'form-membership')) !!}

						<h5 style="padding-left:20px;">Drag items from the list to the area below.</h5>
						<div id="shop_services_list" class="col-md-3">
							<ul class="membershipItems vertical">
								<?php
								if (count($services) > 0 ):
									 foreach ($services as $serv): ?>
								<li data-id="<?=$serv->id?>" data-name="<?=$serv->servicename?>"><?=$serv->servicename?></li>
								<?php endforeach; 
								endif; ?>
							</ul>

						</div>
					{!! Form::close() !!}
				</div>
			</div>		
		</div>
	</div>

	<div class="row col-md-12 membershipItems" id="final_output_list" data="<?=$membership->id?>" style="margin-left:2px;">
		
		
	</div>
</div><!-- end row -->