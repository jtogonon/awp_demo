<?php
use App\Helpers\Helper;
?>
@extends('master')

@section('content')
  	 <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Expenses     
        </h1>
        <div class="pull-right" style="padding:10px;">
          <a href="" class="btn btn-block btn-success btn_add" id="btn_addexpense">Add New Expense</a>
        </div>
      </section>

      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">List</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table id="exp_tbl" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>Name</th>
                        <th>Amount</th>                        
                        <th>Category</th>                        
                        <th>Date</th>   
                        <th>Actions</th>   
                      </tr>
                      </thead>
                      <tbody>
                        <?php 
                        foreach ($expenses as $exp): ?>
                        <tr>
                          <td><?=$exp->name?></td>
                          <td>{{$shopcurrencysymbolhtml}}<?=$exp->amount?></td>                          
                          <td><?=$exp->getCategory()?></td>   
                          <td><?=Helper::formatDate($exp->expense_date,5)?></td>                   
                          <td><!-- <a  class="btn btn-flat" title="Edit">
                                <i class="fa fa-edit"></i>
                              </a> -->
                               <a href="/expenses/delete/<?=$exp->id?>" class="btn btn-flat btn_delete">
                                <i class="fa fa-remove"></i>
                              </a>
                              
                              

                            </td>
                         
                        </tr>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
            </div><!-- end box -->
          </div><!-- end col-xs-12 -->
        </div><!-- end row -->

      </section>


      <div class="modal fade" tabindex="-1" role="dialog" id="modal-add-user">
        <div class="modal-dialog">
          {!! Form::model(new App\Models\Expense, array('url' => 'expenses/save', 'method' => 'post','class'=>'form-horizontal','id'=>'form-add')) !!}
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Add New Expenses</h4>
            </div>
            <div class="modal-body">
              
                  <div class="box-body">
                    <div class="form-group">
                       {!! Form::label('name', 'Name:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-6">                            
                            {!! Form::text('name', '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div> 
                  </div>
                  <div class="form-group">
                     {!! Form::label('expense_category_id', 'Category:',['class'=>'col-sm-3 control-label']) !!}
                      <div class="col-sm-6">
                          {!! Form::select('expense_category_id', $expCategories,'', ['class'=>'form-control']) !!}                 
                      </div>
                  </div>
                  <div class="form-group">
                     {!! Form::label('vendor_id', 'Vendor:',['class'=>'col-sm-3 control-label']) !!}
                      <div class="col-sm-6">
                          {!! Form::select('vendor_id', $vendors,'', ['class'=>'form-control']) !!}
                      </div>
                  </div>
                  <div class="box-body">
                    <div class="form-group">
                       {!! Form::label('amount', 'Amount ($):',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-6">                            
                            {!! Form::text('amount', '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div> 
                  </div>

                  <div class="box-body">
                    <div class="form-group">
                       {!! Form::label('fee', 'Date:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-6">                            
                            {!! Form::text('expense_date', '', ['class'=>'form-control datepicker','id'=>'']) !!}                            
                        </div>
                    </div> 
                  </div>

                  <div class="box-body">
                    <div class="form-group">
                       {!! Form::label('description', 'Description:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-8">                            
                            {!! Form::textarea('description', '', ['class'=>'form-control','rows'=>'3']) !!}                            
                        </div>
                    </div> 
                  </div>

              
            </div>
            <div class="modal-footer">
             
              <button type="submit" class="btn btn-primary">Save New Expense</button>
            </div>
          </div>
          {!! Form::close() !!}
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

@endsection