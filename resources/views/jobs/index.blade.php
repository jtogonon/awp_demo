<?php
use App\Helpers\Helper;
?>
 @extends('master')

@section('content')
  	 <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Jobs
          <small>Latest</small>
        </h1>
        <div class="pull-right" style="padding:10px;">
          <a href="jobs/new" class="btn btn-block btn-success " id="btn_addexpense">Add New Job</a>
        </div>
      </section>

      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">All Jobs</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table id="jobs_tbl" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th width="10%">Job ID</th>
                        <th>Customer</th>
                        <th>Car</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th>Action</th>   
                      </tr>
                      </thead>
                      <tbody>
                        <?php 
                        if (Session::get('timezone')):
                            $timezone = Session::get('timezone');
                            
                          else:
                              $timezone = 'America/Los_Angeles';
                          endif;
                          //echo $timezone;
                        foreach ($jobs as $job): 
                          $class = App\Models\JobStatus::getStatusClass($job->status_id);
                          
                          $start_date = Timezone::convertFromUTC($job->start_date, $timezone, 'M j, Y');
                          ?>
                        <tr>
                          <td><a href="/jobs/view/<?=$job->id?>"><?=$job->id?></a></td>
                          <td><a href="/users/view/<?=$job->user_id?>"><?=$job->getCustomer()?></a></td>
                          <td><a href="/cars/view/<?=$job->car_id?>"><?=$job->getCar()?></a></td>
                          <td align="center"><span class="label label-<?=$class?>"><?=$job->getStatus()?></span></td>
                          <td><?=$start_date?></td>         
                          <td><a  class="btn btn-flat" title="Edit">
                                <i class="fa fa-edit"></i>
                              </a>
                              <a href="/jobs/delete/<?=$job->id?>" class="btn btn-flat btn_delete">
                                <i class="fa fa-remove"></i>
                              </a>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
            </div><!-- end box -->
          </div><!-- end col-xs-12 -->
        </div><!-- end row -->

      </section>

@endsection