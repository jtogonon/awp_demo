<!-- The timeline -->
  

  	<div class="box box-warning direct-chat direct-chat-warning" id="view_job_messages">
        <div class="box-header with-border">
          <h3 class="box-title">Direct Chat</h3>

          <div class="box-tools pull-right">
            <span data-toggle="tooltip" title="3 New Messages" class="badge bg-yellow">3</span>
            
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <!-- Conversations are loaded here -->
          <div class="">
            	

          </div>
          <!--/.direct-chat-messages-->

          
          </div>
          <!-- /.direct-chat-pane -->
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          {!! Form::model(new App\Models\Message, array('url' => '/messages/send', 'method' => 'post','class'=>'form-horizontal','id'=>'job_message_form')) !!}
            {!! Form::hidden('job_id', $job->id,['id'=>'job_id','file'=>true]) !!}                    
            {!! Form::hidden('car_id', $job->car_id,['id'=>'car_id']) !!}    
            {!! Form::hidden('user_id', $job->user_id,['id'=>'user_id']) !!}    
            <div class="input-group">
              <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                  <span class="input-group-btn">
                    <button type="submit" type="button" class="btn btn-warning btn-flat">Send</button>
                  </span>
            </div>
           {!! Form::close() !!}
        </div>
        <!-- /.box-footer-->