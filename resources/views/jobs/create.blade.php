<?php
use App\Helpers\Helper;
?>
 @extends('master')

@section('content')
  	 <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Jobs
         
        </h1>
        <div class="pull-right" style="padding:10px;">
          <a href="/jobs" class="btn btn-block btn-default " id="btn_addexpense">Back to Jobs</a>
        </div>
      </section>

      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title">Create New Job</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                     {!! Form::model(new App\Models\Service, array('url' => 'jobs/save', 'method' => 'post','class'=>'form-horizontal','id'=>'form-add')) !!}
                      <div class="form-group">
                       {!! Form::label('user_id', 'Customer:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-md-4">                            
                            {!! Form::select('user_id', $customers, $user_id, ['class'=>'form-control select2','id'=>'user_id','placeholder'=>'Select Customer']) !!}                            
                        </div>
                        <div class="col-md-2">
                          <a href="" class="btn btn-primary btn-xs btn_add" id="btn_adduser">Add Customer</a>
                        </div>
                      </div> 

                      <div class="form-group">
                       {!! Form::label('user_id', 'Car:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-md-4">                            
                            {!! Form::select('car_id', $cars, $car_id, ['class'=>'form-control select2 ','id'=>'car_id']) !!}                            
                        </div>
                        <div class="col-md-2">
                          <a href="" class="btn btn-primary btn-xs btn_add_car_to_user">Add Car</a>
                        </div>
                      </div> 

                      <!-- <div class="form-group">
                       {!! Form::label('job_name', 'Job Name:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-md-4">                            
                            {!! Form::text('job_name', '', ['class'=>'form-control','id'=>'job_name']) !!}                            
                        </div>                        
                      </div>  -->

                      <div class="form-group">
                       {!! Form::label('services', 'Services to apply on this Job:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-md-6">                            
                            {!! Form::select('services[]', $services,'', ['class'=>'form-control select2','multiple'=>true,'width'=>'75%']) !!}                            
                        </div>
                        
                      </div> 

                      <div class="form-group">
                       {!! Form::label('start_date', 'Start Date:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="input-group col-md-4" style="padding-left:14px;">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>                          
                          {!! Form::text('start_date', $curDate, ['class'=>'form-control datepicker','id'=>'']) !!}    
                        </div>                        
                      </div> 

                      <div class="form-group ">
                       {!! Form::label('start_date', 'Start Time:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="input-group col-md-4 bootstrap-timepicker" style="padding-left:14px;">
                          <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                          </div>                          
                          {!! Form::text('start_time', '', ['class'=>'form-control timepicker','id'=>'']) !!}    
                        </div>                        
                      </div> 

                      <div class="form-group ">
                       {!! Form::label('status', 'Status:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-md-4 ">                                              
                          {!! Form::select('status_id', $statuses ,'', ['class'=>'form-control','id'=>'']) !!}    
                        </div>                        
                      </div> 

                      <div class="form-group">
                       {!! Form::label('notes', 'Notes:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-md-6">                            
                            {!! Form::textarea('notes', '', ['class'=>'form-control','rows'=>3]) !!}
                        </div>                        
                      </div> 

                      <div class="form-group">

                        <label class="col-sm-3 control-label">&nbsp;</label>
                          <div class="col-md-6 ">
                            <button type="submit"  value="" class="btn btn-success">Save New Job</button>&nbsp; <a href="/jobs" class="btn btn-default">Cancel</a>
                        </div>
                      </div>


                     {!! Form::close() !!}
                  </div>
            </div><!-- end box -->
          </div><!-- end col-xs-12 -->
        </div><!-- end row -->

      </section>


      <div class="modal fade" tabindex="-1" role="dialog" id="modal-add-user">
        <div class="modal-dialog">
          {!! Form::model(new App\User, array('url' => 'users/save', 'method' => 'post','class'=>'form-horizontal','id'=>'form-add-user')) !!}
          {!! Form::hidden('for_job_form','1')!!}
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Add New Customer</h4>
            </div>
            <div class="modal-body">
              
                  <div class="box-body">
                    <div class="form-group">
                       {!! Form::label('firstname', 'First Name',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">                            
                            {!! Form::text('firstname', '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div>
                    <div class="form-group">
                       {!! Form::label('lastname', 'Last Name',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">                            
                            {!! Form::text('lastname', '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div>
                    <div class="form-group">
                       {!! Form::label('email', 'Email',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">                            
                            {!! Form::email('email', '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div>
                    <div class="form-group">
                       <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-10 checkbox">                            
                            {!! Form::checkbox('sendwelcomeemail', '', ['class'=>'form-control','id'=>'']) !!}     
                            Send Welcome Email                       
                        </div>
                    </div>
                  </div>

              
            </div>
            <div class="modal-footer">
             
              <button type="submit" class="btn btn-primary">Save New Customer</button>
            </div>
          </div>
          {!! Form::close() !!}
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


      <div class="modal fade" tabindex="-1" role="dialog" id="modal-add-car">
        <div class="modal-dialog">
          {!! Form::model(new App\Models\Car, array('url' => 'cars/save', 'method' => 'post','class'=>'form-horizontal','id'=>'form-add')) !!}
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Add New Car</h4>
            </div>
            <div class="modal-body">
              
                  <div class="box-body">
                    <div class="form-group">
                       {!! Form::label('user_id', 'Customer:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-8">                            
                            {!! Form::select('user_id', $customers, $user_id, ['class'=>'form-control ','disabled'=>true,'style'=>'width:75%;']) !!}       
                        </div>
                    </div> 
                 
                    <div class="form-group">
                       {!! Form::label('model', 'Model Variant:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-8">                            
                            {!! Form::text('model', '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div> 

                    <div class="form-group">
                       {!! Form::label('plate_number', 'Plate Number:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-8">                            
                            {!! Form::text('plate_number', '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div> 

                    <div class="form-group">
                       {!! Form::label('manufacturer_id', 'Manufacturer:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-8">                            
                            {!! Form::select('manufacturer_id', $manufacturers, '', ['class'=>'form-control select2','style'=>'width:75%;']) !!}       
                        </div>
                    </div> 

                  </div>

              
            </div>
            <div class="modal-footer">
             
              <button type="submit" class="btn btn-primary">Save New Car</button>
            </div>
          </div>
          {!! Form::close() !!}
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

@endsection