<?php

use App\Helpers\Helper;
?>
@extends('master')

@section('content')
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Job Details :  <?php echo $job->job_name;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="/jobs">Jobs</a></li>
        <li class="active">Job Details</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">
          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <?php if (!empty($car)): ?>
              <a href="/cars/view/<?=$car->id?>"><img class="profile-user-img img-responsive img-circle" src="{{$carpic}}" alt="Car Image"></a>
              <?php endif; ?>

              <?php if (!empty($car)): ?>
                <h3 class="profile-username text-center"><?php echo $job->getCar();?></h3>
              <?php else: ?>
                <h3 class="profile-username text-center">No Car</h3>
              <?php endif; ?>

              <?php if (!empty($car)): ?>
              <p class="text-muted text-center"><a href='/users/view/<?php echo $job->getCustomerID();?>'><?php echo $job->getCustomer();?></a></p>
              <?php endif; ?>

              <?php if (!empty($car)): ?>
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Date Created</b> <a class="pull-right"><?php echo Helper::formatDate($job->created_at,5);?></a>
                </li>
                <li class="list-group-item">
                  <b>Images</b> <a class="pull-right"><?php echo $job->getImagesCount();?></a>
                </li>
                <?php if ($job->date_completed!=''): ?>
                <li class="list-group-item">
                  <b>Finished</b> <a class="pull-right"><?php echo Helper::formatDate($job->date_completed,5);?></a>
                </li>
              <?php endif; ?>
              </ul>
              <?php endif; ?>
                 <label>Status:</label><div class="btn-group col-md-12 text-center" style="padding-left:50px;">
               
                  <button type="button" class="btn btn-<?=$statClass?> "><?php echo $job->getStatus();?></button>
                  <button type="button" class="btn btn-<?=$statClass?> dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <?php foreach ($statuses as $s=>$stat): ?>
                    <li ><a href="/jobs/changeStatus/<?=$job->id?>/<?=$s?>" class="btn_changeStat"><?=$stat?></a></li>
                    <?php endforeach; ?>                    
                  </ul>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About the Job</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            
              <strong><i class="fa fa-pencil margin-r-5"></i> Services</strong>

              <p>
                <?php 
                $colorItems = array('info','success','danger','warning');
                foreach ($jobservices as $servi => $serv): 
                      $sclass = $colorItems[mt_rand(0,3)];
                ?>
                <span class="label label-<?=$sclass?>"><?=$serv?></span>
                <?php endforeach; ?>                
              </p>

              <hr>

              <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

              <p><?php echo $job->notes;?></p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">              
              <li class="active"><a href="#timeline" data-toggle="tab">Timeline</a></li>
              <li><a href="#details" data-toggle="tab">Details</a></li>
              <li><a href="#pictures" data-toggle="tab">Pictures</a></li>
              <li><a href="#tab_messages" data-toggle="tab">Messages</a></li>
            </ul>
            <div class="tab-content">
              
              <div class="tab-pane active" id="timeline">
                  @include('jobs/_view_timeline')
              </div>

              <div class="tab-pane" id="details">
                {!! Form::model(new App\Models\Job, array('url' => '/jobs/save', 'method' => 'post','class'=>'form-horizontal form_details','id'=>'form-profile')) !!}
                    {!! Form::hidden('id', $job->id,['id'=>'job_id']) !!}                    
                    {!! Form::hidden('user_id', $job->user_id,['id'=>'user_id']) !!}
                    {!! Form::hidden('car_id', $job->car_id,['id'=>'car_id']) !!}         
                    {!! Form::hidden('start_date', $job->start_date) !!}      
                    {!! Form::hidden('start_time', $job->start_time) !!}      
                    @include('jobs/_view_details')
                  {!! Form::close() !!}
              </div>

              <div class="tab-pane" id="pictures">
                {!! Form::model(new App\Models\File, array('url' => '/files/uploadjobpics', 'method' => 'post','class'=>'form-horizontal form_details','id'=>'fileupload')) !!}
                    {!! Form::hidden('job_id', $job->id,['id'=>'job_id','file'=>true]) !!}                    
                    {!! Form::hidden('car_id', $job->car_id,['id'=>'car_id']) !!}    
                    @include('jobs/_view_pictures')
                  {!! Form::close() !!}
              </div>

              <div class="tab-pane" id="tab_messages">
                
                    @include('jobs/_view_messages')
                 
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->

@endsection