    <div class="form-group">
      <label for="inputName" class="col-sm-2 control-label">Customer</label>

      <div class="col-sm-10">        
        {!! Form::text('customer', $job->getCustomer(), ['class'=>'form-control','disabled'=>'true']) !!}
      </div>
    </div>

    
    <div class="form-group">
      <label for="inputEmail" class="col-sm-2 control-label">Car</label>

      <div class="col-sm-10">
        {!! Form::text('car', $job->getCar(), ['class'=>'form-control','disabled'=>'true']) !!}
      </div>
    </div>

    <div class="form-group">
      <label for="inputEmail" class="col-sm-2 control-label">Job Name</label>

      <div class="col-sm-10">
        {!! Form::text('job_name', $job->job_name, ['class'=>'form-control',]) !!}
      </div>
    </div>
    
    <div class="form-group">
      <label for="inputEmail" class="col-sm-2 control-label">Notes</label>

      <div class="col-sm-10">
        {!! Form::textarea('notes', $job->notes, ['class'=>'form-control','rows'=>4]) !!}
      </div>
    </div>
    

    
    
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-success">Save Changes</button>
      </div>
    </div>
