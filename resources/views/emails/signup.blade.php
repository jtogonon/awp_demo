@extends('emailTemplate')

@section('content')
<tbody>
	<tr>
		<td style="padding:30px 0  0 0;">
						

				<p style="font-size: 13px;line-height:24px;font-family:'HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif;">
				<strong>Thanks for signing up with us. </strong><br/>
				<br/>
				Your Login Details:<br/>
				URL: <a href="<?php echo $publicUrl;?>/auth/login"><?php echo $publicUrl;?>/auth/login</a><br/>
				User: <?php echo $username;?> <br/>
				Password: (hidden) <br/>

				</p>
				<p style="font-size: 13px;line-height:24px;font-family:'HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif;">
				Here are 3 tips to help you get started with your account:</p>

				<ul>
					<li style="padding:3px;"><strong>Customers:</strong> Add your customers by going to People -> Customers tab on the left side nav bar. Click the button "Add New User" then a pop-up will appear. You can enter the customer's FirstName, LastName and Email.</li>
					<li style="padding:3px;"><strong>Add Cars:</strong> Go to People->Cars. Click the button "Add New Car". A pop-up will appear, select the Customer, enter Model and Plate Number.. and the Manufacturer. Click Save.</li>
					<li style="padding:3px;"><strong>Add Jobs:</strong> Select the Car in the People->Cars. In the Car Details page, click the big green button "Create New Job". This is useful for day to day business. You can easily view the jobs in the Job section or Calendar.</li>
					<li style="padding:3px;"><strong>Calendar:</strong> Schedule your customer's appointment in your shop and jobs for the day. </li>
				</ul>


				<br>
				<br>
				We are happy to receive any feedback and suggestions.
				<br/><br/>
				For any help getting setup, please contact <a href="mailto:support@autoworkspro.com">support@autoworkspro.com</a>.
				
				<br>
				<br>
				<p style="font-size: 13px;line-height:24px;font-family:'HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif;">
                Thank you,<br/>
				<strong>AutoWorksPro Team</strong><br/>
				<em>Manage your Automotive Business the Right way!</em>

				<br>
				</p>
		</td>
	</tr>
</tbody>

@endsection