@extends('emailTemplate')

@section('content')
<tbody>
	<tr>
		<td style="padding:30px 0  0 0;">
						

				<p style="font-size: 13px;line-height:24px;font-family:'HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif;">
				<strong>Welcome to <?php echo $shopName;?>. </strong><br/>

				<p>We have created your account so you can easily manage your </p>
				<br/>
				Your Login Details:<br/>
				URL: <a href="<?php echo $publicUrl;?>/auth/login"><?php echo $publicUrl;?>/auth/login</a><br/>
				User: <?php echo $username;?> <br/>
				Password: (hidden) <br/>

				</p>
				<p style="font-size: 13px;line-height:24px;font-family:'HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif;">
				Here are 3 tips to help you get started with your account:</p>

				<ul>
					<li style="padding:3px;"><strong>Customers:</strong> Add your customers by clicking the Customers tab on the left side nav bar. When you add new customers, you can send them an email to let them know about AutoWorksPro. The system will automatically create a username and password for them.</li>
					<li style="padding:3px;"><strong>Add Jobs:</strong> Click the Add Job button in the Dashboard. This is useful for day to day business. You can easily view the jobs in the Job section or Calendar.</li>
					<li style="padding:3px;"><strong>Calendar:</strong> Schedule your customer's appointment in your shop. </li>
				</ul>


				<br>
				<br>
				We are still in Beta Version. But we would be happy to receive any feedback and suggestions.
				<br/><br/>
				For any help getting setup, please contact <a href="mailto:support@autoworkspro.com">support@autoworkspro.com</a>.
				
				<br>
				<br>
				<p style="font-size: 13px;line-height:24px;font-family:'HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif;">
                Thank you,<br/>
				<strong>AutoWorksPro Team</strong><br/>
				<em>The Best Way to Manage your Auto Works Business!</em>

				<br>
				</p>
		</td>
	</tr>
</tbody>

@endsection