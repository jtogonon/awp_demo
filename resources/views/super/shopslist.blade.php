<?php
use App\Helpers\Helper;
?>
 @extends('master')

@section('content')
  	 <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Shops
          <small>Latest</small>
        </h1>
        <div class="pull-right" style="padding:10px;">
          <a href="jobs/new" class="btn btn-block btn-success " id="btn_addexpense">Add New Shop</a>
        </div>
      </section>

      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">All Shops</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table id="jobs_tbl" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>Shop Name</th>
                        <th>Login As</th>
                        <th>Account Type</th>
                        <th>Date</th>
                        <th>Action</th>   
                      </tr>
                      </thead>
                      <tbody>
                        <?php 
                        foreach ($shops as $shop): ?>
                        <tr>
                          <td><a href="/super/loginAs/<?=$shop->user_id?>"><?=$shop->name?></a></td>
                          <td><a href="/super/loginAs/<?=$shop->user_id?>"><?=$shop->getCustomer()?></a></td>
                          <td><?=$shop->account_type?></td>
                          <td><?=Helper::formatDate($shop->created_at,5)?></td>         
                          <td><a  class="btn btn-flat" title="View" href="/super/viewShop/<?=$shop->id?>">
                                <i class="fa fa-edit"></i>
                              </a>
                              <a href="/super/shopdelete/<?=$shop->id?>" class="btn btn-flat btn_delete">
                                <i class="fa fa-remove"></i>
                              </a>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
            </div><!-- end box -->
          </div><!-- end col-xs-12 -->
        </div><!-- end row -->

      </section>

@endsection