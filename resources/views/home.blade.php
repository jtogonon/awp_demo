@extends('frontend')

@section('content')
<section id="features" class="container services">
    <div class="row">
        <div class="col-sm-3">
            <h2>In the Cloud</h2>
            <p>Our system is using cloud computing.. so you can access your data anytime, anywhere. </p>
            
        </div>
        <div class="col-sm-3">
            <h2>Awesome Features!</h2>
            <p>We are on a mission to create awesome features specifically for auto repair shops and car maintenance businesses like car wash or car repair. </p>
            
        </div>
        <div class="col-sm-3">
            <h2>Always improving</h2>
            <p>We are committed to improve our software for your business. We are open for any suggestions and feature requests. </p>
            
        </div>
        <div class="col-sm-3">
            <h2>Integrated</h2>
            <p>We have integrated social media like Facebook and Twitter. More integrations will be added like Mailchimp or Aweber.</p>
            
        </div>
    </div>
</section>

<section  class="container features ">
    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="navy-line"></div>
            <h1>The Perfect Software with great features<br/> <span class="navy">  for your AUTO SHOP, CAR WASH or AUTO DETAILING BUSINESS</span> </h1>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 text-center wow fadeInLeft">
            <div>
                <i class="fa fa-mobile features-icon"></i>
                <h2>Full responsive</h2>
                <p>Our application can be viewed using your mobile phone, tablet or desktop.</p>
            </div>
            <div class="m-t-lg">
                <i class="fa fa-bar-chart features-icon"></i>
                <h2>Charts</h2>
                <p>Beautiful charts and statistics to see how your business is doing and how much growth you made.</p>
            </div>
        </div>
        <div class="col-md-6 text-center  wow zoomIn">
            <img src="img/perspective.png" alt="dashboard" class="img-responsive">
        </div>
        <div class="col-md-3 text-center wow fadeInRight">
            <div>
                <i class="fa fa-envelope features-icon"></i>
                <h2>Messages</h2>
                <p>Communicate with your customers effectively. We have an internal messaging system. Talking about the jobs in the shop!</p>
            </div>
            <div class="m-t-lg">
                <i class="fa fa-google features-icon"></i>
                <h2>Integration</h2>
                <p>We have integrated our system on various platforms such as Google, Facebook, Twitter, MailChimp, and more to come.</p>
            </div>
        </div>
    </div>
</section>    
<section class="gray-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Discover how your business is doing</h1>
                <p>We understand as business owner, you want to quickly see an overview of everything. </p>
            </div>
        </div>
        <div class="row features-block">
            <div class="col-lg-6 features-text wow fadeInLeft">
                <small>AUTOWORKSPRO</small>
                <h2>Perfectly designed </h2>
                <p>AutoWorksPro is perfectly designed application for businesses like car wash, auto detailers, auto shop. Stunning dashboard has been created for you to have a quick look of your business.</p>
                <a class="btn btn-primary btn-xs" href="/auth/register" >Sign up now</a>
            </div>
            <div class="col-lg-6 text-right wow fadeInRight">
                <img src="img/dashboard.png" alt="dashboard" class="img-responsive pull-right">
            </div>
        </div>
    </div>
</section>



<section class="features">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Even more great features</h1>
                <p>Still not convinced?</p>
            </div>
        </div>
        <div class="row features-block">
            <div class="col-lg-3 features-text wow fadeInLeft">
                <small>Optimized</small>
                <h2>Latest Technology for Modern Automotive Businesses </h2>
                <p>We have created AutoWorkPro system for your business so you can maximise the potential of today's latest technology.</p>
                
            </div>
            <div class="col-lg-6 text-right m-t-n-lg wow zoomIn">
                <img src="img/iphone.jpg" class="img-responsive" alt="dashboard">
            </div>
            <div class="col-lg-3 features-text text-right wow fadeInRight">
                <small>Modern Systems</small>
                <h2>Engineered with Your Business in Mind </h2>
                <p>AutoWorksPro is a premium software as a service that is focused on how your business will be more profitable and efficiently manage your customers and jobs in a day-to-day business. </p>
                
            </div>
        </div>
    </div>

</section>

<section id="testimonials" class="navy-section testimonials">

    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center wow zoomIn">
                <i class="fa fa-comment big-icon"></i>
                <h1>
                    What our users say
                </h1>
                <div class="testimonials-text">
                    <i>"Awesome! I've been looking for a software like this for many years.. Very useful in my auto detailing business. Highly recommend."</i>
                </div>
                <small>
                    <strong>- Paul Wester</strong>
                </small>
            </div>
        </div>
    </div>

</section>

<section id="pricing" class="pricing">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Pricing</h1>
                <p>Our prising is very simple.. You can even use our app for FREE.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 wow zoomIn">
                <ul class="pricing-plan list-unstyled">
                    <li class="pricing-title">
                        Basic
                    </li>
                    <li class="pricing-desc">
                       No obligation whatsoever. 
                    </li>
                    <li class="pricing-price">
                        <span>Free</span>
                    </li>
                    <li>
                        SSL Encryption
                    </li>
                    <li>
                        <strong>Contacts &amp; Membership Management</strong>
                    </li>
                    <li>
                        Calendar, Jobs, Photo Gallery
                    </li>
                    <li>
                        <strong>Invoicing</strong>
                    </li>
                    <li>
                        50 Customers
                    </li>
                    
                   
                    <li>
                        <a class="btn btn-primary btn-xs" href="/auth/register" id="">Signup</a>
                    </li>
                </ul>
            </div>

            <div class="col-lg-4 wow zoomIn">
                <ul class="pricing-plan list-unstyled selected">
                    <li class="pricing-title">
                        Standard
                    </li>
                    <li class="pricing-desc">
                        Most popular to grow your business.
                    </li>
                    <li class="pricing-price">
                        <span>$15</span> / month
                    </li>
                    <li>
                         SSL Encryption
                    </li>
                    <li>
                        <strong>Contact &amp; Membership Management</strong>
                    </li>
                    <li>
                        Gallery, Jobs, Calendar
                    </li>
                    <li>
                        <strong>Email Broadcasting</strong>
                    </li>
                                   
                    
                    <li>
                        500 Paying Customers
                    </li>
                    <li>
                       <strong> 1 Admin User</strong>
                    </li>
                    <li class="plan-action">
                        <a class="btn btn-primary btn-xs" href="/auth/register">Signup</a>
                    </li>
                </ul>
            </div>

            <div class="col-lg-4 wow zoomIn">
                <ul class="pricing-plan list-unstyled">
                    <li class="pricing-title">
                        Pro
                    </li>
                    <li class="pricing-desc">
                        <em>Coming Soon</em>
                    </li>
                    <li class="pricing-price">
                        <span>$25</span> / month
                    </li>
                    <li>
                        SSL Encryption
                    </li>
                    <li>
                        <strong>Contacts &amp; Membership Management</strong>
                    </li>
                    <li>
                       Gallery, Calendar, Jobs, <br/>Advanced Email Marketing
                    </li>
                    <li>
                       <strong> Invoicing, Social Media Integration</strong>
                    </li> 
                    <li>
                        Premium Support
                    </li>
                    <li>
                       <strong> Unlimited Customers</strong>
                    </li>
                    <li>
                        1 Admin User / 1 Manager
                    </li>
                    <li>
                        Coming Soon
                    </li>
                </ul>
            </div>
        </div>
        <div class="row m-t-lg">
            <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg">
                <h3>Sign up now for FREE. <br/>No Credit Card required. Cancel anytime.</h3>
            </div>
        </div>
    </div>

</section>

<section id="contact" class="gray-section contact">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Contact Us</h1>
                <p>Need help about our software?</p>
            </div>
        </div>
        <div class="row m-b-lg">
            <div class="col-lg-3 col-lg-offset-3">
                <address>
                    <strong><span class="navy">AutoWorksPro </span></strong><br/>
                    Cottonwood Hts.<br/>
                    Antipolo City, Rizal<br/>
                    <abbr title="Phone">P:</abbr> (02) 952-9950
                </address>
            </div>
            <div class="col-lg-4">
                <p class="text-color">
                    Our Help Center is at <a href="http://autoworkspro.uservoice.com" target="_blank">AutoWorksPro.uservoice.com</a>. You can open a ticket whenever you need help on using our system. Feature requests are welcomet too.
                    Send us an email at <a href="mailto:support@autoworkspro.com">support@autoworkspro.com</a>. We can also be reached through our Twitter account and Facebook page.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <a href="mailto:support@autoworkspro.com" class="btn btn-primary">Send us mail</a>
                <p class="m-t-sm">
                    Or follow us on social platform
                </p>
                <ul class="list-inline social-icon">
                    <li><a href="https://twitter.com/autoworkspro" target="_blank"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li><a href="https://www.facebook.com/autoworkspro" target="_blank"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li><a href="https://plus.google.com/+AutoWorksPro-Web-based-Auto-Shop-Management" target="_blank"><i class="fa fa-google-plus"></i></a>
                    </li>
                </ul>
            </div>
        </div>
</section>        





@endsection
