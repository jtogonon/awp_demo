<?php
use App\Helpers\Helper;
?>
@extends('master')

@section('content')
  	 <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Vendors
        </h1>
        <div class="pull-right" style="padding:10px;">
          <a href="" class="btn btn-block btn-success btn_add" id="btn_addproduct">Add New Product</a>
        </div>
      </section>

      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">List</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table id="products_tbl" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>Product</th>
                        <th>SKU</th>
                        <th>Manufacturer</th>
                        <th>Price</th>
                        <th>Actions</th>   
                      </tr>
                      </thead>
                      <tbody>
                        <?php 
                        foreach ($products as $prod): ?>
                        <tr>
                          <td><?=$prod->product_name?></td>
                          <td><?=$prod->sku?></td>
                          <td><?=$prod->getManufacturer()?></td>
                          <td>{{$shopcurrencysymbolhtml}}<?=$prod->unit_price?></td>
                          
                          <td><a  class="btn btn-flat" title="Edit">
                                <i class="fa fa-edit"></i>
                              </a>
                              <a href="/products/delete/<?=$prod->id?>" class="btn btn-flat btn_delete">
                                <i class="fa fa-remove"></i>
                              </a>

                          </td>
                         
                        </tr>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
            </div><!-- end box -->
          </div><!-- end col-xs-12 -->
        </div><!-- end row -->

      </section>



      <div class="modal fade" tabindex="-1" role="dialog" id="modal-add-user">
        <div class="modal-dialog">
          {!! Form::model(new App\Models\Product, array('url' => 'products/save', 'method' => 'post','class'=>'form-horizontal','id'=>'form-add')) !!}
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Add New Product</h4>
            </div>
            <div class="modal-body">
              
                  <div class="box-body">
                    <div class="form-group">
                       {!! Form::label('product_name', 'Product Name',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">                            
                            {!! Form::text('product_name', '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div>
                    <div class="form-group">
                       {!! Form::label('sku', 'SKU',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">                            
                            {!! Form::text('sku', '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div>
                    <div class="form-group">
                       {!! Form::label('unit_price', 'Unit Price',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">                            
                            {!! Form::text('unit_price', '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div>
                    <div class="form-group">
                       {!! Form::label('in_stock', 'How many in stock?',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">                            
                            {!! Form::text('in_stock', '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div>
                    <div class="form-group">
                       {!! Form::label('manufacturer_id', 'Product Manufacturer',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">                            
                            {!! Form::select('manufacturer_id', $manufacturers,'', ['class'=>'form-control']) !!}                 
                        </div>
                    </div>
                    
                   
                  </div>

              
            </div>
            <div class="modal-footer">
             
              <button type="submit" class="btn btn-primary">Save New Product</button>
            </div>
          </div>
          {!! Form::close() !!}
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

@endsection