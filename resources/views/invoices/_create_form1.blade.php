    <div class="form-group">
      <label for="inputName" class="col-sm-2 control-label">Invoice Date</label>

      <div class="col-sm-3">        
        {!! Form::text('invoice_date', date('m/d/Y'), ['class'=>'form-control datepicker','id'=>'invoice_date']) !!}
      </div>

      <label for="inputName" class="col-sm-2 control-label">Invoice Due Date</label>

      <div class="col-sm-3">        
        {!! Form::text('due_date', date('m/d/Y'), ['class'=>'form-control datepicker','id'=>'due_date']) !!}
      </div>
    </div>

    <div class="form-group">
      <label for="inputName" class="col-sm-2 control-label">Customer</label>

      <div class="col-sm-3">        
        {!! Form::select('user_id', $customers,'', ['class'=>'form-control','id'=>'user_id']) !!}
      </div>

      <label for="inputName" class="col-sm-2 control-label">Invoice Number</label>

      <div class="col-sm-3">        
        {!! Form::text('invoicenumber', $invoicenumber, ['class'=>'form-control','id'=>'invoicenumber']) !!}
      </div>
    </div>