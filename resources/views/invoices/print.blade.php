<?php
use App\Helpers\Helper;
?>
@extends('print')

@section('content')

  <!-- Main content -->
  <section class="invoice">

        <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> <?=$auth->getShop()->name?>
            <small class="pull-right">Date: <?=Helper::formatDate($invoice->invoice_date,6)?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong><?=$auth->getShop()->name?></strong><br>
            <?=$auth->shop->address1?>, <?=$auth->shop->address2?><br>
            <?=$auth->shop->city?>, <?=$auth->shop->state?> <?=$auth->shop->zip?><br>
            Phone: <?=$auth->shop->phone?><br>
            Email: <?=$auth->shop->shop_email?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong><?=$invoice->getCustomer()?></strong><br>
            <?php if ($invoice->getCustomerAddress()): ?>
              <?=$invoice->getCustomerAddress()->address1?>, <?=$invoice->getCustomerAddress()->address2?><br>
              <?=$invoice->getCustomerAddress()->city?>, <?=$invoice->getCustomerAddress()->state?> <?=$invoice->getCustomerAddress()->zip?><br>
                  
            Phone: <?=$invoice->getCustomerAddress()->mobile?><br>
            Email: <?=$invoice->getUser()->email?>
             <?php endif; ?>       
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <br>         
          <b>Invoice #<?=Helper::formatInvoiceNumber($invoice->invoicenumber)?></b><br/>       
          <b>Payment Due:</b> <?=Helper::formatDate($invoice->due_date,6)?><br>     
          <?php
          $statusClass = ($invoice->status=='Paid') ? 'success': 'warning';
          ?>    
          <b>Status:</b> <span class="label label-<?=$statusClass?>"><?=$invoice->status?></span><br/><br/> 
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

     
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Qty</th>
              <th>Service</th>              
              <th>Description</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
              <?php foreach ($invoice_items as $i): 
              ?>
            <tr>
              <td><?=$i->quantity?></td>
              <td><?=$i->getService()->servicename?></td>             
              <td><?=$i->description?></td>
              <td>{{$shopcurrencysymbolhtml}}<?=$i->unit_price?></td>
            </tr>
          <?php endforeach; ?>
            
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Payment Methods:</p>
          <img src="/img/credit/visa.png" alt="Visa">
          <img src="/img/credit/mastercard.png" alt="Mastercard">
          <img src="/img/credit/american-express.png" alt="American Express">
          <img src="/img/credit/paypal2.png" alt="Paypal">

          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Thank you for doing business with us. 
          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead danger">Amount Due {{$shopcurrencysymbolhtml}}<?=$invoice->balance_due?></p>

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td>{{$shopcurrencysymbolhtml}}<?=$invoice->subtotal?></td>
              </tr>
              <tr>
                <th>Tax</th>
                <td>{{$shopcurrencysymbolhtml}}<?=$invoice->tax?></td>
              </tr>             
              <tr>
                <th>Total:</th>
                <td>{{$shopcurrencysymbolhtml}}<?=$invoice->total?></td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      
    </section>
  <!-- /.content -->
@endsection