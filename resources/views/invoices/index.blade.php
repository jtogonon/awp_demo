<?php
use App\Helpers\Helper;
?>
@extends('master')

@section('content')
  	 <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Invoices
          <small>Outstanding</small>
        </h1>
        <!-- commented this, no manual creation of invoice for now.
        invoice are automatically created after a job is saved as DONE
         <div class="pull-right" style="padding:10px;">
          <a href="/invoices/new" class="btn btn-block btn-success" id="btn_addinvoice">Add New Invoice</a>
        </div> -->
      </section>

      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">List</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table id="invoice_tbl" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>Invoice #</th>
                        <th>Customer</th>
                        <th>Invoice Date</th>
                        <th>Status</th>       
                        <th>Balance Due</th>                       
                        <th>Actions</th>   
                      </tr>
                      </thead>
                      <tbody>
                        <?php 
                        foreach ($invoices as $inv): 
                          $class = ($inv->status=='Paid') ? 'success' : 'warning';
                        ?>
                        <tr>
                          <td><a href="/invoices/view/<?=$inv->invoice_id?>"><?=Helper::formatInvoiceNumber($inv->invoicenumber)?></a></td>
                          <td><a href="/users/view/<?=$inv->user_id?>"><?=$inv->getCustomer()?></a></td>                          
                          <td><?=Helper::formatDate($inv->invoice_date,5)?></td>                   
                          <td><span class="label label-<?=$class?>"><?=$inv->status?></label></td>
                          <td>{{$shopcurrencysymbolhtml}}<?=$inv->balance_due?></td>    
                          <td><a href="/invoices/view/<?=$inv->invoice_id?>" class="btn btn-flat" title="Edit">
                                <i class="fa fa-edit"></i>
                              </a>
                              <a href="/invoices/delete/<?=$inv->invoice_id?>" class="btn btn-flat btn_delete">
                                <i class="fa fa-remove"></i>
                              </a>
                              
                              <a class="btn btn-flat" title="Send Invoice">
                                <i class="fa fa-envelope"></i>
                              </a>

                            </td>
                         
                        </tr>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
            </div><!-- end box -->
          </div><!-- end col-xs-12 -->
        </div><!-- end row -->

      </section>

@endsection