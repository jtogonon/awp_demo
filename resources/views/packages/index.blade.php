<?php
use App\Helpers\Helper;
?>
 @extends('master')

@section('content')
  	 <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Packages
          <small>Control panel</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
      </section>

      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">All Packages</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table id="cars_tbl" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>Package Name</th>
                        <th>Fee</th>
                        <th>Created</th>                        
                        <th></th>   
                      </tr>
                      </thead>
                      <tbody>
                        <?php 
                        foreach ($packages as $pak): ?>
                        <tr>
                          <td><?=$pak->package_name?></td>
                          <td><?=$pak->fee?></td>
                          <td><?=Helper::formatDate($pak->created_at,5)?></td>         
                          <td><a  class="btn btn-flat" title="Edit">
                                <i class="fa fa-edit"></i>
                              </a>
                              <a href="/packages/delete/<?=$pak->id?>" class="btn btn-flat btn_delete">
                                <i class="fa fa-remove"></i>
                              </a>

                          </td>
                         
                        </tr>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
            </div><!-- end box -->
          </div><!-- end col-xs-12 -->
        </div><!-- end row -->

      </section>

@endsection