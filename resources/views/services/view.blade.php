<?php
use App\Helpers\Helper;
?>
@extends('master')

@section('content')
  	 <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Services      
        </h1>
        <div class="pull-right" style="padding:10px;">
          <a href="/services" class="btn btn-block btn-success" id="btn_addservice">Back to Services</a>
        </div>
      </section>

      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">View Service Details</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="box-body">
                      {!! Form::model(new App\Models\Service, array('url' => 'services/save', 'method' => 'post','class'=>'form-horizontal','id'=>'form-add')) !!}
                        
                          {!! Form::hidden('id', $service->id, ['class'=>'form-control']) !!}                        

                        <div class="form-group">
                             {!! Form::label('name', 'Service Name:',['class'=>'col-sm-3 control-label']) !!}
                              <div class="col-md-4">
                                  
                                  {!! Form::text('servicename', $service->servicename, ['class'=>'form-control']) !!}
                                  
                              </div>                          
                        </div>



                        <div class="form-group">
                           {!! Form::label('fee', 'Fee:',['class'=>'col-sm-3 control-label']) !!}
                            <div class="col-md-4">
                                {!! Form::text('fee', number_format($service->fee, 2), ['class'=>'form-control','id'=>'payment_received']) !!}                 
                            </div>
                        </div>                    

                        
                        <div class="form-group">
                             {!! Form::label('description', 'Description:',['class'=>'col-sm-3 control-label']) !!}
                              <div class="col-md-4">                            
                                  {!! Form::textarea('description', $service->description, ['class'=>'form-control','rows'=>'3']) !!}                            
                              </div>
                        </div> 
                        <div class="form-group">
                             {!! Form::label('', '',['class'=>'col-sm-3 control-label']) !!}
                              <div class="col-md-4">                            
                                  <button type="submit"  value="" class="btn btn-success">Save Changes</button>&nbsp; <a href="/services" class="btn btn-default">Cancel</a>
                              </div>
                        </div>

                        {!! Form::close() !!}
                        
                    </div>
                  </div>
            </div><!-- end box -->
          </div><!-- end col-xs-12 -->
        </div><!-- end row -->

      </section>

@endsection