<?php
use App\Helpers\Helper;
?>
@extends('master')

@section('content')
  	 <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Services
        </h1>
        <div class="pull-right" style="padding:10px;">
          <a href="" class="btn btn-block btn-success btn_add" id="btn_addservice">Add New Service</a>
        </div>
      </section>

      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">List</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table id="services_tbl" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>Service Name</th>
                        <th>Fee</th>
                        
                        <th>Actions</th>   
                      </tr>
                      </thead>
                      <tbody>
                        <?php 
                        foreach ($services as $serv): ?>
                        <tr>
                          <td><a href="/services/view/<?=$serv->id?>"><?=$serv->servicename?></a></td>
                          <td>{{$shopcurrencysymbolhtml}}<?=round($serv->fee,2)?></td>
                          
                          
                          <td><a href="/services/view/<?=$serv->id?>" class="btn btn-flat" title="Edit">
                                <i class="fa fa-edit"></i>
                              </a>
                              <a href="/services/delete/<?=$serv->id?>" class="btn btn-flat btn_delete">
                                <i class="fa fa-remove"></i>
                              </a>

                          </td>
                         
                        </tr>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
            </div><!-- end box -->
          </div><!-- end col-xs-12 -->
        </div><!-- end row -->

      </section>


      <div class="modal fade" tabindex="-1" role="dialog" id="modal-add-user">
        <div class="modal-dialog">
          {!! Form::model(new App\Models\Service, array('url' => 'services/save', 'method' => 'post','class'=>'form-horizontal','id'=>'form-add')) !!}
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Add New Service</h4>
            </div>
            <div class="modal-body">
              
                  <div class="box-body">
                    <div class="form-group">
                       {!! Form::label('servicename', 'Service Name:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-8">                            
                            {!! Form::text('servicename', '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div> 
                  </div>
                  <div class="box-body">
                    <div class="form-group">
                       {!! Form::label('fee', 'Fee ($):',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-8">                            
                            {!! Form::text('fee', '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div> 
                  </div>

              
            </div>
            <div class="modal-footer">
             
              <button type="submit" class="btn btn-primary">Save New Service</button>
            </div>
          </div>
          {!! Form::close() !!}
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

@endsection