<?php
use App\Models\Country;
use App\Helpers\Helper;
?>
@extends('master')

@section('content')
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Upgrade My Account
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/dashboard">Dashboard</a></li>
        <li class="active">My Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="{{$userprofilepic}}" alt="User profile picture">

              <h3 class="profile-username text-center"><?php echo $user->firstname .' '. $user->lastname;?></h3>

              <p class="text-muted text-center"><?php echo $shop->name; ?></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Date Joined</b> <a class="pull-right"><?php echo Helper::formatDate($shop->created_at,5); ?></a>
                </li>
                <li class="list-group-item">
                  <b>Account Type</b> <a class="pull-right"><?php echo ucfirst($shop->account_type); ?></a>
                </li>
              
              </ul>

            
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
        </div>
        <!-- /.col -->
        <?php
        if ($type='pro'):
          $class_Pro = 'active';
          $class_Standard = '';
        else:
          $class_Pro = '';
          $class_Standard = 'active';
        endif;
        ?>
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">             
              <li class="<?=$class_Standard?>"><a href="#standard" data-toggle="tab">Upgrade to Standard</a></li>
              <li class="<?=$class_Pro?>"><a href="#pro" data-toggle="tab">Upgrade to PRO <i>(coming soon)</i></a></li>

            </ul>
            <div class="tab-content">
              <div class="tab-pane <?=$class_Standard?>" id="standard">

                <h3 class="red">Standard User</h3>
                <p>As Standard user you will enjoy the following features in AutoWorksPro.</p>
                <ul>
                  <li>$15 / month</li>
                  <li>SSL Encryption and all the current features you are enjoying in your Free Account</li>                  
                  <li>Email Broadcasting</li>
                  <li>500 Paying Customers - <em>get more profits</em></li>
                  <li>1 Admin User</li>
                </ul>
                <center>
                <a href="#" class="btn btn-primary btn-success btn-lg" style=""><b>Click Here to Upgrade Account as Standard User</b></a>
                <br/><small>Clicking the button above will take you to Paypal to create a subscription with $15 monthly payment. <br/>You can cancel anytime.</small>
                </center>
              
              
              </div>

              <div class="tab-pane <?=$class_Pro?>" id="pro">

                <h2>PRO</h2>
               
                <em>The PRO Account will be available soon.</em>
              </div>

             
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->

@endsection