<?php
use App\Helpers\Helper;
?>
@extends('master')

@section('content')
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Car Details
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/cars">Cars</a></li>
        <li class="active">Car Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-success">
            <div class="box-body box-profile">
              <a href="/cars/view/<?=$car->id?>"><img class="profile-user-img img-responsive img-circle" src="{{$carpic}}" alt="Car Image"></a>

              <h3 class="profile-username text-center"><?php echo $car->model;?></h3>

              <p class="text-muted text-center"><a href='/users/view/<?php echo $car->getCustomerId();?>'><?php echo $car->getCustomer();?></a></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Date Added</b> <a class="pull-right"><?php echo Helper::formatDate($car->created_at,5);?></a>
                </li>
                <li class="list-group-item">
                  <b>Serviced</b> <a class="pull-right"><?php echo $car->totalTimesServiced();?></a>
                </li>                
                
              </ul>

             <a href="/jobs/new/<?=$car->user_id?>/<?=$car->id?>" class="btn btn-success btn-block"><b>Create New Job</b></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About the Car</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body"> 
              <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

              <p><?=$car->notes?></p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">              
              <li class="active"><a href="#timeline" data-toggle="tab">Timeline</a></li>
              <li><a href="#details" data-toggle="tab">Details</a></li>
            </ul>
            <div class="tab-content">
              
              <div class="tab-pane active" id="timeline">
                @include('cars/_view_timeline')
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="details">
                {!! Form::model(new App\User, array('url' => '/cars/save', 'method' => 'post','class'=>'form-horizontal form_details','id'=>'form-profile')) !!}
                    {!! Form::hidden('id', $car->id,['id'=>'car_id']) !!}
                    {!! Form::hidden('user_id', $car->user_id) !!}
                  @include('cars/_view_details')
                  {!! Form::close() !!}
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->

@endsection