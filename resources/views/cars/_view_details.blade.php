<div class="form-group">
    <label for="inputName" class="col-sm-2 control-label">Owner</label>

    <div class="col-sm-10">
      <input type="owner" class="form-control" id="inputName" placeholder="Owner's Name" value="<?php echo $car->getCustomer();?>" disabled>
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail" class="col-sm-2 control-label">Model / Variant</label>

    <div class="col-sm-10">
      {!! Form::text('model', $car->model, ['class'=>'form-control','id'=>'']) !!}
    </div>
  </div>
  <div class="form-group">
    <label for="inputName" class="col-sm-2 control-label">Plate Number</label>

    <div class="col-sm-10">
      {!! Form::text('plate_number', $car->plate_number, ['class'=>'form-control','id'=>'plate_number']) !!}
    </div>
  </div>
  <div class="form-group">
    <label for="inputExperience" class="col-sm-2 control-label">Manufacturer</label>

    <div class="col-sm-10">
      {!! Form::select('manufacturer_id', $manufacturers, $car->manufacturer_id, ['class'=>'form-control','id'=>'']) !!}
    </div>
  </div>

  <div class="form-group">
    <label for="inputExperience" class="col-sm-2 control-label">Notes</label>

    <div class="col-sm-10">
      {!! Form::textarea('notes', $car->notes, ['class'=>'form-control','rows'=>4]) !!}
    </div>
  </div>
  
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-success">Save Changes</button>
    </div>
  </div>