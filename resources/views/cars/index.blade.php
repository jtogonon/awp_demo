@extends('master')

@section('content')
  	 <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Users' Cars
          <small>Control panel</small>
        </h1>
        <div class="pull-right" style="padding:10px;">
          <a href="" class="btn btn-block btn-success btn_add" id="btn_addexpense">Add New Car</a>
        </div>
      </section>

      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">All Cars</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table id="cars_tbl" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>Car</th>
                        <th>Owner</th>
                        <th>PlateNumber</th>
                        <th>Manufacturer</th>                        
                        <th>Actions</th>   
                      </tr>
                      </thead>
                      <tbody>
                        <?php 
                        foreach ($cars as $car): ?>
                        <tr>
                          <td><a href="/cars/view/<?=$car->id?>"><?=$car->model?></a></td>
                          <td><a href="/users/view/<?=$car->getCustomerId()?>"><?=$car->getCustomer()?></a></td>
                          <td><a href="/cars/view/<?=$car->id?>"><?=$car->plate_number?></a></td>
                          <td><?=$car->getManufacturer()?></td>
                          <td>
                            <a  class="btn btn-flat" title="Edit">
                                <i class="fa fa-edit"></i>
                              </a>
                               <a href="/cars/delete/<?=$car->id?>" class="btn btn-flat btn_delete">
                                <i class="fa fa-remove"></i>
                              </a>                            
                          </td>                         
                        </tr>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
            </div><!-- end box -->
          </div><!-- end col-xs-12 -->
        </div><!-- end row -->

      </section>


      <div class="modal fade" tabindex="-1" role="dialog" id="modal-add-user">
        <div class="modal-dialog">
          {!! Form::model(new App\Models\Car, array('url' => 'cars/save', 'method' => 'post','class'=>'form-horizontal','id'=>'form-add')) !!}
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Add New Car</h4>
            </div>
            <div class="modal-body">
              
                  <div class="box-body">
                    <div class="form-group">
                       {!! Form::label('user_id', 'Customer:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-8">                            
                            {!! Form::select('user_id', $customers, '', ['class'=>'form-control select2','style'=>'width:75%;']) !!}       
                        </div>
                    </div> 
                 
                    <div class="form-group">
                       {!! Form::label('model', 'Model Variant:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-8">                            
                            {!! Form::text('model', '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div> 

                    <div class="form-group">
                       {!! Form::label('plate_number', 'Plate Number:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-8">                            
                            {!! Form::text('plate_number', '', ['class'=>'form-control','id'=>'plate_number']) !!}                            
                        </div>
                    </div> 

                    <div class="form-group">
                       {!! Form::label('manufacturer_id', 'Manufacturer:',['class'=>'col-sm-3 control-label']) !!}
                        <div class="col-sm-8">                            
                            {!! Form::select('manufacturer_id', $manufacturers, '', ['class'=>'form-control select2','style'=>'width:75%;']) !!}       
                        </div>
                    </div> 

                  </div>

              
            </div>
            <div class="modal-footer">
             
              <button type="submit" class="btn btn-primary">Save New Car</button>
            </div>
          </div>
          {!! Form::close() !!}
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

@endsection