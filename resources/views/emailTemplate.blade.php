<html>
<head></head>
<body style="background-color: #E4E4E4;padding: 20px; margin: 0; min-width: 640px;">
	<table border="0" cellspacing="0" width="630" style="color:#262626;background-color:#fff;
		padding:27px 30px 20px 30px;margin:auto; border:1px solid #e1e1e1;">
		<tbody>
			<!-- header -->
			<!-- remove header when used by SHOP -->
			@if ($main_sender  && $main_sender =="autoworkspro")
			<tr style="background:#01313b">
				<td style="padding:40px 30px">
					<a target="_blank" style="text-decoration:none;color:inherit;font-family:'HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;">
						<img src="{{$publicUrl}}/img/autoworkspro-logo2.png" alt="Auto Works Pro">
					</a>					
				</td>
			</tr>
			@endif
			
			@if ($main_sender && $main_sender =="shop" AND shop_logo !="")
			<tr>
				<td style="padding:10px 10px"><img src="{{$shop_logo}}" alt="" width="120"></td>
			</tr>
			@endif
		</tbody>

		@yield('content')

		

		<!--footer-->
		<tbody>
			<tr>
				<td align="right" style="padding:25px 0  0 0;">
					<table border="0" cellspacing="0" cellpadding="0" style="padding-bottom:9px;" align="right">
						<tbody>
							<tr style="">
								<td width="24" style="padding:0 7px 0 0;">
									<a href="https://www.facebook.com/autoworkspro" style="border-width:0; text-decoration:none;border:none;" target="_blank">
										<img src="{{$publicUrl}}/img/email-icon-facebook.jpg" width="24" height="24" alt="Facebook Image">
									</a>
								</td>
								<td width="24">
									<a href="https://twitter.com/autoworkspro" style="border-width:0; text-decoration:none;border:none;" target="_blank">
										<img src="{{$publicUrl}}/img/email-icon-twitter.jpg" width="24" height="24" alt="Twitter Image">
									</a>
								</td>
							</tr>
							<tr style="height:1px; background:#000;padding-top:15px">
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>

	</body>
</html>