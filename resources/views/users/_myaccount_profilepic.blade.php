
    <div class="form-group">
      <label for="inputName" class="col-sm-2 control-label">Upload Picture</label>

      <span class="btn btn-success fileinput-button">
        <i class="glyphicon glyphicon-plus"></i>
        <span>Select file...</span>
        <!-- The file input field used as target for the file upload widget -->
        <input id="fileupload" type="file" name="profilepic">
    </span>
        
      
    </div>

   
    <hr/>

    <br>
    <!-- The global progress bar -->
    <div id="progress" class="progress">
        <div class="progress-bar progress-bar-success"></div>
    </div>
    <!-- The container for the uploaded files -->
    <div id="files" class="files"></div>

   
    
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-success">Save Changes</button>
      </div>
    </div>

