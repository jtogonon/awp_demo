@extends('master')

@section('content')
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="/users">Customers</a></li>
        <li class="active">User profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="{{$userprofilepic}}" alt="User profile picture">

              <h3 class="profile-username text-center"><?php echo $user->firstname . ' '.$user->lastname?></h3>

              <p class="text-muted text-center"><!-- TODO profession here --></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Cars</b> <a class="pull-right"><?php echo $user->countCars();?></a>
                </li>
                <li class="list-group-item">
                  <b>Paid Services</b> <a class="pull-right"><?=$paidServices?></a>
                </li>
                <li class="list-group-item">
                  <b>Total Amount</b> <a class="pull-right">{{$shopcurrencysymbolhtml}}<?=number_format($paymentsMade,2)?></a>
                </li>
              </ul>

              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                  @include('users/_view_aboutbox')          
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">              
              <li class="active"><a href="#timeline" data-toggle="tab">Timeline</a></li>
              <li><a href="#profile" data-toggle="tab">Profile</a></li>
              <li><a href="#membership" data-toggle="tab">Membership</a></li>
              <li><a href="#cars" data-toggle="tab">Cars</a></li>
            </ul>
            <div class="tab-content">
              
              <div class="tab-pane active" id="timeline">
                  @include('users/_view_timeline')
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="profile">
                  {!! Form::model(new App\User, array('url' => '/users/save', 'method' => 'post','class'=>'form-horizontal form_details','id'=>'form-profile')) !!}
                    {!! Form::hidden('id', $user->id,['id'=>'user_id']) !!}
                    {!! Form::hidden('userdetails_id', $userDetails->id) !!}
                    @include('users/_view_profile')
                  {!! Form::close() !!}
              </div>
              <!-- /.tab-pane -->

              <!-- start tab-pane -->
              <div class="tab-pane" id="membership">
                  {!! Form::model(new App\User, array('url' => '', 'method' => 'post','class'=>'form-horizontal','id'=>'form-profile')) !!}
                    {!! Form::hidden('id', $user->id) !!}
                    @include('users/_view_membership')
                  {!! Form::close() !!}
              </div>
              <!-- /.tab-pane -->


              <!-- start tab-pane -->
              <div class="tab-pane" id="cars">
                  {!! Form::model(new App\Models\Car, array('url' => '', 'method' => 'post','class'=>'form-horizontal','id'=>'form-profile')) !!}
                    {!! Form::hidden('id', $user->id) !!}
                    @include('users/_view_cars')
                  {!! Form::close() !!}
              </div>
              <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->

    <div class="modal fade" tabindex="-1" role="dialog" id="modal-add-user">
        <div class="modal-dialog">
          {!! Form::model(new App\User, array('url' => 'users/save_usermembership', 'method' => 'post','class'=>'form-horizontal','id'=>'form-add')) !!}
          {!! Form::hidden('user_id', $user->id, ['class'=>'form-control']) !!}      
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Add Customer to Membership Level</h4>
            </div>
            <div class="modal-body">
              
                  <div class="box-body">
                    <div class="form-group">
                       {!! Form::label('membership', 'Membership Level',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">                            
                            {!! Form::select('membership_id', $membershipLevels, '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div>
                    <br/>
                    <div class="form-group">
                       {!! Form::label('notes', 'Notes',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">                            
                            {!! Form::textarea('notes', '', ['class'=>'form-control','rows'=>'4','placeholder'=>'Put some notes here']) !!}      
                        </div>
                    </div>
                   
                    <div class="form-group">
                       {!! Form::label('notes', '*',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">   
                             <br/><small><em>Curently all membership will expire after a year unless cancelled by customer.</em></small>
                        </div>
                    </div>
                  </div>
            </div>
            <div class="modal-footer">
             
              <button type="submit" class="btn btn-primary">Add User to Membership</button>
            </div>
          </div>
          {!! Form::close() !!}
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

@endsection