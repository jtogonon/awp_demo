    <div class="form-group">
      <label for="inputName" class="col-sm-2 control-label">Shop  Name</label>

      <div class="col-sm-10">        
        {!! Form::text('name', $shop->name, ['class'=>'form-control','id'=>'firstname']) !!}
      </div>
    </div>

    <div class="form-group">
      <label for="inputEmail" class="col-sm-2 control-label">Transaction Email</label>

      <div class="col-sm-10">
        {!! Form::email('shop_email', $shop->shop_email, ['class'=>'form-control','id'=>'shop_email']) !!}
      </div>
    </div>
    <div class="form-group">
      <label for="inputEmail" class="col-sm-2 control-label">Phone #</label>

      <div class="col-sm-10">
        {!! Form::text('phone', $shop->phone, ['class'=>'form-control','id'=>'phone']) !!}
      </div>
    </div>

    <div class="form-group">
      <label for="inputEmail" class="col-sm-2 control-label">Tag Line</label>

      <div class="col-sm-10">
        {!! Form::textarea('tagline', $shop->tagline, ['class'=>'form-control','rows'=>'3']) !!}
      </div>
    </div>
    
    

    <hr/>
    <h4>Shop Address</h4>

    <div class="form-group">
      <label for="inputName" class="col-sm-2 control-label">Address 1</label>

      <div class="col-sm-10">
        {!! Form::text('address1', $shop->address1, ['class'=>'form-control','id'=>'address1']) !!}
      </div>
    </div>

    <div class="form-group">
      <label for="inputName" class="col-sm-2 control-label">Address 2</label>
      <div class="col-sm-10">
        {!! Form::text('address2', $shop->address2, ['class'=>'form-control','id'=>'address2']) !!}
      </div>
    </div>

    <div class="form-group">
      <label for="inputName" class="col-sm-2 control-label">City</label>
      <div class="col-sm-10">
        {!! Form::text('city', $shop->city, ['class'=>'form-control','id'=>'city']) !!}
      </div>
    </div>

    <div class="form-group">
      <label for="inputName" class="col-sm-2 control-label">Zip</label>
      <div class="col-sm-10">
        {!! Form::text('zip', $shop->zip, ['class'=>'form-control','id'=>'zip']) !!}
      </div>
    </div>

    <div class="form-group">
      <label for="inputName" class="col-sm-2 control-label">State / Province</label>
      <div class="col-sm-10">
        {!! Form::text('state', $shop->state, ['class'=>'form-control','id'=>'state']) !!}
      </div>
    </div>

    <div class="form-group">
      <label for="inputName" class="col-sm-2 control-label">State / Province</label>
      <div class="col-sm-10">
        {!! Form::select('country', $countries,$shop->country, ['class'=>'form-control','id'=>'state']) !!}
      </div>
    </div>

    <hr/>

    <div class="form-group">
      <label for="time_zone_id" class="col-sm-2 control-label">Timezone</label>
      <div class="col-sm-10">       

        <?php echo Timezone::selectForm($shop->timezone, 'Select a timezone',array('class' => 'form-control', 'name' => 'timezone')); ?>
      </div>
    </div>

    <div class="form-group">
      <label for="time_zone_id" class="col-sm-2 control-label">Currency</label>
      <div class="col-sm-10">
        {!! Form::select('currency_id', $currencies, $shop->currency_id, ['class'=>'form-control','id'=>'currency_id']) !!}
      </div>
    </div>
    <hr/>

   
    
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-success">Save Changes</button>
      </div>
    </div>
