    <div class="form-group">
      <label for="inputName" class="col-sm-2 control-label">First Name</label>

      <div class="col-sm-10">        
        {!! Form::text('firstname', $user->firstname, ['class'=>'form-control','id'=>'firstname']) !!}
      </div>
    </div>

    <div class="form-group">
      <label for="inputName" class="col-sm-2 control-label">Last Name</label>

      <div class="col-sm-10">
        {!! Form::text('lastname', $user->lastname, ['class'=>'form-control','id'=>'lastname']) !!}
      </div>
    </div>
    <div class="form-group">
      <label for="inputEmail" class="col-sm-2 control-label">Email</label>

      <div class="col-sm-10">
        {!! Form::email('email', $user->email, ['class'=>'form-control','id'=>'email']) !!}
      </div>
    </div>
    
    

    <hr/>
    <h4>Billing Address</h4>

    <div class="form-group">
      <label for="inputName" class="col-sm-2 control-label">Address 1</label>

      <div class="col-sm-10">
        {!! Form::text('address1', $userDetails->address1, ['class'=>'form-control','id'=>'address1']) !!}
      </div>
    </div>

    <div class="form-group">
      <label for="inputName" class="col-sm-2 control-label">Address 2</label>
      <div class="col-sm-10">
        {!! Form::text('address2', $userDetails->address2, ['class'=>'form-control','id'=>'address2']) !!}
      </div>
    </div>

    <div class="form-group">
      <label for="inputName" class="col-sm-2 control-label">City</label>
      <div class="col-sm-10">
        {!! Form::text('city', $userDetails->city, ['class'=>'form-control','id'=>'city']) !!}
      </div>
    </div>

    <div class="form-group">
      <label for="inputName" class="col-sm-2 control-label">Zip</label>
      <div class="col-sm-10">
        {!! Form::text('zip', $userDetails->zip, ['class'=>'form-control','id'=>'zip']) !!}
      </div>
    </div>

    <div class="form-group">
      <label for="inputName" class="col-sm-2 control-label">State / Province</label>
      <div class="col-sm-10">
        {!! Form::text('state', $userDetails->state, ['class'=>'form-control','id'=>'state']) !!}
      </div>
    </div>

    <div class="form-group">
      <label for="inputName" class="col-sm-2 control-label">Country</label>
      <div class="col-sm-10">
        {!! Form::select('country', $countries,$userDetails->country, ['class'=>'form-control','id'=>'state']) !!}
      </div>
    </div>

    <hr/>

    <?php if (!$shop_admin_account): ?>

    <div class="form-group">
      <label for="inputExperience" class="col-sm-2 control-label">Notes</label>

      <div class="col-sm-10">
        {!! Form::textarea('notes', $userDetails->notes, ['class'=>'form-control','id'=>'notes','rows'=>5]) !!}
      </div>
    </div>
    <?php endif; ?>
    
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-success">Save Changes</button>
      </div>
    </div>
