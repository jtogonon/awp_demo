<?php
use App\Models\Country;
?>
<strong><i class="fa fa-book margin-r-5"></i> Membership</strong>

              <p class="text-muted">
                @if($usermembership!=false)
                  {{$usermembership->membership_name}}
                  <br/>Member since {{$userMem->start_date}}
                @else

                @endif
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

              <p class="text-muted"><?php echo $userDetails->address1 .' '.$userDetails->address2 .', <br/>'. $userDetails->city  ?>
                <?php echo $userDetails->state .', <br/>' .Country::getName($userDetails->country). ', '.$userDetails->zip;?></p>

              <hr>



              <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

              <p><?php echo $userDetails->notes; ?></p>