<?php
use App\Models\Country;
use App\Helpers\Helper;
?>
@extends('master')

@section('content')
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Shop Admin Account
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/dashboard">Dashboard</a></li>
        <li class="active">My Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="{{$userprofilepic}}" alt="User profile picture">

              <h3 class="profile-username text-center"><?php echo $user->firstname .' '. $user->lastname;?></h3>

              <p class="text-muted text-center"><?php echo $shop->name; ?></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Date Joined</b> <a class="pull-right"><?php echo Helper::formatDate($shop->created_at,5); ?></a>
                </li>
                <li class="list-group-item">
                  <b>Account Type</b> <a class="pull-right"><?php echo ucfirst($shop->account_type); ?></a>
                </li>
                <li class="list-group-item">
                  <b>Status</b> <a class="pull-right"><?=$shop->getStatus();?></a>
                </li>
              </ul>

              <?php if ($shop->account_type == 'free'): ?>
                <a href="#" class="btn btn-primary btn-success"><b>Upgrade Account</b></a>
              <?php endif; ?>
              <?php if ($shop->account_type == 'standard'): ?>
                <a href="#" class="btn btn-primary btn-success"><b>Upgrade to Pro</b></a>
              <?php endif; ?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About My Shop</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Tag Line</strong>

              <p class="text-muted">
                <?php echo $shop->tagline; ?>
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

              <p class="text-muted"><?php echo $shop->address1 .' '. $shop->address2; ?><br/>
              <?php echo $shop->city .' '. $shop->state;?><br/>
              <?php echo Country::getName($shop->country). ', '.$shop->zip; ?></p>

             
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">             
              <li class="active"><a href="#profile" data-toggle="tab">Profile</a></li>
              <li><a href="#shop_settings" data-toggle="tab">Shop Settings</a></li>
              <li><a href="#profile_pic" data-toggle="tab">Profile Picture</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="profile">
                  {!! Form::model(new App\User, array('url' => '/users/save', 'method' => 'post','class'=>'form-horizontal form_details','id'=>'form-profile')) !!}
                    {!! Form::hidden('id', $user->id,['id'=>'user_id']) !!}
                    {!! Form::hidden('userdetails_id', $userDetails->id) !!}
                    @include('users/_view_profile')
                  {!! Form::close() !!}
              </div>

              <div class="tab-pane" id="shop_settings">
                  {!! Form::model(new App\Models\Shop, array('url' => '/users/saveshopsettings', 'method' => 'post','class'=>'form-horizontal form_details','id'=>'form-profile')) !!}
                    {!! Form::hidden('id', $user->id,['id'=>'user_id']) !!}
                    {!! Form::hidden('shop_id', $shop->id) !!}
                    @include('users/_myaccount_shopsettings')
                  {!! Form::close() !!}
              </div>

              <div class="tab-pane" id="profile_pic">
                  {!! Form::model(new App\Models\File, array('url' => '/files/upload', 'method' => 'post','class'=>'form-horizontal form_details','id'=>'form-profile')) !!}
                    {!! Form::hidden('id', $user->id,['id'=>'user_id']) !!}                    
                    @include('users/_myaccount_profilepic')
                  {!! Form::close() !!}
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->

@endsection