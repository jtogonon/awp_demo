    <div class="form-group">
      <label for="inputName" class="col-sm-2 control-label">Membership</label>

      <div class="col-sm-10">        
        @if($usermembership!=false)
          <input type="text" value="{{$user->getUserMembershipName()}}" class="form-control no-border"/>

          
        @else
          <input type="text" value="No current membership" class="form-control no-border"/>
          <br/>
            <button type="button" class="btn btn-success btn_add" >Add Membership</button>
          
        @endif
      </div>

      @if($usermembership!=false)
      <div id="box_membership_record" class="content">

      </div>
      @endif
    </div>


    
