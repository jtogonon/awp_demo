<?php
use App\Helpers\Helper;
?>
@extends('master')

@section('content')
  	 <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Users / Customers
          <small>Control panel</small>
        </h1>
        <div class="pull-right" style="padding:10px;">
          <a href="" class="btn btn-block btn-success btn_add" id="btn_adduser">Add New User</a>
        </div>
      </section>

      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">All Customers</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table id="users_tbl" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>Name</th>                        
                        <th>Email</th>
                        <th>Membership</th>
                        <th>Date Joined</th>
                        
                        <th>Actions</th>
                      </tr>
                      </thead>
                      <tbody>
                        <?php 
                        foreach ($customers as $customer): ?>
                        <tr>
                          <td><a href="/users/view/<?=$customer->id?>"><?=$customer->firstname . ' '.$customer->lastname?></a></td>                          
                          <td><?=$customer->email?></td>
                          <td><?=$customer->getUserMembershipName()?></td>
                          <td><?=Helper::formatDate($customer->created_at,5)?></td>                          
                          <td><a  class="btn btn-flat" title="Edit">
                                <i class="fa fa-edit"></i>
                              </a>
                               <a href="/users/delete/<?=$customer->id?>" class="btn btn-flat btn_delete">
                                <i class="fa fa-remove"></i>
                              </a>
                              
                              <a class="btn btn-flat" title="Create New Job">
                                <i class="fa fa-tasks"></i>
                              </a>

                            </td>
                        </tr>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
            </div><!-- end box -->
          </div><!-- end col-xs-12 -->
        </div><!-- end row -->

      </section>



      <div class="modal fade" tabindex="-1" role="dialog" id="modal-add-user">
        <div class="modal-dialog">
          {!! Form::model(new App\User, array('url' => 'users/save', 'method' => 'post','class'=>'form-horizontal','id'=>'form-add')) !!}
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Add New Customer</h4>
            </div>
            <div class="modal-body">
              
                  <div class="box-body">
                    <div class="form-group">
                       {!! Form::label('firstname', 'First Name',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">                            
                            {!! Form::text('firstname', '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div>
                    <div class="form-group">
                       {!! Form::label('lastname', 'Last Name',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">                            
                            {!! Form::text('lastname', '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div>
                    <div class="form-group">
                       {!! Form::label('email', 'Email',['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">                            
                            {!! Form::email('email', '', ['class'=>'form-control','id'=>'']) !!}                            
                        </div>
                    </div>
                    <!-- <div class="form-group">
                       <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-10 checkbox">                            
                            {!! Form::checkbox('sendwelcomeemail', '', ['class'=>'form-control','id'=>'']) !!}     
                            Send Welcome Email                       
                        </div>
                    </div> -->
                  </div>

              
            </div>
            <div class="modal-footer">
             
              <button type="submit" class="btn btn-primary">Save New Customer</button>
            </div>
          </div>
          {!! Form::close() !!}
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      
@endsection