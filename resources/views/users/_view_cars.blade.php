<table id="cars_tbl" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>Car</th>
                        <th>Plate #</th>
                        <th>Manufacturer</th>                        
                        <th>Actions</th>   
                      </tr>
                      </thead>
                      <tbody>
                        <?php 
                        foreach ($cars as $car): ?>
                        <tr>
                          <td><a href="/cars/view/<?=$car->id?>"><?=$car->model?></a></td>
                          <td><?=$car->plate_number?></td>
                          <td><?=$car->getManufacturer()?></td>
                          <td>
                            <a  class="btn btn-flat" title="Edit">
                                <i class="fa fa-edit"></i>
                              </a>
                               <a href="/cars/delete/<?=$car->id?>" class="btn btn-flat btn_delete">
                                <i class="fa fa-remove"></i>
                              </a>                            
                          </td>                         
                        </tr>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
