<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
	
	public function status()
	{
		return $this->belongsTo('App\Models\ShopStatus','status_id','id');
	}

	public function getStatus()
	{
		if($this->status):
			return $this->status->name;
		else:
			return '';
		endif;
	}

	public function customer()
	{
		return $this->belongsTo('App\User','user_id','id');
	}

	public function timezone()
	{
		return $this->belongsTo('App\Models\Timezone','time_zone_id','id');
	}

	public function getTimezone()
	{
		if($this->timezone):
			return $this->timezone;
		else:
			return '';
		endif;
	}

	public function getCustomer()
	{
		if($this->customer):
			return $this->customer->firstname.' '.$this->customer->lastname;
		else:
			return '';
		endif;
	}


	public static function getAllShops($status)
	{
		$shops = self::where('status_id', 2)
               ->orderBy('created_at', 'desc')
               ->get();

        return $shops;
	}
}