<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{

	

	public function manufacturer()
	{
		return $this->belongsTo('App\Models\CarManufacturer','manufacturer_id','id');
	}
	public static function getVendorsByShopId($shop_id)
	{
		$cars = self::where('shop_id', $shop_id)
               ->orderBy('name', 'asc')               
               ->get();

        return $cars;
	}

	public static function getLists($shop_id)
	{
		$cats = self::where('shop_id', $shop_id)
               ->orderBy('name', 'asc')
               ->get();

        return $cats;
	}


}

