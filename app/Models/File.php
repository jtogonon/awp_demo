<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
	var $table = "files";

	public static function getImagesByJobId($job_id, $car_id, $shop_id)
	{

		$images = self::where('shop_id', $shop_id)
				->where('car_id', $car_id)
				->where('job_id', $job_id)
               ->orderBy('created_at', 'desc')
               ->get();

        return $images;
	}
}