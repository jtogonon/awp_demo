<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{

	var $table = 'membership';

	public function customer()
	{
		return $this->belongsTo('App\User','user_id','id');
	}

	
	public static function getMembershipsByShopId($shop_id)
	{
		$mems = self::where('shop_id', $shop_id)
               ->orderBy('membership_name', 'asc')               
               ->get();

        return $mems;
	}

	public function getCustomer()
	{
		if($this->customer):
			return $this->customer->name;
		else:
			return '';
		endif;
	}

	public function getManufacturer()
	{
		if($this->customer):
			return $this->customer->name;
		else:
			return '';
		endif;
	}


	/*
	var list = true, if false return count */
	public static function getMembers($shop_id, $membership_id, $list = true) 
	{
		if ($list){
			$userMembership = new UserMembership;
			return $userMembership->getMembers($shop_id, $membership_id);
		} else {
			$userMembership = new UserMembership;
			$members = $userMembership->getMembers($shop_id, $membership_id);
			return count($members);
		}
	}

	public static function getMembershipListForSelect($shop_id)
	{
		$levels = self::where('shop_id',$shop_id)
            ->orderBy('membership_name', 'asc')
            ->get();

        return $levels;
	}

}

