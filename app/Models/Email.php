<?php namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Ivory\HttpAdapter\Guzzle6HttpAdapter;

use Log;


class Email extends Model
{

	var $table = 'emails';


	/**
	 * @return mixed
     */
	public static function getListForSelect()
	{
		//TODO make sure to show distinct names
		$zones = self::select('id','name','symbol')
            ->orderBy('name', 'asc')
            ->get();

        return $zones;
	}

    /**
     * @throws SparkPost
     */
    public function pre_send_mail($data){
        $result = $this->send_email($data);

        return $result;
    }

    /**
     * @param $data
     * @return array
     * @throws SparkPost_error
     */
    public function send_email($data)
	{
		$results ='';
		$templateId = 'thank-you-email1';
		$templateId = 'my-first-email';



		$recipients = $data['recipients'];

		$httpAdapter = new Guzzle6HttpAdapter(new Client());
		$sparky = new SparkPost($httpAdapter, ['key'=>'a157d528ddf5149940418d9c8db6a71ae147beea']);

		if (isset($recipients[0])):
			$name1 = (isset($recipients[0]['name'])) ? $recipients[0]['name'] : '';
			$email1 = (isset($recipients[0]['email'])) ? $recipients[0]['email'] : '';
		endif;
		$substitutionData = ['name'=>$name1];

		try {
			
			/*
			Note: if using templateId, exclude html, text, and subject fields 
			*/
		  // Build your email and send it!
		  $results = $sparky->transmission->send([
		    'from'=>[
		            'name' => 'Support',
		            'email' => 'support@autoworkspro.com'
		        ],		   
		    'html'=>$data['emailBody'],
		    'text'=>$data['emailBody'],
		    'substitutionData'=>$substitutionData,
		    'subject'=>$data['subject'],
		    'recipients'=>[
		      [
		        'address'=>[
		          'name'=>$name1,
		          'email'=>$email1
		        ]
		      ]
		    ]
		  ]);
		  
		  Log::info('Email sent to '.$email1);

		} catch (\Exception $err) {
		  Log::info('Failed sending email: '.$err->getAPIMessage() .' Code:'.$err->getAPICode() .' Desc: '.$err->getAPIDescription() );
		}

		return $results;
	}

    /**
     * @param $emails_to
     * @return array
     */
	public static function extractEmails($emails_to)
	{
        $recipient = [];
		if (strpos($emails_to, ',') !== false) {
			$pcs = explode(',', $emails_to);
			foreach($pcs as $email):
                $email = trim($email);
				$user = User::where('email',$email)->get();
                if (count($user)> 0):
                    $u = $user[0];
                    $recipient[] = ['email'=>$u->email,'name'=>$u->firstname .' '. $u->lastname];
                else:
                    $recipient[] = ['email'=>$email,'name'=>''];
                endif;

			endforeach;
		} else {
            $recipient[] = ['email'=>$emails_to,'name'=>''];
		}
        return $recipient;
	}


}