<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class Job extends Model
{
	public function customer()
	{
		return $this->belongsTo('App\User','user_id','id');
	}

	public function car()
	{
		return $this->belongsTo('App\Models\Car','car_id','id');
	}

	public function status()
	{
		return $this->belongsTo('App\Models\JobStatus','status_id','id');
	}

	public function files()
	{
		return $this->hasMany('App\Models\File','job_id','id');
	}

	public function getCustomer()
	{
		if($this->customer):
			return $this->customer->firstname.' '.$this->customer->lastname;
		else:
			return '';
		endif;
	}

	public function getCustomerID()
	{
		if($this->customer):
			return $this->customer->id;
		else:
			return '';
		endif;
	}

	public function getCar()
	{
		if($this->car):
			return $this->car->model;
		else:
			return '';
		endif;
	}
	public function getStatus()
	{
		if($this->status):
			return $this->status->status_name;
		else:
			return '';
		endif;
	}

	public static function getJobsByShopId($shop_id, $limit = 1000)
	{
		$jobs = self::where('shop_id', $shop_id)
               ->orderBy('created_at', 'desc')
               ->limit($limit)
               ->get();

        return $jobs;
	}	

	

	public static function getJobServices($job_id)
	{
		$services = DB::table('job_services')
					->where('job_id',$job_id)
					->get();

		return $services;
	}


	public function getImages()
	{

	}

	public static function getImagesCount()
	{
		return 0;
	}

	public static function saveServices($services, $job_id, $auth){
		//TODO, insert services for a job
		foreach ($services as $serv=>$s){
			\DB::table('job_services')
				->insert(array('job_id'=>$job_id,'service_id'=>$s,'shop_id'=>$auth->shop_id,'user_id'=>$auth->id));
				
		}
	}

	public static function getServicesOnThisJob($job_id)
	{
		$services = \DB::table('job_services')
					->where('job_id',$job_id)
					->get();

		return $services;
	}

	public static function getJobsFinished($shop_id, $date, $status)
	{
		$jobs = self::where('shop_id', $shop_id)
				->where('shop_id', $shop_id)
				->where('status_id', $status)
               	->orderBy('created_at', 'desc')
               	->get();

        return $jobs;
	}

	public static function checkServiceOnThisJob($job_id, $service_id)
	{
		$services = \DB::table('job_services')
					->where('job_id',$job_id)
					->where('service_id',$service_id)
					->get();

		if (count($services) > 0 ) :
			return count($services);
		else:
			return 0;
		endif;
	}
}