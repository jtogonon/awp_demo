<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductManufacturer extends Model
{

	var $table = 'products_manufacturers';

	public static function getProdManufacturersByShopId($shop_id)
	{
		$servs = self::where('shop_id', $shop_id)
               ->orderBy('name', 'asc')               
               ->get();

        return $servs;
	}


}

