<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Job;

class UserMembership extends Model
{

	var $table = 'users_membership';

	public function customer()
	{
		return $this->belongsTo('App\User','user_id','id');
	}

	public function membership()
	{
		return $this->belongsTo('App\Models\Membership','membership_id','id');
	}

	
	public static function getMembershipsByShopId($shop_id)
	{
		$mems = self::where('shop_id', $shop_id)
               ->orderBy('membership_name', 'asc')               
               ->get();

        return $mems;
	}

	public function getCustomer()
	{
		if($this->customer):
			return $this->customer->name;
		else:
			return '';
		endif;
	}

	public function getManufacturer()
	{
		if($this->customer):
			return $this->customer->name;
		else:
			return '';
		endif;
	}


	public function getMembers($shop_id, $membership_id)
	{
		$members = self::where('shop_id', $shop_id)
				->where('membership_id', $membership_id)                         
               ->get();

        return $members;
	}

	public static function getMembershipByUserId($shop_id, $user_id)
	{
		$mem = self::where('shop_id', $shop_id)
				->where('user_id', $user_id)                  
               ->get();

        if (count($mem)>0):
        	return $mem;
        else :
        	return false;
        endif;
	}

	public static function getMembersPaidServicesFromDate($shop_id, $user_id, $start_date = null)
	{
		if ($start_date ==null):
			$jobs = Job::where('shop_id', $shop_id)
				->where('user_id', $user_id)    
				->where('status_id','>=', 3)				
               ->get();

		else:

			$jobs = Job::where('shop_id', $shop_id)
				->where('user_id', $user_id)    
				->where('status_id','>=', 3)
				->where('start_date','>=', $start_date)
               ->get();
        endif;
               
               
        if (count($jobs)>0):
        	return $jobs;
        else :
        	return false;
        endif;
	}
}

