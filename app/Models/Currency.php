<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{

	var $table = 'currencies';


	public static function getListForSelect()
	{
		//TODO make sure to show distinct names
		$zones = self::select('id','name','symbol')
            ->orderBy('name', 'asc')
            ->get();

        return $zones;
	}
}