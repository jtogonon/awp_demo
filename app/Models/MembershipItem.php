<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\MembershipItem;

class MembershipItem extends Model
{

	var $table = 'membership_items';

	public function customer()
	{
		return $this->belongsTo('App\User','user_id','id');
	}

	public function membership()
	{
		return $this->belongsTo('App\Models\Membership','membership_id','id');
	}

	public function getMembership()
	{
		if($this->membership):
			return $this->membership;
		else:
			return '';
		endif;
	}

	public function service()
	{
		return $this->belongsTo('App\Models\Service','service_id','id');
	}

	public function getService()
	{
		if($this->service):
			return $this->service;
		else:
			return '';
		endif;
	}

	public static function checkAndSave($shop_id, $membership_id, $service_id, $service_limit, $item_id)
	{
		$items = self::where('shop_id',$shop_id)
			->where('membership_id',$membership_id)            
			->where('service_id',$service_id)       			
            ->get();

        if (count($items)== 0 ){
            $item  = new MembershipItem; //new item to insert           
            $item->shop_id = $shop_id;
	        $item->membership_id = $membership_id;
	        $item->service_id = $service_id;
	        $item->service_limit = $service_limit;
	        $item->save();

        } 

        
        return;
	}

	public static function getItems($shop_id, $membership_id)
	{
		$items2 = self::where('shop_id',$shop_id)
			->where('membership_id',$membership_id)            			
            ->get();

        return $items2;
	}

}


