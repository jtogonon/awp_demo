<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

	public function customer()
	{
		return $this->belongsTo('App\User','user_id','id');
	}

	public function getCustomer()
	{
		if($this->customer):
			return $this->customer->firstname.' '.$this->customer->lastname;
		else:
			return '';
		endif;
	}

	public static function getEventsByShopId($shop_id)
	{
		$events = self::where('shop_id', $shop_id)
               ->orderBy('created_at', 'desc')
               ->get();

        return $events;
	}	

}