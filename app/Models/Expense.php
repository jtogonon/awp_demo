<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
	public function customer()
	{
		return $this->belongsTo('App\User','user_id','id');
	}

	public function expense_category()
	{
		return $this->belongsTo('App\Models\ExpenseCategory','expense_category_id','id');
	}

	public function getCustomer()
	{
		if($this->customer):
			return $this->customer->firstname. ' '.$this->customer->lastname;
		else:
			return '';
		endif;
	}

	public function getCategory()
	{
		if($this->expense_category):
			return $this->expense_category->name;
		else:
			return '';
		endif;
	}

	public static function geExpensesByShopId($shop_id)
	{
		$expenses = self::where('shop_id', $shop_id)
               ->orderBy('created_at', 'desc')
               ->get();

        return $expenses;
	}	
}