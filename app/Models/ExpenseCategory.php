<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpenseCategory extends Model
{
	public function customer()
	{
		return $this->belongsTo('App\User','user_id','id');
	}

	public function getCustomer()
	{
		if($this->customer):
			return $this->customer->firstname. ' '.$this->customer->lastname;
		else:
			return '';
		endif;
	}

	public static function getExpensesCategoriesByShopId($shop_id)
	{
		$expenses = self::where('shop_id', $shop_id)
               ->orderBy('created_at', 'desc')
               ->get();

        return $expenses;
	}	


	public function getTotalUnderCategory()
	{
		return 1;
	}

	public static function getLists($shop_id)
	{
		$cats = self::where('shop_id', $shop_id)
               ->orderBy('name', 'asc')
               ->get();

        return $cats;
	}
}