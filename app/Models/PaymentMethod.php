<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
	var $table = 'payment_methods';

	public static function getMethodsForSelect($shop_id=0)
	{
		$shop_id = 0; //zero for now.. in the future each shop can have its own method
		$methods = self::select('id','name')
			->where('shop_id',$shop_id)
            ->orderBy('name', 'asc')
            ->get();

        return $methods;

	}
}

