<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{
	var $table = 'invoices_items';

	public static function getServicesByShopId($shop_id)
	{
		$servs = self::where('shop_id', $shop_id)
               ->orderBy('servicename', 'asc')               
               ->get();

        return $servs;
	}

	public function service()
	{
		return $this->belongsTo('App\Models\Service','service_id','id');
	}

	public function getService()
	{
		if($this->service):
			return $this->service;
		else:
			return '';
		endif;
	}

}

