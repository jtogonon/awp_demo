<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobStatus extends Model
{
	
	var $table = 'job_statuses';

	public static function getStatuses()
	{
		$shop_id = 0;

		$status = self::where('shop_id', $shop_id)				               
               ->orderBy('id','asc')
               ->get();

        return $status;
	}


	public static function getStatusClass($status)
	{
		//checking id for now
		//TODO, should check status as string, check actual word
		switch($status):
			case 1:
			default:
				$class = 'info';
			break;

			case 2:
				$class = 'primary';
			break;

			case 3:
				$class = 'warning';
			break;

			case 4:
				$class = 'success';
			break;
		endswitch;


		return $class;

	}
}