<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\InvoiceItem;
use App\Helpers\Helper;

class Invoice extends Model
{
  var $primaryKey = 'invoice_id';
	public function customer()
	{
		return $this->belongsTo('App\User','user_id','id');
	}

  public function job()
  {
    return $this->belongsTo('App\Models\Job','job_id','id');
  }

	public function getCustomer()
	{
		if($this->customer):
			return $this->customer->firstname. ' '.$this->customer->lastname;
		else:
			return '';
		endif;
	}

  public function getUser()
  {
    if($this->customer):
      return $this->customer;
    else:
      return '';
    endif;
  }



  public function getCustomerAddress()
  {
    if($this->customer):
      return $this->customer->userdetails;
    else:
      return '';
    endif;
  }

	public static function geInvoicesByShopId($shop_id)
	{
		$invoices = self::where('shop_id', $shop_id)
               ->orderBy('invoicenumber', 'desc')
               ->get();

        return $invoices;
	}	

  public static function geInvoicesByUserId($user_id, $status)
  {
    $invoices = self::where('user_id', $user_id)
               ->where('status',$status)
               ->orderBy('invoicenumber', 'desc')
               ->get();

        return $invoices;
  } 

	public static function getShopLastInvoiceNumber($shop_id)
    {
        $items = self::where('shop_id', $shop_id)
               ->orderBy('invoicenumber', 'desc')
               ->get();

        if (count($items)==0){
           
            return 100; //starting number
        } else {
        	

            return $items[0]->invoicenumber + 1;
        }

        
    }

    public static function checkByJobId($job_id)
    {
    	$invoice = self::where('job_id', $job_id)
               ->orderBy('invoicenumber', 'desc')
               ->get();

        if (count($invoice)==1){
            return true;
        } else {
        	
        	return false;
        }
    }

    public static function getItems($invoice_id)
    {
      $items = InvoiceItem::where('invoice_id', $invoice_id)                          
               ->get();

        if (count($items)>0){
            return $items;
        } else {          
          return false;
        }
    }

    public static function getInvoiceNumber($invoice_id)
    {
      $invoice = self::find($invoice_id);               

        if (count($invoice)==1){
            return Helper::formatInvoiceNumber($invoice->invoicenumber);
        } else {
          
          return false;
        }
    }
}