<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
	public function customer()
	{
		return $this->belongsTo('App\User','user_id','id');
	}

	public function invoice()
	{
		return $this->belongsTo('App\Models\Invoice','invoice_id','invoice_id');
	}

	public function getCustomer()
	{
		if($this->customer):
			return $this->customer->firstname. ' '.$this->customer->lastname;
		else:
			return '';
		endif;
	}

	public function getInvoice()
	{
		if($this->invoice):
			return $this->invoice;
		else:
			return '';
		endif;
	}

	public static function gePaymentsByShopId($shop_id)
	{
		$invoices = self::where('shop_id', $shop_id)
               ->orderBy('created_at', 'desc')
               ->get();

        return $invoices;
	}	

	public static function getPaymentsToday($shop_id)
	{
		$dateToday = date('Y-m-d');
		$payments = self::where('shop_id', $shop_id)
				->where('payment_received_on', $dateToday)
				->sum('payment_received');
               

        return $payments;
	}


	public static function getPaymentsMonthly($shop_id, $thisMonth)
	{
		$payments = self::where('shop_id', $shop_id)
				->where('payment_received_on','LIKE', $thisMonth.'%')
				->sum('payment_received');
               

        return $payments;
	}

	public static function getPaymentsMadeByUser($user_id)
	{
		$payments = self::where('user_id', $user_id)				
				->sum('payment_received');
               

        return $payments;
	}
}