<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
	
	public static function getAllTodoItems($shop_id, $user_id)
	{
		$todos = self::where('shop_id', $shop_id)
				->where('user_id', $user_id)
               ->orderBy('created_at', 'asc')
               ->limit(10)
               ->get();

        return $todos;
	}
}