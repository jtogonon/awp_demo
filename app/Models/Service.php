<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{

	public static function getServicesByShopId($shop_id)
	{
		$servs = self::where('shop_id', $shop_id)
               ->orderBy('servicename', 'asc')               
               ->get();

        return $servs;
	}


}

