<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarManufacturer extends Model
{
	var $table = 'cars_manufacturers';

	public function customer()
	{
		return $this->belongsTo('App\User','user_id','id');
	}

	public function manufacturer()
	{
		return $this->belongsTo('App\User','user_id','id');
	}

	public static function getManufacturersList($shop_id)
	{
		$mans = self::select('id','name')
            ->orderBy('name', 'asc')
            ->get();

        return $mans;
	}
	
}

