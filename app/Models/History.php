<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
	var $table = 'history';

	public function user()
	{
		return $this->belongsTo('App\User','user_id','id');
	}


	public function getUser()
	{
		if($this->user):
			return $this->user->firstname. ' '.$this->user->lastname;
		else:
			return '';
		endif;
	}

	/**
	 * @param $shop_id
	 * @param null $user_id
	 * @param $module
	 * @param $desc
	 * @param $data
	 * @param null $job_id
	 * @param null $car_id
     */
	public static function write($shop_id, $user_id=null, $module, $desc, $data, $job_id = null, $car_id = null)
	{
		$history = new self;
		$history->shop_id = $shop_id;
		$history->user_id = $user_id;
		$history->car_id = $car_id;
		$history->module  = $module;
		$history->description = $desc;
		$history->data_arr = json_encode($data);

		if ($job_id>0):
			$history->job_id = $job_id;
		endif;

		$history->save();
	}

	/**
	 * @param $shop_id
	 * @param $user_id
	 * @return mixed
     */
	public static function getUserTimelineDates($shop_id, $user_id)
	{
		$dates = self::where('shop_id', $shop_id)
				->where('user_id', '=', $user_id)
		            ->Where(function ($query) {
		                $query->where('module', 'user')
		                      ->orWhere('module', 'payment');
		            })				
               ->groupBy('created_at')
               ->orderBy('created_at','desc')
               ->limit(20)
               ->get();

        return $dates;
	}

	public static function getCarTimelineDates($shop_id, $car_id)
	{
		$dates = self::where('shop_id', $shop_id)
				->where('car_id', $car_id)
				->where('module', 'car')
               ->groupBy('created_at')
               ->orderBy('created_at','desc')
               ->limit(20)
               ->get();

        return $dates;
	}

	public static function getJobTimelineDates($shop_id, $job_id)
	{

        $dates = self::where('shop_id', $shop_id)
				->where('job_id', '=', $job_id)
		            ->Where(function ($query) {
		                $query->where('module', 'job')
		                      ->orWhere('module', 'payment');
		            })
               ->groupBy('created_at')
               ->orderBy('created_at','desc')
               ->limit(20)
               ->get();


        return $dates;
	}


	public static function getIconByModule($module, $desc = null)
	{
		switch ($module) {
			case 'email':
				$icon = 'envelope';
			break;

			case 'car':
				$icon = 'car';
			break;

			case 'job':
				$icon = 'wrench';
				if (preg_match('/Uploaded image/',$desc)):
					$icon = 'image';
				endif;
				if (preg_match('/Invoice/',$desc)):
					$icon = 'money';
				endif;
			break;

			case 'payment':
				$icon = 'money';				
				if (preg_match('/Invoice/',$desc)):
					$icon = 'money';
				endif;
			break;
			
			case 'user':
			default:
				$icon = 'user';
			break;
		}
		return $icon;
	}


	public static function showFormattedHistoryData($module, $desc, $dataarr)
	{
		$html ='';
		switch($module):
			case 'job':
				if (strlen($dataarr) > 3 && preg_match('/Uploaded image/',$desc)) :
					$arr = json_decode($dataarr);
					//$html .= '<ul>';
					foreach ($arr as $pic):
						$html .= '<img src="'.$pic->thumbnailUrl.'" class="margin">';
					endforeach;
					//$html .= '</ul>';
				endif;
			break;
		endswitch;

		return $html;
	}
	
}