<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\File;
class Car extends Model
{

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo object
     */
	public function customer()
	{
		return $this->belongsTo('App\User','user_id','id');
	}

	public function manufacturer()
	{
		return $this->belongsTo('App\Models\CarManufacturer','manufacturer_id','id');
	}

	/**
	 * @param $shop_id
	 * @return mixed
     */
	public static function getCarsByShopId($shop_id)
	{
		$cars = self::where('shop_id', $shop_id)
               ->orderBy('model', 'asc')               
               ->get();

        return $cars;
	}

	/**
	 * @return string
     */
	public function getCustomer()
	{
		if($this->customer):
			return $this->customer->firstname.' '.$this->customer->lastname;
		else:
			return '';
		endif;
	}

	/**
	 * @return string
     */
	public function getCustomerId()
	{
		if($this->customer):
			return $this->customer->id;
		else:
			return '';
		endif;
	}

	/**
	 * @return string
     */
	public function getManufacturer()
	{
		if($this->manufacturer):
			return $this->manufacturer->name;
		else:
			return '';
		endif;
	}

	/**
	 * @param $shop_id
	 * @param $user_id
	 * @return mixed
     */
	public static function getCarsByUserId($shop_id, $user_id)
	{
		$cars = self::where('shop_id', $shop_id)
				->where('user_id', $user_id)
               ->orderBy('model', 'asc')               
               ->get();

        return $cars;
	}


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany object
     */
	public function pics()
	{
		return $this->hasMany('App\Models\File','job_id','id');
	}

	public static function getCarMainPic($shop_id, $car_id)
	{
		
		$pic = File::where('shop_id', $shop_id)
			->where('car_id', $car_id)
			/*->where('main_carpic', 1)*/
           ->get();

        if (count($pic)> 0):
        	return $pic[0]->file_name_thumb;
        else:
        	return $pic;
    	endif;

	}


	public function totalTimesServiced()
	{
		$car_id = $this->id;
		$jobs = Job::where('car_id', $car_id)
			->where('status_id', 4)		
           ->get();
        return count($jobs);
	}
}

