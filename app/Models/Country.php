<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	/**
	 * @return mixed
     */
	public static function getListForSelect()
	{
		$countries = self::select('id','name')
            ->orderBy('name', 'asc')
            ->get();

        return $countries;
	}

	/**
	 * @param $id
	 * @return mixed|string
     */
	public static function getName($id)
	{
		if (empty($id)):
			return '';
		else:
			$country = self::find($id);
			return $country->name;
		endif;
	}

}