<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

	public static function getProductsByShopId($shop_id)
	{
		$servs = self::where('shop_id', $shop_id)
               ->orderBy('product_name', 'asc')               
               ->get();

        return $servs;
	}

	public function manufacturer()
	{
		return $this->belongsTo('App\Models\ProductManufacturer','manufacturer_id','id');
	}

	public function getManufacturer()
	{
		if($this->manufacturer):
			return $this->manufacturer->name;
		else:
			return '';
		endif;
	}


}

