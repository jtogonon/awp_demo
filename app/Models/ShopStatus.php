<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopStatus extends Model
{
	var $table = 'shop_statuses';
}