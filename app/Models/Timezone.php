<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Timezone extends Model
{

	var $table = 'time_zones';


	public static function getListForSelect()
	{
		//TODO make sure to show distinct names
		$zones = self::select('id','name')
            ->orderBy('name', 'asc')
            ->get();

        return $zones;
	}
}