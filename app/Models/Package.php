<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
	

	public static function gePackagesByShopId($shop_id)
	{
		$expenses = self::where('shop_id', $shop_id)
               ->orderBy('created_at', 'desc')
               ->get();

        return $expenses;
	}	
}