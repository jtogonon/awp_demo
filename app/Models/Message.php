<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	

	public static function getReceivedMessagesByShopId($shop_id, $recipient_id)
	{

		$images = self::where('shop_id', $shop_id)
				->where('car_id', $car_id)
				->where('job_id', $job_id)
               ->orderBy('created_at', 'desc')
               ->get();

        return $images;
	}

	public static function getMessagesOnThisJob($shop_id, $job_id)
	{

		$msges = self::where('shop_id', $shop_id)				
				->where('job_id', $job_id)
               ->orderBy('created_at', 'desc')
               ->get();

        return $msges;
	}

	 public function created_ago()
	{
	    return $this->created_at->diffForHumans();
	}

}