<?php

 namespace App\Helpers;

class Helper
{
    public static function formatDate($string, $type)
    {
        if (empty($string) || $string =='0000-00-00' || $string == '0000-00-00 00:00:00'){
            return date('m/d/Y');
        }
        switch($type){
            case 1:
                //sample: 10/01/2015 =====> 2015-10-01; input to DB
                $pcs = explode('/', $string);
                return $pcs[2].'-'.$pcs[0].'-'.$pcs[1];
            break;

            case 2:
                //sample: 2015-10-01 ======> 10/01/2015; from DB to input box
                $pcs = explode('-', $string);
                return $pcs[1].'/'.$pcs[2].'/'.$pcs[0];
            break;

            case 3:
                //sample: 2015-10-01 00:00:00 ======> 10/01/2015; from DB to input box
                $pc = explode(' ', $string);
                $pcs = explode('-', $pc[0]);
                return $pcs[1].'/'.$pcs[2].'/'.$pcs[0];
            break;
            case 4:
                //sample: 2015-11-12 ======>21 December, 2015; from DB to output
                $pcs = explode('-', $string);
                return date('j F, Y',mktime(0, 0, 0,$pcs[1],$pcs[2],$pcs[0]));
            break;

            case 5:
                //sample: 2015-12-13 00:00:00 ======> Dec 12, 2015; from DB to output
            	$pc = explode(' ', $string);
                $pcs = explode('-', $pc[0]);
                
                return date('j M Y',mktime(0, 0, 0,$pcs[1],$pcs[2],$pcs[0]));
            break;

            case 6:
                //sample: 2015-12-13 ======> 13/12/2015 from DB to output
                
                $pcs = explode('-', $string);
                
                return date('d/m/Y',mktime(0, 0, 0,$pcs[1],$pcs[2],$pcs[0]));
            break;
        }
        return $str;
    }

    public static function formatTime($string, $type)
    {
        if (empty($string) || $string =='00:00:00'){
            return date('H:i:s');
        }
        switch($type){
            case 1:
                //sample: 09:15 PM =====> 21:15:00; input to DB
                return date("H:i:s", strtotime($string));
            break;

            case 2:
                //sample: 2015-10-01 ======> 10/01/2015; from DB to input box
                $pcs = explode('-', $string);
                return $pcs[1].'/'.$pcs[2].'/'.$pcs[0];
            break;

            case 3:
                //sample:21:15:00 ======>  09:15 PM; from DB to ouput
                //$pcs = explode(':', $string);
                return date("g:i a", strtotime($string));
            break;
        }//end switch
    }

    public static function arrayToObject( $array ){
      foreach( $array as $key => $value ){
        if( is_array( $value ) ) $array[ $key ] = self::arrayToObject( $value );
      }
      return (object) $array;
    }

    public static function random_string($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

    public static function formatInvoiceNumber($string)
    {
        $length = 8;
        return str_pad($string, $length, "0", STR_PAD_LEFT);
    }



}