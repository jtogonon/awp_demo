<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Auth;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//

		 app('view')->composer('*', function($view)
		    {
		    	
			    $controllerName = '';
			    $action = '';
			    $view->with(compact('controllerName', 'action'));
		    	if (app('request')->route()!=null):
			        $action = app('request')->route()->getAction();

			        $controller = class_basename($action['controller']);

			        list($controllerName, $action) = explode('@', $controller);

			      
			       
			     endif;
			      $view->with(compact('controllerName', 'action'));

		        if ( (Auth::check())  ):
		        	$user = \Auth::user();
		        	$userfullname = $user->firstname.' '.$user->lastname;
		        	$view->with('userfullname',$userfullname);
		        	if ($user->profile_pic!=''):
		        		$userprofilepic = url('/uploads/profile/'.$user->profile_pic);
		        	else:
		        		$userprofilepic = url('/img/icons/icon-user-big.jpg');
		        	endif;
		        	$view->with('userprofilepic',$userprofilepic);

		        	if ($user->hasRole('shopadmin')):
		        		if ($user->shop->currency_id !=null):
		        			$view->with('shopcurrencysymbolhtml',$user->getShopCurrency()->symbol_html);
		        			$view->with('shopcurrencysymbol',$user->getShopCurrency()->symbol);
		        		else:
		        			$view->with('shopcurrencysymbolhtml','$');
		        			$view->with('shopcurrencysymbol','USD');

		        		endif;
		        	endif;

		        	

		        	$view->with('newUnOpenedEmails',0);
		        	$view->with('newUnReadNotifications',0);

		        	$view->with('userauth',$user);

		        	
		        endif;

		        $view->with('metaTitle','AutoWorksPro - ' );

		    });


		
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);
	}

}
