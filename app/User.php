<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;

use App\Models\Currency;
class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;
	use EntrustUserTrait; 

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['firstname','lastname', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];


	public static function getCustomersByShopId($shop_id,$admin_id)
	{
		$users = User::where('shop_id', $shop_id)		
				->where('id', '!=', $admin_id)					
               ->orderBy('firstname', 'desc')               
               ->get();

        return $users;
	}

	public static function getCustomersList($shop_id, $admin_id)
	{
		//TODO: should not pull user which is same with admin id
		$users = User::where('shop_id', $shop_id)			
			->where('id', '!=', $admin_id)			
			->select('id','firstname','lastname')
            ->orderBy('firstname', 'asc')               
             ->orderBy('lastname', 'asc')               
            ->get();

        return $users;
		
	}

	public function cars()
	{
		return $this->hasMany('App\Models\Car','user_id','id');
	}

	public function userdetails()
	{
		return $this->hasOne('App\Models\UserDetail');
	}

	public function shop()
	{
		return $this->hasOne('App\Models\Shop');
	}

	public function usermembership()
	{
		return $this->hasOne('App\Models\UserMembership');
	}

	public function getUserMembership()
	{
		if($this->usermembership):
			return $this->usermembership;
		else:
			return '';
		endif;
	}

	public function getUserMembershipName()
	{
		if($this->usermembership):
			return $this->usermembership->membership->membership_name;
		else:
			return '';
		endif;
	}

	public function getShop()
	{
		if($this->shop):
			return $this->shop;
		else:
			return '';
		endif;
	}

	public function getShopCurrency()
	{
		if($this->shop):
			$currency = Currency::find($this->shop->currency_id);
			return $currency;
		else:
			return '';
		endif;
	}

	public function getCustomerCars()
	{
		if($this->cars):
			return $this->cars;
		else:
			return '';
		endif;
	}

	public function countCars()
	{
		if ($this->getCustomerCars()){
			return count($this->getCustomerCars());
		} else {
			return 0;
		}
	}

	public function getUserDetails()
	{
		if($this->userdetails):
			return $this->userdetails;
		else:
			return '';
		endif;
	}

}
