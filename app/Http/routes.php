<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::get('dashboard', 'DashboardController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

/*CUSTOMERS OR USERS*/
Route::get('users', 'UsersController@index');
Route::post('users/save', 'UsersController@save');
Route::post('users/save_usermembership', 'UsersController@save_usermembership');
Route::post('users/saveshopsettings', 'UsersController@saveshopsettings');

/*ACCOUNT*/
Route::get('account/upgrade/{type?}', 'AccountController@upgrade');

Route::get('users/delete/{id?}', 'UsersController@delete');
Route::get('my_account', 'UsersController@my_account');
Route::get('users/view/{id?}', 'UsersController@view');
Route::get('users/getTimeline/{id?}', 'UsersController@getTimeline');
Route::get('users/get_membership_record/{id?}', 'UsersController@get_membership_record');

/*CARS*/
Route::get('cars', 'CarsController@index');
Route::get('cars/delete/{id?}', 'CarsController@delete');
Route::post('cars/save', 'CarsController@save');
Route::get('cars/view/{id?}', 'CarsController@view');
Route::get('cars/getTimeline/{id?}', 'CarsController@getTimeline');

/*MEMBERSHIPS*/
Route::get('memberships', 'MembershipsController@index');
Route::post('memberships/save', 'MembershipsController@save');
Route::post('memberships/saveitems', 'MembershipsController@saveItems');
Route::post('memberships/saveItemsDetails', 'MembershipsController@saveItemsDetails');
Route::get('memberships/delete/{id?}', 'MembershipsController@delete');
Route::get('memberships/view/{id?}', 'MembershipsController@view');
Route::get('memberships/deleteitem/{id?}', 'MembershipsController@deleteitem');

/*INVOICES*/
Route::get('invoices', 'InvoicesController@index');
Route::get('invoices/new', 'InvoicesController@create');
Route::get('invoices/view/{id?}', 'InvoicesController@view');
Route::get('invoices/print/{id?}', 'InvoicesController@printview');
Route::get('invoices/getInvoicesByUserId/{id?}', 'InvoicesController@getInvoicesByUserId');
Route::get('invoices/delete/{id?}', 'InvoicesController@delete');

/*PAYMENTS*/
Route::get('payments', 'PaymentsController@index');
Route::get('payments/delete/{id?}', 'PaymentsController@delete');
Route::get('payments/new/{id?}', 'PaymentsController@create');
Route::post('payments/save', 'PaymentsController@save');

/*EXPENSES*/
Route::get('expenses', 'ExpensesController@index');
Route::get('expenses/delete/{id?}', 'ExpensesController@delete');
Route::post('expenses/save', 'ExpensesController@save');


/*EXPENSESCATEGORIES*/
Route::get('expenses_categories', 'ExpensesCategoriesController@index');
Route::get('expenses_categories/delete/{id?}', 'ExpensesCategoriesController@delete');
Route::post('expenses_categories/save', 'ExpensesCategoriesController@save');

/*VENDORS*/
Route::get('vendors', 'VendorsController@index');
Route::get('vendors/delete/{id?}', 'VendorsController@delete');
Route::post('vendors/save', 'VendorsController@save');

/*MESSAGES*/
Route::get('messages', 'MessagesController@index');
Route::get('messages/inbox', 'MessagesController@inbox');
Route::get('messages/compose', 'MessagesController@compose');
Route::get('messages/sent', 'MessagesController@sent');
Route::get('messages/delete/{id?}', 'MessagesController@delete');
Route::post('messages/save', 'MessagesController@save');
Route::post('messages/send', 'MessagesController@send');

/*SERVICES*/
Route::get('services', 'ServicesController@index');
Route::post('services/save', 'ServicesController@save');
Route::get('services/delete/{id?}', 'ServicesController@delete');
Route::get('services/view/{id?}', 'ServicesController@view');

/*PRODUCTS*/
Route::get('products', 'ProductsController@index');
Route::post('products/save', 'ProductsController@save');
Route::get('products/delete/{id?}', 'ProductsController@delete');


/*PRODUCT MANUFACTURERS*/
Route::get('product_manufacturers', 'ProductManufacturersController@index');
Route::post('product_manufacturers/save', 'ProductManufacturersController@save');
Route::get('product_manufacturers/delete/{id?}', 'ProductManufacturersController@delete');

/*PACKAGES*/
Route::get('packages', 'PackagesController@index');
Route::get('packages/delete/{id?}', 'PackagesController@delete');

/*JOBS*/
Route::get('jobs', 'JobsController@index');
Route::get('jobs/new/{user_id?}/{car_id?}', 'JobsController@create');
Route::post('jobs/save', 'JobsController@save');
Route::get('jobs/getcars/{user_id?}', 'JobsController@getCarsByUserId');
Route::get('jobs/view/{job_id?}', 'JobsController@view');
Route::get('jobs/delete/{id?}', 'JobsController@delete');
Route::get('jobs/changeStatus/{job_id?}/{status?}', 'JobsController@changeStatus');
Route::get('jobs/getTimeline/{id?}', 'JobsController@getTimeline');
Route::get('jobs/getMessages/{id?}', 'JobsController@getMessages');

/*EVENTS/CALENDAR*/
Route::get('calendar', 'EventsController@index');
Route::get('events/new/{date?}', 'EventsController@create');
Route::get('events/view/{id?}', 'EventsController@view');
Route::post('events/save', 'EventsController@save');

/*TODOS - in Dashboard*/
Route::post('todos/save', 'TodosController@save');
Route::get('todos/check/{id?}', 'TodosController@check');
Route::get('todos/uncheck/{id?}', 'TodosController@uncheck');

/*FILES*/
Route::get('files', 'FilesController@index');
Route::post('files/upload', 'FilesController@upload');
Route::post('files/uploadjobpics', 'FilesController@uploadjobpics');
Route::get('files/uploadjobpics', 'FilesController@uploadjobpics');
Route::get('files/delete/{id?}', 'FilesController@delete');

/*EMAILS*/
Route::get('emails/compose', 'EmailsController@compose');
Route::post('emails/send', 'EmailsController@send');

Route::get('super', 'SuperController@index');
Route::get('super/shops', 'SuperController@shopslist');
Route::get('super/loginAs/{user_id?}', 'SuperController@loginAs');
