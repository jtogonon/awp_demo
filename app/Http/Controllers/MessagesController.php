<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use Response,Asset,Html;

use App\User, App\Models\Message;
use App\Models\Shop;
use App\Helpers\Helper;

class MessagesController extends Controller {

	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth');

	}

	/**
	 * Show the users listing page
	 *
	 * @return Response
	 */
	public function index()
	{
		$auth = $this->auth->user();
		if (!$auth->hasRole('superadmin') && !$auth->hasRole('shopadmin')){
			 return redirect('/login');
		}  

		//JS
		$cssArray = ['/components/bootstrap-datepicker/css/datepicker3.css','/plugins/datatables/dataTables.bootstrap.css'
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/back/js/invoices.js','/plugins/datatables/jquery.dataTables.min.js','/plugins/datatables/dataTables.bootstrap.js'];
		Asset::add($jsArray, 'footer');	

		//get the messages
		$messages = Message::getMessagesByShopId($auth->shop_id);

		$data['messages'] = $messages;

		return view('messages/index', $data);
	}

	public function getMessages()
	{
		//get the messages
		$messages = Message::getMessagesByShopId($auth->shop_id);

		$data['messages'] = $messages;

		return Response::json(['data' => array('html' =>'saved',)]);
	}


	public function send(Request $req)
	{
		

		$data = [];
		$auth = $this->auth->user();

		$msg = new Message;
		$msg->shop_id = $auth->shop_id;
		$msg->job_id = $req->input('job_id');
		$msg->sender= $auth->id;
		$msg->recipient = $req->input('user_id');
		$msg->message_text = $req->input('message');
		$msg->send_date = date('Y-m-d H:i:s');
		$msg->opened = 0;
		$msg->save();

		

		return Response::json(['data' => array('html' =>'saved',)]);
	}
}
