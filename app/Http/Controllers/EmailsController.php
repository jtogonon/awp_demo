<?php namespace App\Http\Controllers;

use App\Models\Email;
use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use Response,Asset,Html;

use App\User, App\Models\History;
use App\Helpers\Helper;

use Log;

class EmailsController extends Controller {

	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth');

	}

	/**
	 * Show the users listing page
	 *
	 * @return Response
	 */
	public function index()
	{
		$auth = $this->auth->user();
		if (!$auth->hasRole('superadmin') && !$auth->hasRole('shopadmin')){
			 return redirect('/login');
		}  

		//JS
		$cssArray = ['/components/bootstrap-datepicker/css/datepicker3.css','/plugins/datatables/dataTables.bootstrap.css','/plugins/select2/select2.min.css'
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['back/js/cars.js','/plugins/datatables/jquery.dataTables.min.js','/plugins/datatables/dataTables.bootstrap.js','/plugins/select2/select2.full.min.js'];
		Asset::add($jsArray, 'footer');	

		//get the cars
		$cars = Car::getCarsByShopId($auth->shop_id);
		$data['cars'] = $cars;


		//get customers for dropdown
		$users = User::getCustomersList($auth->shop_id, $auth->id);
		$customerArr =array(''=>'-select-');		



		foreach ($users as $customer =>$c):
			$customerArr[$c['id']] = $c['firstname']. ' '.$c['lastname'];
		endforeach;
		$data['customers'] = $customerArr;


		//get manufacturers for dropdown
		$users = CarManufacturer::getManufacturersList($auth->shop_id);
		$manufacturers =array(''=>'-select-');		

		foreach ($users as $customer =>$c):
			$manufacturers[$c['id']] = $c['name'];
		endforeach;
		$data['manufacturers'] = $manufacturers;

		return view('cars/index', $data);
	}

	public function save(Request $request)
	{
		$auth = $this->auth->user();



		return Response::json(['data' => array('html' =>'saved','redirectTo'=>'/cars','msg'=>$msg)]);
	}


    /**
     * @param $id
     */
    public function delete($id)
	{
		$account = User::find($id);

		if (\Request::ajax()){
			//$res = $car ->delete();

			
		}

	}

    /**
     * Show the email composer view
     * @return view
     */
    public function compose()
	{
		$auth = $this->auth->user();

		$data = [];

		$data['user'] = $auth;

		//get customers for dropdown
		$users = User::getCustomersList($auth->shop_id, $auth->id);
		$customerArr =[];

		foreach ($users as $customer =>$c):
			$customerArr[$c['id']] = $c['firstname']. ' '.$c['lastname'];
		endforeach;
		$data['customers'] = $customerArr;

		return view('emailmarketing/compose', $data);
	}

    /**
     * This process the AJAX request to send emails based on selected radio button where to sendto
     * @param Request $req
     * @return mixed
     */
    public function send(Request $req)
	{
		$auth = Auth::user();
		$redirectTo = '';
        $errorMsg = '';
        $html ='';

        $recipients = [];
		if ($req->input('sendto')=='1'): //send to all
			$customers = User::getCustomersByShopId($auth->shop_id, $auth->id);
            foreach($customers as $c){
                $recipients[] = [$c->email =>$c->firstname .' '.$c->lastname];
            }
		endif;

		if ($req->input('sendto')=='2'): //send to selected customers
            $customers = $req->input('customers');
            foreach ($customers as $c):
                $user = User::find($c);
                $recipients[] = [$user->email =>$user->firstname .' '.$user->lastname];
            endforeach;
		endif;

		if ($req->input('sendto')=='3'): //send to set email
            $emailsto = $req->input('customers');
            $recipients = Email::extractEmails($emailsto);
		endif;

		$from = $auth->shop->shop_email;
		if ($from == ''){
			$errorMsg = 'Missing Shop Transactional Email. Please enter it in your Account Shop Settings.';
            $html ='failed';
		}

        $data = ['from'=>$from,
                'subject'=>$req->input('emailSubject'),
                'recipients'=>$recipients,
                'emailBody'=>$req->input('emailBody')
                ];

        Log::info(print_r($data,true));

        $email = new Email;
        $result = $email->pre_send_mail($data);
        
        
        if ($result){
        	$html = 'sent';
    	} else {
    		$errorMsg = 'Failed sending Email. Please contact Support.';
    	}
		
		return \Response::json(['data' => array('html' =>$html,'redirectTo'=>$redirectTo,'errorMsg'=>$errorMsg)]);
	}
}
