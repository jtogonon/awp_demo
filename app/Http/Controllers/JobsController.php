<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use Response,Asset,Html;

use  App\Models\Job,App\User, App\Models\Car,App\Models\Service, App\Models\History;
use App\Models\JobStatus, App\Models\Invoice,App\Models\InvoiceItem, App\Models\CarManufacturer;
use App\Helpers\Helper,App\Models\Message;
use Carbon\Carbon;

class JobsController extends BaseController {

	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth');

	}

	/**
	 * Show the payments listing page
	 *
	 * @return Response
	 */
	public function index()
	{
		$auth = $this->auth->user();
		if (!$auth->hasRole('superadmin') && !$auth->hasRole('shopadmin')){
			 return redirect('/login');
		}  
		
		//JS
		$cssArray = ['/plugins/datatables/dataTables.bootstrap.css',
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/back/js/jobs.js','/plugins/datatables/jquery.dataTables.min.js','/plugins/datatables/dataTables.bootstrap.js'];
		Asset::add($jsArray, 'footer');	

		//get the packages
		$jobs = Job::getJobsByShopId($auth->shop_id);

		$data['jobs'] = $jobs;
		$data['user'] = $auth;

		return view('jobs/index', $data);
	}

	public function create($user_id = null, $car_id = null)
	{
		$data = [];
		$auth = $this->auth->user();
		$data['user_id'] = $user_id;
		$data['car_id'] = $car_id;

		$data['cars'] = [];

		//JS
		$cssArray = ['/plugins/datepicker/datepicker3.css','/plugins/select2/select2.min.css','/plugins/timepicker/bootstrap-timepicker.min.css'
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/back/js/jobs.js','/plugins/select2/select2.full.min.js','/plugins/datepicker/bootstrap-datepicker.js','/plugins/timepicker/bootstrap-timepicker.min.js'];
		Asset::add($jsArray, 'footer');	


		//get customers for dropdown
		$users = User::getCustomersList($auth->shop_id,$auth->id);
		$customerArr =array(''=>'Select a Customer');		

		foreach ($users as $customer =>$c):
			$customerArr[$c['id']] = $c['firstname']. ' '.$c['lastname'];
		endforeach;
		$data['customers'] = $customerArr;

		if ($user_id > 0): //pull this user's cars
			$cars = Car::getCarsByUserId($auth->shop_id, $user_id);
			$carArr =array(''=>'Select a Car');		

			foreach ($cars as $car =>$ca):
				$carArr[$ca['id']] = $ca['model'];
			endforeach;
			$data['cars'] = $carArr;

		endif;


		//get services for dropdown
		$servs = Service::getServicesByShopId($auth->shop_id);
		$servicesArr = [];		
		foreach ($servs as $serv =>$s):
			$servicesArr[$s['id']] = $s['servicename'];
		endforeach;
		$data['services'] = $servicesArr;

		//get status for job
		$stats = JobStatus::getStatuses($auth->shop_id);
		$statsArr = [];		
		foreach ($stats as $stat =>$s):
			$statsArr[$s['id']] = $s['status_name'];
		endforeach;
		$data['statuses'] = $statsArr;

		//get manufacturers for dropdown
		$users = CarManufacturer::getManufacturersList($auth->shop_id);
		$manufacturers =array(''=>'-select-');		

		foreach ($users as $customer =>$c):
			$manufacturers[$c['id']] = $c['name'];
		endforeach;
		$data['manufacturers'] = $manufacturers;


		$data['curDate'] = date('m/d/Y');
		return view('jobs/create', $data);
	}

	public function save(Request $request)
	{
		$auth = $this->auth->user();

		$validator = \Validator::make($request->all(), [
            'user_id' => 'required',
            'car_id' => 'required',
            'start_date' => 'required',
            'start_time' => 'required',
        ]);

        if ($validator->fails()) {
        	$msg = '';        	
        	foreach($validator->messages()->toArray() as $key):
        		$msg .= $key[0]."\n";
        	endforeach;
        	return Response::json(['data' => array('html' =>'failed','errormsg'=>$msg)]);
        }


		if (\Request::ajax()):
			if ($request->input('id')):
				$job= Job::find($request->input('id'));
				$job->start_date = $request->input('start_date');
				$job->start_time = $request->input('start_time');

				$redirectTo = '/jobs/view/'.$request->input('id');
			else:
				$job = new Job;
				$job->shop_id = $auth->shop_id;
				$job->start_date = Helper::formatDate($request->input('start_date'),1);
				$job->start_time = Helper::formatTime($request->input('start_time'),1);
				$job->status_id = $request->input('status_id');
				


			endif;
			
			$job->user_id = $request->input('user_id');
			$job->car_id = $request->input('car_id');
			
			$job->job_name = '';
			$job->notes = $request->input('notes');
			
			$job->save();

			//TODO
			//insert services
			Job::saveServices($request->input('services'), $job->id, $auth);

			//write history
			if ($request->input('id')):
				History::write($auth->shop_id, $job->user_id, 'job', 'Job details were edited.',array(),$job->id);
			else:
				History::write($auth->shop_id, $job->user_id, 'job', 'Job was created.',array(),$job->id);
			endif;

			if (!$request->input('id')):
				$redirectTo = '/jobs/view/'.$job->id;
			endif;

		endif;

		return Response::json(['data' => array('html' =>'saved','redirectTo'=>$redirectTo,'msg'=>'New Job successfully created!')]);
	}

	public function getCarsByUserId($user_id)
	{
		$auth = $this->auth->user();

		

		$cars = Car::getCarsByUserId($auth->shop_id, $user_id);
		
		$resData = array('0'=>'Select Car');
		foreach ($cars as $result) {
			$resData[] = array("id"=>"{$result->id}", "name"=>$result->model);
		}


		return Response::json($resData);

	}

	public function view($id)
	{
		$auth = $this->auth->user();



		$data = [];

		//JS
		$cssArray = ['/plugins/fileupload/jquery.fileupload.css','/plugins/fileupload/jquery.fileupload-ui.css','//blueimp.github.io/Gallery/css/blueimp-gallery.min.css',
		];
		Asset::add($cssArray, 'headerCss');



		$jsArray = ['//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js','//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js',
			'//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js','//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js',
			'/plugins/fileupload/jquery.iframe-transport.js',
			'/plugins/fileupload/jquery.fileupload.js','/plugins/fileupload/jquery.fileupload-process.js','/plugins/fileupload/jquery.fileupload-image.js',
			'/plugins/fileupload/jquery.fileupload-validate.js','/plugins/fileupload/jquery.fileupload-ui.js','/plugins/fileupload/fileuploadmain.js','/back/js/jobs.js',
			];
		Asset::add($jsArray, 'footer');	

		$job= Job::find($id);
		if (empty($job)){
			 return redirect('/jobs');
		}
		
		$car = Car::find($job->car_id);

		$carpic = Car::getCarMainPic($auth->shop_id, $job->car_id);

		if (count($carpic)==0):
			$data['carpic'] = url('/img/icons/icon-car.jpg');
		else:
			$data['carpic'] = url('/uploads/images/'.$carpic);
		endif;

		$data['car']= $car;
		$data['job']= $job;

		$script = "var car_id = $job->car_id;
			var job_id=$job->id";
		Asset::addScript($script, 'ra_js');


		//get status for job
		$stats = JobStatus::getStatuses($auth->shop_id);
		$statsArr = [];		
		foreach ($stats as $stat =>$s):
			$statsArr[$s['id']] = $s['status_name'];
		endforeach;
		$data['statuses'] = $statsArr;

		//get services applied on this job
		$jobServices = Job::getJobServices($id);
		//dd($jobServices);
		$jservices = [];
		foreach ($jobServices as $serv):
			$service = Service::find($serv->service_id);
			$jservices[$serv->service_id] = $service->servicename;
		endforeach;
		$data['jobservices'] = $jservices;

		$data['statClass'] = JobStatus::getStatusClass($job->status_id);

		
		return view('jobs/view', $data);
	}

	public function delete($id)
	{
		$j = Job::find($id);
		$res = $j->delete();

		if ($res):
			$msg = 'Job successfully deleted!';
			$status = 'deleted';
		else:
			$msg = 'Failed deleting!';
			$status = 'failed';
		endif;

		return Response::json(['data' => array('html' =>$status,'redirectTo'=>'/jobs','msg'=>$msg)]);

	}

	public function changeStatus($job_id, $status)
	{
		$auth = $this->auth->user();

		if (\Request::ajax()):
			
				$job= Job::find($job_id);
				$job->status_id = $status;		

				$jobStatus = JobStatus::find($status);

				if ($status ==3 || $status ==4){
					$job->date_completed = date('Y-m-d H:i:s');

					//get items of this job					
					$jobServices = Job::getJobServices($job_id);
					//dd($jobServices);
					
					$subtotal = 0;
					foreach ($jobServices as $serv):
						$service = Service::find($serv->service_id);
						$subtotal = $subtotal +  $service->fee;					

					endforeach;

					$discount = 0;

					$total = $subtotal - $discount;

					//create an invoice
					$exists = Invoice::checkByJobId($job->id);
					if ($exists!=true)://no Invoice yet, create one
						$invoice = new Invoice;
						$invoice->shop_id  = $auth->shop_id;
						$invoice->user_id  = $job->user_id;
						$invoice->job_id  = $job->id;

						$invoicenumberVal = Invoice::getShopLastInvoiceNumber($auth->shop_id);
						$invoice->invoicenumber  = $invoicenumberVal + 1;
						$invoice->due_date = ''; //TODO
						$invoice->invoice_date = date('Y-m-d');
						$invoice->subtotal = $subtotal;
						$invoice->discount  = 0;
						$invoice->total = $total;
						$invoice->balance_due= $total;
						$invoice->status = 'Outstanding';
						$invoice->invoicehash= Helper::random_string(32);
						if ($invoice->save()):
							$servicedWith = '';
							foreach ($jobServices as $serv):	
								$service = Service::find($serv->service_id);							
								//insert
								$item = new InvoiceItem;
								$item->invoice_id = $invoice->invoice_id;
								$item->service_id = $serv->service_id;
								$item->user_id = $job->user_id;
								$item->quantity = 1;
								$item->unit_price = $service->fee;
								$item->is_taxable = 0;
								$item->discount = 0;
								$item->description = '';
								$item->timestamps = false;
								$item->save();

								$servicedWith .= $service->servicename. ', ';

							endforeach;
							History::write($auth->shop_id, $job->user_id, 'job', "Invoice <a href='/invoices/view/".$invoice->invoice_id."'>#".Helper::formatInvoiceNumber($invoice->invoicenumber). "</a> was created." ,array(),$job->id);

							History::write($auth->shop_id, $job->user_id, 'car', "Car was serviced with the following: " .$servicedWith .' <br/> Job ID <a href="/jobs/view/'.$job->id.'">'.$job->id.'</a>',array(),$job->id,$job->car_id);

						endif;
					endif;
				}//marked as finished


				if($job->save()){
					//write history
					History::write($auth->shop_id, $job->user_id, 'job', "Job status was set to '".$jobStatus->status_name."'" ,array(),$job->id);
					$msg = "Successfully changed status.";
				} else {
					$msg = "Failed changing status.";
				}

			
			endif;

		return Response::json(['data' => array('html' =>'saved','redirectTo'=>'/jobs/view/'.$job_id,'msg'=>$msg)]);
	}


	public function getTimeline($job_id)
	{
		$auth = $this->auth->user();
		$dates = History::getJobTimelineDates($auth->shop_id, $job_id);

		$colorItems = array('blue','green','yellow','red');
		$theDates = [];

		$items = '<ul class="timeline timeline-inverse" id="view_profile_timeline">';
		foreach ($dates as $date):
			$theDate = substr($date->created_at,0,10);
			if (!in_array( $theDate, $theDates)):
				$theDates[] = $theDate;
				$dateColor = $colorItems[mt_rand(0,3)];
				
				$items .= 
				'<li class="time-label">
			          <span class="bg-'.$dateColor.'">
			            '.Helper::formatDate($date->created_at,5).'
			          </span>
			    </li>';
			endif;
			$iconColor = $colorItems[mt_rand(0,3)];
			$timeago = $date->created_at->diffForHumans();
			$items .= '
			<li>
      			<i class="fa fa-'.History::getIconByModule($date->module,$date->description).' bg-'.$iconColor.'"></i>
      			<div class="timeline-item">
      				<span class="time"><i class="fa fa-clock-o"></i> '.$timeago.'</span>
      				<h3 class="timeline-header">'.$date->description.'</h3>
      				<div class="timeline-body">
      					'.History::showFormattedHistoryData($date->module, $date->description,$date->data_arr).'
      				</div>
      			</div>
      		</li>
			';

    	endforeach;
    	$items .= '</ul>';
		$returnHTML = $items;
		return response()->json(array('success' => true, 'html'=>$returnHTML));
	}

	public function getMessages($job_id)
	{
		

		$auth = $this->auth->user();

		$html ='';

		$messages = Message::getMessagesOnThisJob($auth->shop_id, $job_id);

		$right = '';
		$pull = 'left';
		$timestamppull = 'right';
		$prevUser = 0;
		$curUser = 0;
		foreach ($messages as $msg):
			$user = User::find($msg->sender);

			if ($user->profile_pic!=''):
        		$userprofilepic = url('/uploads/profile/'.$user->profile_pic);
        	else:
        		$userprofilepic = url('/img/icons/icon-user-big.jpg');
        	endif;

        	$right = ($right=='')?'':'right';
        	$pull = ($pull=='left')?'left':'right';
        	$timestamppull = ($timestamppull=='left')?'left':'right';

        	
			$timeago = $msg->created_at->diffForHumans();
			$curUser = $msg->sender;
		

			$html  .= '
					  <div class="direct-chat-msg '.$right.'">
					    <div class="direct-chat-info clearfix">
					      <span class="direct-chat-name pull-'.$pull.'">Alexander Pierce</span>
					      <span class="direct-chat-timestamp pull-'.$timestamppull.'">'.$timeago.'</span>
					    </div>
					    <!-- /.direct-chat-info -->
					    <img class="direct-chat-img" src="'.$userprofilepic.'" alt="'.$user->firstname.' '.$user->lastname.'"><!-- /.direct-chat-img -->
					    <div class="direct-chat-text">
					      '.$msg->message_text.'
					    </div>
					    <!-- /.direct-chat-text -->
					  </div>
					  <!-- /.direct-chat-msg -->';
			if ($curUser==$prevUser && $prevUser !=0):
				$right = ($right=='')?'right':'';
				$pull = ($pull=='left')?'right':'left';
				$timestamppull = ($timestamppull=='left')?'right':'left';
			endif;

			$prevUser = $curUser;
		endforeach;

		return response()->json(array('success' => true, 'html'=>$html));
	}

}
