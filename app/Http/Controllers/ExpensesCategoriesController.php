<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use Response,Asset,Html;

use App\User, App\Models\Invoice, App\Models\Expense, App\Models\ExpenseCategory;

class ExpensesCategoriesController extends Controller {

	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth');

	}

	/**
	 * Show the payments listing page
	 *
	 * @return Response
	 */
	public function index()
	{
		$auth = $this->auth->user();
		if (!$auth->hasRole('superadmin') && !$auth->hasRole('shopadmin')){
			 return redirect('/login');
		}  

		//JS
		$cssArray = ['/plugins/datatables/dataTables.bootstrap.css'
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/back/js/cars.js','/plugins/datatables/jquery.dataTables.min.js','/plugins/datatables/dataTables.bootstrap.js'];
		Asset::add($jsArray, 'footer');	

		//get the payments
		$expCategories = ExpenseCategory::getExpensesCategoriesByShopId($auth->shop_id);

		$data['expCategories'] = $expCategories;

		return view('expensescategories/index', $data);
	}

	public function save(Request $request)
	{
		$auth = $this->auth->user();

		$validator = \Validator::make($request->all(), [
            'name' => 'required',            
        ]);

        if ($validator->fails()) {
        	$msg = '';        	
        	foreach($validator->messages()->toArray() as $key):
        		$msg .= $key[0]."\n";
        	endforeach;
        	return Response::json(['data' => array('html' =>'failed','errormsg'=>$msg)]);
        }

		if (\Request::ajax()):
			if ($request->input('id')):
				$exp= ExpenseCategory::find($request->input('id'));
			
			else:
				$exp = new ExpenseCategory;
				$exp->shop_id = $auth->shop_id;
			endif;
			
			$exp->name = $request->input('name');
			$exp->save();

		endif;

		return Response::json(['data' => array('html' =>'saved','redirectTo'=>'/expenses_categories','msg'=>'New Category successfully saved!')]);
	}

	public function delete($id)
	{
		$exp = ExpenseCategory::find($id);
		$res = $exp->delete();

		if ($res):
			$msg = 'Expense Category successfully deleted!';
			$status = 'deleted';
		else:
			$msg = 'Failed deleting!';
			$status = 'failed';
		endif;

		return Response::json(['data' => array('html' =>$status,'redirectTo'=>'/expenses_categories','msg'=>$msg)]);

	}
	

}
