<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use Response,Asset,Html;

use App\Models\Package, App\Models\Product;

class PackagesController extends Controller {

	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth');

	}

	/**
	 * Show the payments listing page
	 *
	 * @return Response
	 */
	public function index()
	{
		$auth = $this->auth->user();
		if (!$auth->hasRole('superadmin') && !$auth->hasRole('shopadmin')){
			 return redirect('/login');
		}  

		//JS
		$cssArray = ['/components/bootstrap-datepicker/css/datepicker3.css','/plugins/datatables/dataTables.bootstrap.css'
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/back/js/cars.js','/plugins/datatables/jquery.dataTables.min.js','/plugins/datatables/dataTables.bootstrap.js'];
		Asset::add($jsArray, 'footer');	

		//get the packages
		$packages= Package::gePackagesByShopId($auth->shop_id);

		$data['packages'] = $packages;

		return view('packages/index', $data);
	}

	public function save(Request $request)
	{
		$auth = $this->auth->user();

		if (\Request::ajax()):
			if ($request->input('id')):
				$pak= Package::find($request->input('id'));
			
			else:
				$pak = new Package;
				$pak->shop_id = $auth->shop_id;
			endif;
			
			$pak->package_name = $request->input('package_name');
			$pak->save();

		endif;

		return Response::json(['data' => array('html' =>'saved',)]);
	}

	public function delete($id)
	{
		$pak = Package::find($id);
		$res = $pak->delete();

		if ($res):
			$msg = 'Package successfully deleted!';
			$status = 'deleted';
		else:
			$msg = 'Failed deleting!';
			$status = 'failed';
		endif;

		return Response::json(['data' => array('html' =>$status,'redirectTo'=>'/packages','msg'=>$msg)]);

	}

	

}
