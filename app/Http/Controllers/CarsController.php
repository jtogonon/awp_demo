<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use Response,Asset,Html;

use App\User, App\Models\Car, App\Models\CarManufacturer,App\Models\History;
use App\Helpers\Helper;

class CarsController extends Controller {

	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth');

	}

	/**
	 * Show the users listing page
	 *
	 * @return Response
	 */
	public function index()
	{
		$auth = $this->auth->user();
		if (!$auth->hasRole('superadmin') && !$auth->hasRole('shopadmin')){
			 return redirect('/login');
		}  

		//JS
		$cssArray = ['/components/bootstrap-datepicker/css/datepicker3.css','/plugins/datatables/dataTables.bootstrap.css','/plugins/select2/select2.min.css'
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['back/js/cars.js','/plugins/datatables/jquery.dataTables.min.js','/plugins/datatables/dataTables.bootstrap.js','/plugins/select2/select2.full.min.js'];
		Asset::add($jsArray, 'footer');	

		//get the cars
		$cars = Car::getCarsByShopId($auth->shop_id);
		$data['cars'] = $cars;


		//get customers for dropdown
		$users = User::getCustomersList($auth->shop_id, $auth->id);
		$customerArr =array(''=>'-select-');		



		foreach ($users as $customer =>$c):
			$customerArr[$c['id']] = $c['firstname']. ' '.$c['lastname'];
		endforeach;
		$data['customers'] = $customerArr;


		//get manufacturers for dropdown
		$users = CarManufacturer::getManufacturersList($auth->shop_id);
		$manufacturers =array(''=>'-select-');		

		foreach ($users as $customer =>$c):
			$manufacturers[$c['id']] = $c['name'];
		endforeach;
		$data['manufacturers'] = $manufacturers;

		return view('cars/index', $data);
	}

	public function save(Request $request)
	{
		$auth = $this->auth->user();

		$validator = \Validator::make($request->all(), [
            'user_id' => 'required',
            'plate_number' => 'required',
            'model' => 'required',
        ]);

        if ($validator->fails()) {
        	$msg = '';        	
        	foreach($validator->messages()->toArray() as $key):
        		$msg .= $key[0]."\n";
        	endforeach;
        	return Response::json(['data' => array('html' =>'failed','errormsg'=>$msg)]);
        }


		if (\Request::ajax()):
			if ($request->input('id')):
				$car = Car::find($request->input('id'));
				$msg = 'Car data changes successfully saved!';
				$historyMsg = 'Car info was modified';
			else:
				$car = new Car;
				$car->shop_id = $auth->shop_id;
				$msg =  'New Car successfully saved!';
				$historyMsg = 'New Car was added: ' . $request->input('model');
			endif;
			$car->user_id = $request->input('user_id');
			$car->plate_number = $request->input('plate_number');
			$car->model= $request->input('model');
			$car->notes= $request->input('notes');
			$car->manufacturer_id= $request->input('manufacturer_id');

			if($car->save()):
				History::write($auth->shop_id, $request->input('user_id'), 'car', $historyMsg ,array(), null, $car->id);
			endif;

		endif;

		return Response::json(['data' => array('html' =>'saved','redirectTo'=>'/cars','msg'=>$msg)]);
	}

	
	public function delete($id)
	{
		$car = Car::find($id);

		if (\Request::ajax()){
			$res = $car ->delete();

			if ($res):
				$msg = 'Car successfully deleted!';
				$status = 'deleted';
			else:
				$msg = 'Failed deleting!';
				$status = 'failed';
			endif;

			return Response::json(['data' => array('html' =>$status,'redirectTo'=>'/cars','msg'=>$msg)]);
		}

	}

	public function view($id)
	{
		$auth = $this->auth->user();

		$data = [];

		$jsArray = ['/back/js/cars.js','/plugins/datatables/jquery.dataTables.min.js','/plugins/datatables/dataTables.bootstrap.js'];
		Asset::add($jsArray, 'footer');	

		$car = Car::find($id);
		if (!$car):
			return redirect('/cars');
		endif;
		$data['car'] = $car;

		$carpic = Car::getCarMainPic($auth->shop_id, $id);
		if (count($carpic)==0):
			$data['carpic'] = url('/img/icons/icon-car.jpg');
		else:
			$data['carpic'] = url('/uploads/images/'.$carpic);
		endif;


		//get manufacturers for dropdown
		$users = CarManufacturer::getManufacturersList($auth->shop_id);
		$manufacturers =array(''=>'-select-');		

		foreach ($users as $customer =>$c):
			$manufacturers[$c['id']] = $c['name'];
		endforeach;
		$data['manufacturers'] = $manufacturers;

		return view('cars/view', $data);
	}

	public function getTimeline($id)
	{
		$auth = $this->auth->user();
		$dates = History::getCarTimelineDates($auth->shop_id, $id);

		$colorItems = array('blue','green','yellow','red');
		$theDates = [];

		$items = '<ul class="timeline timeline-inverse" id="view_profile_timeline">';
		foreach ($dates as $date):
			$theDate = substr($date->created_at,0,10);
			if (!in_array( $theDate, $theDates)):
				$theDates[] = $theDate;
				$dateColor = $colorItems[mt_rand(0,3)];
				
				$items .= 
				'<li class="time-label">
			          <span class="bg-'.$dateColor.'">
			            '.Helper::formatDate($date->created_at,5).'
			          </span>
			    </li>';
			endif;
			$iconColor = $colorItems[mt_rand(0,3)];
			$timeago = $date->created_at->diffForHumans();
			$items .= '
			<li>
      			<i class="fa fa-'.History::getIconByModule($date->module,$date->description).' bg-'.$iconColor.'"></i>
      			<div class="timeline-item">
      				<span class="time"><i class="fa fa-clock-o"></i> '.$timeago.'</span>
      				<h3 class="timeline-header">'.$date->description.'</h3>
      				<div class="timeline-body">
      				'.History::showFormattedHistoryData($date->module, $date->description,$date->data_arr).'
      				</div>
      			</div>
      		</li>
			';

    	endforeach;
    	$items .= '</ul>';
		$returnHTML = $items;
		return response()->json(array('success' => true, 'html'=>$returnHTML));
	}

}
