<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use Response,Asset,Html;

use App\Models\Vendor, App\Models\Product, App\Models\ProductManufacturer;

class ProductManufacturersController extends Controller {

	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth');

	}

	/**
	 * Show the payments listing page
	 *
	 * @return Response
	 */
	public function index()
	{
		$auth = $this->auth->user();
		if (!$auth->hasRole('superadmin') && !$auth->hasRole('shopadmin')){
			 return redirect('/login');
		}  

		//JS
		$cssArray = ['/components/bootstrap-datepicker/css/datepicker3.css','/plugins/datatables/dataTables.bootstrap.css'
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/back/js/cars.js','/plugins/datatables/jquery.dataTables.min.js','/plugins/datatables/dataTables.bootstrap.js'];
		Asset::add($jsArray, 'footer');	

		//get the vendors
		$manus= ProductManufacturer::getProdManufacturersByShopId($auth->shop_id);

		$data['manufacturers'] = $manus;

		return view('productmanufacturers/index', $data);
	}

	public function save(Request $request)
	{
		$auth = $this->auth->user();

		$validator = \Validator::make($request->all(), [
            'name' => 'required',            
        ]);

        if ($validator->fails()) {
        	$msg = '';        	
        	foreach($validator->messages()->toArray() as $key):
        		$msg .= $key[0]."\n";
        	endforeach;
        	return Response::json(['data' => array('html' =>'failed','errormsg'=>$msg)]);
        }

		if (\Request::ajax()):
			if ($request->input('id')):
				$prodMan= ProductManufacturer::find($request->input('id'));
			
			else:
				$prodMan = new ProductManufacturer;
				$prodMan->shop_id = $auth->shop_id;
			endif;
			
			$prodMan->name = $request->input('name');
			$prodMan->save();

		endif;

		return Response::json(['data' => array('html' =>'saved','redirectTo'=>'/product_manufacturers','msg'=>'New Product Manufacturer successfully saved!')]);
	}/*end function*/


	public function delete($id)
	{
		$man = ProductManufacturer::find($id);
		$res = $man->delete();

		if ($res):
			$msg = 'Manufacturer entry successfully deleted!';
			$status = 'deleted';
		else:
			$msg = 'Failed deleting!';
			$status = 'failed';
		endif;

		return Response::json(['data' => array('html' =>$status,'redirectTo'=>'/product_manufacturers','msg'=>$msg)]);

	}

}
