<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use Response,Asset,Html;

use App\User, App\Models\Invoice, App\Models\Payment, App\Models\PaymentMethod, App\Models\History;
use App\Helpers\Helper;
class PaymentsController extends Controller {

	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth');

	}

	/**
	 * Show the payments listing page
	 *
	 * @return Response
	 */
	public function index()
	{
		$auth = $this->auth->user();
		if (!$auth->hasRole('superadmin') && !$auth->hasRole('shopadmin')){
			 return redirect('/login');
		}  

		//JS
		$cssArray = ['/components/bootstrap-datepicker/css/datepicker3.css','/plugins/datatables/dataTables.bootstrap.css'
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/back/js/payments.js','/plugins/datatables/jquery.dataTables.min.js','/plugins/datatables/dataTables.bootstrap.js'];
		Asset::add($jsArray, 'footer');	

		//get the payments
		$payments = Payment::gePaymentsByShopId($auth->shop_id);

		$data['payments'] = $payments;

		return view('payments/index', $data);
	}

	public function create($invoice_id = null)
	{
		$data = [];

		$auth = $this->auth->user();

		//JS
		$cssArray = ['/plugins/datepicker/datepicker3.css','/plugins/select2/select2.min.css','/plugins/timepicker/bootstrap-timepicker.min.css'
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/back/js/payments.js','/plugins/select2/select2.full.min.js','/plugins/datepicker/bootstrap-datepicker.js','/plugins/timepicker/bootstrap-timepicker.min.js'];
		Asset::add($jsArray, 'footer');	

		//get payment methods for dropdown		
		$methods = PaymentMethod::getMethodsForSelect($auth->shop_id);
		$methodsArr = [];			
		foreach ($methods as $method =>$m):
			$methodsArr[$m['id']] = $m['name'];
		endforeach;
		if ($invoice_id==null){
			//make a dropdown of customers
			//get customers for dropdown
			$users = User::getCustomersList($auth->shop_id, $auth->id);
			$customerArr =array(''=>'Select a Customer');		

			foreach ($users as $customer =>$c):
				$customerArr[$c['id']] = $c['firstname']. ' '.$c['lastname'];
			endforeach;
			$data['customers'] = $customerArr;
			$data['invoice_id'] = 0;
			$data['invoice'] = new Invoice;
		} else {
			$invoice = Invoice::find($invoice_id);
			$data['invoice'] = $invoice;
			$data['invoice_id'] = $invoice_id;
		}
		
		$data['paymentMethods'] = $methodsArr;

		return view('payments/create', $data);
	}

	public function save(Request $request)
	{
		$auth = $this->auth->user();

		$msg = '';
		if (\Request::ajax()):
			if ($request->input('id')):
				$payment= Payment::find($request->input('id'));
			
			else:
				$payment = new Payment;
				$payment->shop_id = $auth->shop_id;
			endif;
			//get invoice data
			if ($request->input('invoice_id')>0):
				$invoice = Invoice::find($request->input('invoice_id'));
			endif;
			$payment->user_id 	= $request->input('user_id');
			$payment->amount 	= $request->input('payment_received');
			$payment->payment_method_id = $request->input('payment_method_id');
			$payment->payment_received_on 	= Helper::formatDate($request->input('payment_received_on'),1);
			$payment->payment_received    	= $request->input('payment_received');
			$payment->invoice_id	    	= $request->input('invoice_id');
			$payment->notes 		    	= $request->input('notes');
			$payment->save();

			if ($invoice->total == $request->input('payment_received')){
				$invoice->status = 'Paid';
				$invoice->balance_due = 0;
				$invoice->save();
			} else {
				//update invoice
				$newBalance = $invoice->balance_due - $payment->payment_received;
				$invoice->balance_due = $newBalance;
				$invoice->save();
			}
			if ($invoice->balance_due == 0){
				$invoice->status = 'Paid';
				$invoice->save();

				$history = 'Paid Invoice # <a href="/invoices/view/'.$invoice->invoice_id.'">'.Helper::formatInvoiceNumber($invoice->invoicenumber).'</a>.';
				History::write($auth->shop_id, $invoice->user_id, 'payment', $history,array(),$invoice->job_id);
			}

			$msg = 'Successfully made a payment.';

		endif;

		return Response::json(['data' => array('html' =>'saved','redirectTo'=>'/payments','msg'=>$msg)]);
	}

	public function delete($id)
	{
		$pay = Payment::find($id);
		$res = $pay->delete();

		if ($res):
			$msg = 'Payment successfully deleted!';
			$status = 'deleted';
		else:
			$msg = 'Failed deleting!';
			$status = 'failed';
		endif;

		return Response::json(['data' => array('html' =>$status,'redirectTo'=>'/payments','msg'=>$msg)]);

	}

}
