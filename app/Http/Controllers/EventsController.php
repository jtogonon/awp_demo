<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use Response,Asset,Html;

use App\Models\Vendor, App\Models\Job, App\User, App\Models\Service;
use App\Models\Event;

use App\Helpers\Helper;

class EventsController extends Controller {

	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth');
	}

	/**
	 * Show the payments listing page
	 *
	 * @return Response
	 */
	public function index()
	{
		$auth = $this->auth->user();
		if (!$auth->hasRole('superadmin') && !$auth->hasRole('shopadmin')){
			 return redirect('/login');
		}  

		//JS
		$cssArray = ['/plugins/fullcalendar/fullcalendar.min.css'];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/back/js/events.js','https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js','/plugins/fullcalendar/fullcalendar.min.js'];
		Asset::add($jsArray, 'footer');	

		//get the jobs
		$jobss = Job::getJobsByShopId($auth->shop_id);
        $jobs = array();
        foreach ($jobss as $event){
        	$jobs[] = array('id'=>$event->id,
        					'event_type'=>'job',
        					  'event_name'=>$event->job_name,
        					  'start_date'=>$event->start_date,
        					  'start_time'=>$event->start_time,
        					  'end_date'=>$event->end_date,
        					  'end_time'=>$event->end_time,  
        					  'customer'=>$event->getCustomer(),
        					  );
        }
		$jobs = Helper::arrayToObject($jobs);

		//get events
		$eventss = Event::getEventsByShopId($auth->shop_id);
        $events = array();        
        foreach ($eventss as $event){
        	$events[] = array('id'=>$event->id,
        					'event_type'=>'appointment',
        					  'event_name'=>$event->event_name,
        					  'start_date'=>$event->start_date,
        					  'start_time'=>$event->start_time,
        					  'end_date'=>$event->end_date,
        					  'end_time'=>$event->end_time,  
        					  'customer'=>$event->getCustomer(),
        					  );
        }


		$events = Helper::arrayToObject($events);
		$newevents = (object) array_merge((array) $events, (array) $jobs);
		$data['events'] = $newevents;

		$data['siteUrl'] = url();

		return view('events/index', $data); //show the calendar here
	}

	public function save(Request $request)
	{
		$auth = $this->auth->user();

		$validator = \Validator::make($request->all(), [
            'event_name' => 'required',           
            
        ]);

        if ($validator->fails()) {
        	$msg = '';        	
        	foreach($validator->messages()->toArray() as $key):
        		$msg .= $key[0]."\n";
        	endforeach;
        	return Response::json(['data' => array('html' =>'failed','errormsg'=>$msg)]);
        }

		if (\Request::ajax()):
			if ($request->input('event_id')):
				$job= Event::find($request->input('event_id'));
			
			else:
				$job = new Event;
				$job->shop_id = $auth->shop_id;
			endif;
			
			$job->user_id = $request->input('user_id');
			$job->car_id = $request->input('car_id');
			$job->event_name = $request->input('event_name');
			$job->event_type= 'appointment';
			$job->notes = $request->input('notes');
			$job->start_date = Helper::formatDate($request->input('start_date'),1);
			$job->start_time = Helper::formatTime($request->input('start_time'),1);
			$job->save();

		endif;

		return Response::json(['data' => array('html' =>'saved','redirectTo'=>'/calendar','msg'=>'New Event successfully saved!')]);
	}

	public function delete($id)
	{
		$v = Vendor::find($id);
		$res = $v->delete();

		if ($res):
			$msg = 'Event successfully deleted!';
			$status = 'deleted';
		else:
			$msg = 'Failed deleting!';
			$status = 'failed';
		endif;

		return Response::json(['data' => array('html' =>$status,'redirectTo'=>'/vendors','msg'=>$msg)]);

	}

	/**
	 * @param $date
	 * @return \Illuminate\View\View
     */
	public function create($date)
	{
		$data = [];
		$auth = $this->auth->user();

		//JS
		$cssArray = ['/plugins/datepicker/datepicker3.css','/plugins/select2/select2.min.css','/plugins/timepicker/bootstrap-timepicker.min.css'
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/back/js/jobs.js','/plugins/select2/select2.full.min.js','/plugins/datepicker/bootstrap-datepicker.js','/plugins/timepicker/bootstrap-timepicker.min.js'];
		Asset::add($jsArray, 'footer');	


		//get customers for dropdown
		$users = User::getCustomersList($auth->shop_id,$auth->id);
		$customerArr =array(''=>'Select a Customer');		

		foreach ($users as $customer =>$c):
			$customerArr[$c['id']] = $c['firstname']. ' '.$c['lastname'];
		endforeach;
		$data['customers'] = $customerArr;


		$data['curDate'] = date('m/d/Y');
		return view('events/create', $data); //add event form
	}

	public function view($id)
	{
		$auth = $this->auth->user();
		$event = Event::find($id);
		if ($event->shop_id != $auth->shop_id){
			return redirect('/calendar');
		}

		//JS
		$cssArray = ['/plugins/datepicker/datepicker3.css','/plugins/select2/select2.min.css','/plugins/timepicker/bootstrap-timepicker.min.css'
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/back/js/jobs.js','/plugins/select2/select2.full.min.js','/plugins/datepicker/bootstrap-datepicker.js','/plugins/timepicker/bootstrap-timepicker.min.js'];
		Asset::add($jsArray, 'footer');	


		//get customers for dropdown
		$users = User::getCustomersList($auth->shop_id,$auth->id);
		$customerArr =array(''=>'Select a Customer');		

		foreach ($users as $customer =>$c):
			$customerArr[$c['id']] = $c['firstname']. ' '.$c['lastname'];
		endforeach;
		$data['customers'] = $customerArr;

		$data['event'] = $event;
		$data['curDate'] = date('m/d/Y');
		return view('events/view', $data); //event view	
	}
}
