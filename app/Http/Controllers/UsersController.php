<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use Response,Asset,Html;


use App\User, App\Models\Role, App\Models\Permission,App\Models\History;
use App\Models\Country, App\Models\Car, App\Models\Shop;
use App\Models\Membership, App\Models\UserMembership,App\Models\UserDetail, App\Models\Payment;
use App\Models\Timezone, App\Models\Currency, App\Models\MembershipItem, App\Models\Service,App\Models\Job;

use App\Helpers\Helper;
class UsersController extends Controller {

	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth');

	}

	/**
	 * Show the users listing page
	 *
	 * @return Response
	 */
	public function index()
	{
		$auth = $this->auth->user();
		if (!$auth->hasRole('superadmin') && !$auth->hasRole('shopadmin')){
			 return redirect('/login');
		}  

		//JS
		$cssArray = ['/components/bootstrap-datepicker/css/datepicker3.css','/plugins/datatables/dataTables.bootstrap.css'
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/back/js/customers.js','/plugins/datatables/jquery.dataTables.min.js','/plugins/datatables/dataTables.bootstrap.js'];
		Asset::add($jsArray, 'footer');	

		//get the users
		$users = User::getCustomersByShopId($auth->shop_id, $auth->id);
		
		$data['customers'] = $users;

		return view('users/index', $data);
	}

	public function save(Request $request)
	{
		$auth = $this->auth->user();

		if (\Request::ajax()):
			if ($request->input('id')):
				$user = User::find($request->input('id'));
			
			else:
				$user = new User;
				$user->shop_id = $auth->shop_id;
				$user->email = $request->input('email');
			endif;

			$user_old = $user->getAttributes();
			
			$user->firstname = $request->input('firstname');
			$user->lastname = $request->input('lastname');

			$user->save();

			//check if there are address details
			if ($request->input('address1') ) :
				if($request->input('userdetails_id') > 0):
					$userdetails = UserDetail::find($request->input('userdetails_id'));			
				else:
					$userdetails = new UserDetail;					
				endif;
				$userdetails_old = $userdetails->getAttributes();

				$userdetails->user_id= $request->input('id');
				$userdetails->address1 = $request->input('address1');
				$userdetails->address2 = $request->input('address2');
				$userdetails->city = $request->input('city');
				$userdetails->state = $request->input('state');
				$userdetails->zip = $request->input('zip');
				$userdetails->country = $request->input('country');
				$userdetails->notes = $request->input('notes');
				$userdetails->save();
			endif;

			if ($request->input('sendwelcomeemail')):
				//TODO, before make the send mail functional, make the login page for customers first
				//send welcome email
				    /*$datavars = array('publicUrl'=>url('/'),
				    			'shop_logo'=>url('/'),
				    			'username'=>$request->input('email'),	    			
				    			'main_sender'=>'shop',
				    			'shopName'=>$auth->shop->name,
				    			);
				   	$recipient = array(
				   		'email'=>$request->input('email'),
				   		'name'=>$request->input('name'));
					Mail::send('emails.welcomecustomer', $datavars, function($msg) use ($recipient) 
				    {
				        $msg->from( 'support@autoworkspro.com', 'Support');

				        $msg->to($recipient['email'], $recipient['name'])->subject( 'Welcome to AutoWorksPro');
				        //$msg->to('jetogonon@gmail.com', \Input::get('name'))->subject( 'Welcome to AutoWorksPro');
				    });*/
			endif;

			//write history
			if ($request->input('id')):
				History::write($auth->shop_id, $user->id, 'user', 'User details changed.',array('old_userdata'=>$user_old,'old_userdetails'=>$userdetails_old));
				$msg = 'Changes to customer details saved!';
			else:
				History::write($auth->shop_id, $user->id, 'user', 'User account was created manually',array());
				$msg = 'New User successfully saved!';
			endif;

		endif;

		if ($auth->id == $user->id){
			$redirectTo = '/my_account';
		} else {
			$redirectTo = '/users';
		}

		if ($request->input('for_job_form')=='1'): //the form is in the job form, creation is from that page
			$redirectTo = '/jobs/new/'.$user->id;
			return Response::json(['data' => array('html' =>'redirect','redirectTo'=>$redirectTo)]);
		else :
			return Response::json(['data' => array('html' =>'saved','redirectTo'=>$redirectTo,'msg'=>$msg)]);
		endif;
	}

	public function my_account()
	{
		$data = [];
		$auth = $this->auth->user();
		$user = User::find($auth->id);

		//JS
		$cssArray = ['/components/bootstrap-datepicker/css/datepicker3.css','/plugins/datatables/dataTables.bootstrap.css','/plugins/fileupload/jquery.fileupload.css'];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/plugins/fileupload/jquery.fileupload.js','/plugins/fileupload/jquery.iframe-transport.js','/back/js/customers.js','/plugins/datatables/jquery.dataTables.min.js','/plugins/datatables/dataTables.bootstrap.js',];
		Asset::add($jsArray, 'footer');	
		

			$data['user']= $user;

			$userDetails = $user->getUserDetails();
			if ($userDetails):
				$data['userDetails'] = $user->getUserDetails();
			else:
				$data['userDetails'] = new UserDetail;
			endif;

			//get countries for select
			$countries = Country::getListForSelect();
			$countryArr =array(''=>'Select a Country');	

			foreach ($countries as $country =>$c):
				$countryArr[$c['id']] = $c['name'];
			endforeach;
			$data['countries'] = $countryArr;


			//get timezones
			$zones = Timezone::getListForSelect();
			$timezoneArr =array(''=>'Select a Timezone');	

			foreach ($zones as $zone =>$z):
				$timezoneArr[$z['id']] = $z['name'];
			endforeach;
			$data['timezones'] = $timezoneArr;

			//get currences
			$currencies = Currency::getListForSelect();
			$currencyArr =array(''=>'Select a Currency');	

			foreach ($currencies as $cur =>$c):
				$currencyArr[$c['id']] = $c['name'];
			endforeach;
			$data['currencies'] = $currencyArr;



			//get shop details
			$data['shop'] = Shop::find($auth->shop_id);
			$data['shop_admin_account'] = true;


		return view('users/my_account', $data);
		
	}

	public function saveshopsettings(Request $request)
	{
		$auth = $this->auth->user();

		if (\Request::ajax()):
			if ($request->input('shop_id')):
				$shop = Shop::find($request->input('shop_id'));
				$shop->address1 = $request->input('address1');
				$shop->address2 = $request->input('address2');
				$shop->city = $request->input('city');
				$shop->state = $request->input('state');
				$shop->zip = $request->input('zip');
				$shop->country = $request->input('country');
				$shop->tagline = $request->input('tagline');
				$shop->shop_email = $request->input('shop_email');
				$shop->phone = $request->input('phone');
				$shop->timezone = $request->input('timezone');
				$shop->currency_id = $request->input('currency_id');
				$shop->save();

			endif;
		endif;

		$msg = 'Successfully saved shop settings';

		return Response::json(['data' => array('html' =>'saved','redirectTo'=>'/my_account','msg'=>$msg)]);
	}


	public function delete($id)
	{
		$user = User::find($id);

		if (\Request::ajax()){
			$res = $user ->delete();

			if ($res):
				$msg = 'User successfully deleted!';
				$status = 'deleted';
			else:
				$msg = 'Failed deleting!';
				$status = 'failed';
			endif;

			return Response::json(['data' => array('html' =>$status,'redirectTo'=>'/users','msg'=>$msg)]);
		}

	}
	

	public function view($id)
	{
		$data = [];
		$auth = $this->auth->user();
		$user = User::find($id);
		$data['shop_admin_account'] = false;

		//JS
		$cssArray = ['/components/bootstrap-datepicker/css/datepicker3.css','/plugins/datatables/dataTables.bootstrap.css'];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/back/js/customers.js','/plugins/datatables/jquery.dataTables.min.js','/plugins/datatables/dataTables.bootstrap.js'];
		Asset::add($jsArray, 'footer');	
		
		if ($user && $user->shop_id == $auth->shop_id):
			$data['user']= $user;

			$data['usermembership'] = false;
			$userMembership = $user->getUserMembership();		

			if ($userMembership):
				$data['userMem'] = $userMembership;
				$data['usermembership'] = Membership::find($userMembership->id);
			endif;

			$userDetails = $user->getUserDetails();
			if ($userDetails):
				$data['userDetails'] = $user->getUserDetails();
			else:
				$data['userDetails'] = new UserDetail;
			endif;

			//get countries for select
			$countries = Country::getListForSelect();
			$countryArr =array(''=>'Select a Country');	

			foreach ($countries as $country =>$c):
				$countryArr[$c['id']] = $c['name'];
			endforeach;
			$data['countries'] = $countryArr;

			$levels = Membership::getMembershipListForSelect($auth->shop_id);
			$membershipArr =array(''=>'Select a Membership Level');	

			foreach ($levels as $cmem =>$l):
				$membershipArr[$l['id']] = $l['membership_name'];
			endforeach;

			$data['membershipLevels'] = $membershipArr;

			$data['cars']   = Car::getCarsByUserId($auth->shop_id, $user->id);

			$jobsPaid = UserMembership::getMembersPaidServicesFromDate($auth->shop_id, $user->id, null);
			$data['paidServices'] = count($jobsPaid);

			$data['paymentsMade'] = Payment::getPaymentsMadeByUser($user->id);

			return view('users/view', $data);
		else:
			return redirect('/dashboard');//no user found, then redirect 
		endif;
	}


	public function getTimeline($user_id)
	{
		$auth = $this->auth->user();
		$dates = History::getUserTimelineDates($auth->shop_id, $user_id);

		$colorItems = array('blue','green','yellow','red');
		$theDates = [];

		$items = '<ul class="timeline timeline-inverse" id="view_profile_timeline">';
		foreach ($dates as $date):
			$theDate = substr($date->created_at,0,10);
			if (!in_array( $theDate, $theDates)):
				$theDates[] = $theDate;
				$dateColor = $colorItems[mt_rand(0,3)];
				
				$items .= 
				'<li class="time-label">
			          <span class="bg-'.$dateColor.'">
			            '.Helper::formatDate($date->created_at,5).'
			          </span>
			    </li>';
			endif;
			$iconColor = $colorItems[mt_rand(0,3)];
			$timeago = $date->created_at->diffForHumans();
			$items .= '
			<li>
      			<i class="fa fa-'.History::getIconByModule($date->module).' bg-'.$iconColor.'"></i>
      			<div class="timeline-item">
      				<span class="time"><i class="fa fa-clock-o"></i> '.$timeago.'</span>
      				<h3 class="timeline-header">'.$date->description.'</h3>
      				<div class="timeline-body">
      				</div>
      			</div>
      		</li>
			';

    	endforeach;
    	$items .= '</ul>';
		$returnHTML = $items;
		return response()->json(array('success' => true, 'html'=>$returnHTML));
	}

	public function save_usermembership(Request $request)
	{
		$msg = '';
		$auth = $this->auth->user();
		if ($request->input('user_id')):
			$user_id = $request->input('user_id');
			$end_date = date('Y-m-d', strtotime('+1 year'));
			$membership = Membership::find($request->input('membership_id'));

			$userMem = new UserMembership;
			$userMem->user_id = $user_id;
			$userMem->shop_id = $auth->shop_id;
			$userMem->membership_id = $request->input('membership_id');
			$userMem->notes = $request->input('notes');
			$userMem->start_date = date('Y-m-d');
			$userMem->end_date = $end_date;
			$userMem->save();

			$msg = 'User successfully added to Membership';
			History::write($auth->shop_id, $user_id, 'user', 'User was added successfully to Membership Level: '. $membership->membership_name,array('id'=>$request->input('membership_id')));
		endif;
		return Response::json(['data' => array('html' =>'saved','redirectTo'=>'/users/view/'. $user_id,'msg'=>$msg)]);

	}

	public function get_membership_record($user_id)
	{
		$auth = $this->auth->user();
		$html = '';

		//get start date of membership
		$usermembership = UserMembership::getMembershipByUserId($auth->shop_id, $user_id);
		if ($usermembership){
			$startDate = $usermembership[0]->start_date;
			$jobsPaid = UserMembership::getMembersPaidServicesFromDate($auth->shop_id, $user_id, $startDate);

			//$membership = Membership::find($usermembership[0]->membership_id);
			$items = MembershipItem::getItems($auth->shop_id, $usermembership[0]->membership_id);

			$tbl = '<br/><table class="table table-striped"><tr><th>Name</th><th>Count to use</th><th>Used</th><th>Count Left</th></tr>';
			foreach ($items as $jobService):
				//$servicesMade = Job::getJobServices($job->id);
				$service = Service::find($jobService->service_id);
				$countUsed = 0;
				
				foreach ($jobsPaid as $job):
					//check if there is a service included in this job
					$serviceCount = Job::checkServiceOnThisJob($job->id, $service->id);
					$countUsed = $serviceCount + $countUsed;
				endforeach;
				$countLeft = $jobService->service_limit - $countUsed;
				$tbl .= '<tr><td>'.$service->servicename.'</td><td>'.$jobService->service_limit.'</td><td>'.$countUsed.'</td><td>'.$countLeft.'</td></tr>';
				$countUsed = 0;

			endforeach;
			$tbl .='</table>';

		} else {
			$startDate = '';
		}
		return Response::json(['data' => array('html' =>$tbl)]);
	}
}
