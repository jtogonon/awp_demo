<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use Response,Asset,Html;

use App\User, App\Models\History;
use App\Helpers\Helper;

class AccountController extends Controller {

	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth');

	}

	/**
	 * Show the users listing page
	 *
	 * @return Response
	 */
	public function index()
	{
		$auth = $this->auth->user();
		if (!$auth->hasRole('superadmin') && !$auth->hasRole('shopadmin')){
			 return redirect('/login');
		}  

		//JS
		$cssArray = ['/components/bootstrap-datepicker/css/datepicker3.css','/plugins/datatables/dataTables.bootstrap.css','/plugins/select2/select2.min.css'
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['back/js/cars.js','/plugins/datatables/jquery.dataTables.min.js','/plugins/datatables/dataTables.bootstrap.js','/plugins/select2/select2.full.min.js'];
		Asset::add($jsArray, 'footer');	

		//get the cars
		$cars = Car::getCarsByShopId($auth->shop_id);
		$data['cars'] = $cars;


		//get customers for dropdown
		$users = User::getCustomersList($auth->shop_id, $auth->id);
		$customerArr =array(''=>'-select-');		



		foreach ($users as $customer =>$c):
			$customerArr[$c['id']] = $c['firstname']. ' '.$c['lastname'];
		endforeach;
		$data['customers'] = $customerArr;


		//get manufacturers for dropdown
		$users = CarManufacturer::getManufacturersList($auth->shop_id);
		$manufacturers =array(''=>'-select-');		

		foreach ($users as $customer =>$c):
			$manufacturers[$c['id']] = $c['name'];
		endforeach;
		$data['manufacturers'] = $manufacturers;

		return view('cars/index', $data);
	}

	public function save(Request $request)
	{
		$auth = $this->auth->user();



		return Response::json(['data' => array('html' =>'saved','redirectTo'=>'/cars','msg'=>$msg)]);
	}

	
	public function delete($id)
	{
		$account = User::find($id);

		if (\Request::ajax()){
			//$res = $car ->delete();

			
		}

	}

	public function upgrade($type=null)
	{
		$auth = $this->auth->user();

		$data = [];

		$jsArray = [];
		Asset::add($jsArray, 'footer');	

		$data['user'] = $auth;
		$data['shop'] = $auth->shop;
		$data['type'] = $type;

		return view('account/upgrade', $data);
	}

	

}
