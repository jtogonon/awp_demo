<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use Response,Asset,Html;

use App\Models\Vendor, App\Models\Service;

class ServicesController extends Controller {

	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth');

	}

	/**
	 * Show the payments listing page
	 *
	 * @return Response
	 */
	public function index()
	{
		$auth = $this->auth->user();
		if (!$auth->hasRole('superadmin') && !$auth->hasRole('shopadmin')){
			 return redirect('/login');
		}  

		//JS
		$cssArray = ['/components/bootstrap-datepicker/css/datepicker3.css','/plugins/datatables/dataTables.bootstrap.css'
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/back/js/services.js','/plugins/datatables/jquery.dataTables.min.js','/plugins/datatables/dataTables.bootstrap.js'];
		Asset::add($jsArray, 'footer');	

		//get the vendors
		$services= Service::getServicesByShopId($auth->shop_id);

		$data['services'] = $services;
		setlocale(LC_MONETARY, 'en_US');

		return view('services/index', $data);
	}

	public function save(Request $request)
	{
		$auth = $this->auth->user();

		$validator = \Validator::make($request->all(), [
            'servicename' => 'required',
            'fee' => 'required',
        ]);

        if ($validator->fails()) {
        	$msg = '';        	
        	foreach($validator->messages()->toArray() as $key):
        		$msg .= $key[0]."\n";
        	endforeach;
        	return Response::json(['data' => array('html' =>'failed','errormsg'=>$msg)]);
        }

		if (\Request::ajax()):
			if ($request->input('id')):
				$serv= Service::find($request->input('id'));
				$msg = 'Service details successfully saved.';
			else:
				$serv = new Service;
				$serv->shop_id = $auth->shop_id;
				$msg = 'New Service successfully saved!';
			endif;
			
			$serv->servicename = $request->input('servicename');
			$serv->fee = $request->input('fee');
			$serv->description = $request->input('description');
			$serv->save();

		endif;

		return Response::json(['data' => array('html' =>'saved','redirectTo'=>'/services','msg'=>$msg)]);
	}

	public function delete($id)
	{
		$serv = Service::find($id);
		$res = $serv->delete();

		if ($res):
			$msg = 'Service successfully deleted!';
			$status = 'deleted';
		else:
			$msg = 'Failed deleting!';
			$status = 'failed';
		endif;

		return Response::json(['data' => array('html' =>$status,'redirectTo'=>'/services','msg'=>$msg)]);

	}

	public function view($id)
	{
		$data = [];

		$jsArray = ['/back/js/services.js'];
		Asset::add($jsArray, 'footer');	

		$auth = $this->auth->user();
		$service = Service::find($id);
		$data['service'] = $service;

		
		return view('services/view', $data);
	}

}
