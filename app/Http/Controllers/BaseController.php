<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use Asset;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\RedirectResponse;
use Auth;



class BaseController extends Controller {
	
	protected $page_title;
	public $layout = 'master';
	public $curController = '';
	public $curAction = '';

	public function __construct(Guard $auth)
	{
		$this->auth = $auth;

		$action = app('request')->route()->getAction();
		$controller = class_basename($action['controller']);
		list($controller, $action) = explode('@', $controller);
		$this->curController = $controller;
		$this->curAction = $action;


		$this->page_title = 'Dashboard';
		$this->show_TopPanel = true;
		$this->title_TopPanel = 'Dashboard';
		$TopPanel_submenu = array('Dashboard'=>'/','Staff'=>'','Customers'=>'');

		View::share('show_TopPanel', $this->show_TopPanel);
		View::share('title_TopPanel',$this->title_TopPanel);
		View::share('TopPanel_button','');
		View::share('TopPanel_submenu',$TopPanel_submenu);


	}
}