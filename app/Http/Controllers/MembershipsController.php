<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use Response,Asset,Html;


use App\User, App\Models\Membership, App\Models\History, App\Models\MembershipItem;
use App\Models\Service;

use App\Helpers\Helper;
class MembershipsController extends Controller {

	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth');

	}

	/**
	 * Show the users listing page
	 *
	 * @return Response
	 */
	public function index()
	{
		$auth = $this->auth->user();
		$data['auth'] = $auth;
		if (!$auth->hasRole('superadmin') && !$auth->hasRole('shopadmin')){
			 return redirect('/login');
		}  

		//JS
		$cssArray = ['/components/bootstrap-datepicker/css/datepicker3.css','/plugins/datatables/dataTables.bootstrap.css'
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/back/js/memberships.js','/plugins/datatables/jquery.dataTables.min.js','/plugins/datatables/dataTables.bootstrap.js'];
		Asset::add($jsArray, 'footer');	

		//get the users
		$memberships = Membership::getMembershipsByShopId($auth->shop_id);
		
		$data['memberships'] = $memberships;

		return view('memberships/index', $data);
	}

	public function save(Request $request)
	{
		$auth = $this->auth->user();

		$validator = \Validator::make($request->all(), [
            'membership_name' => 'required',
            'frequency' => 'required',
            'fee' => 'required',
        ]);

        if ($validator->fails()) {
        	$msg = '';        	
        	foreach($validator->messages()->toArray() as $key):
        		$msg .= $key[0]."\n";
        	endforeach;
        	return Response::json(['data' => array('html' =>'failed','errormsg'=>$msg)]);
        }

		if (\Request::ajax()):
			if ($request->input('id')):
				$mem= Membership::find($request->input('id'));
				$historyMsg = 'Membership details were modified.';
				$logdata = $mem;
				$msg = 'Membership details changed successfully!';
			else:
				$mem = new Membership;
				$mem->shop_id = $auth->shop_id;
				$historyMsg = 'Membership was created.';
				$logdata = $mem;
				$msg = 'New Membership Level successfully saved!';
			endif;
			
			$mem->membership_name = $request->input('membership_name');
			$mem->fee = $request->input('fee');
			$mem->frequency = $request->input('frequency');
			if($mem->save()){
				History::write($auth->shop_id, null, 'membership', $historyMsg ,array('olddata'=>$logdata));
			}

		endif;

		return Response::json(['data' => array('html' =>'saved','redirectTo'=>'/memberships','msg'=>$msg)]);
	}

	public function delete($id)
	{
		//TODO
		//check if there are existing members of this membership level

		$serv = Membership::find($id);
		$res = $serv->delete();

		if ($res):
			$msg = 'Membership Level successfully deleted!';
			$status = 'deleted';
		else:
			$msg = 'Failed deleting!';
			$status = 'failed';
		endif;

		return Response::json(['data' => array('html' =>$status,'redirectTo'=>'/memberships','msg'=>$msg)]);

	}

	public function view($id)
	{
		$data = [];
		$auth = $this->auth->user();
		$membership = Membership::find($id);
		if (!$membership):
			 return redirect('/memberships');
		endif;
		$data['membership'] = $membership;


		$jsArray = ['/back/js/memberships.js','/plugins/sortable/jquery-sortable-min.js'];
		Asset::add($jsArray, 'footer');	

		//get services of this shop
		$services = Service::getServicesByShopId($auth->shop_id);
		$data['services'] = $services;

		$data['membersCount'] = Membership::getMembers($auth->shop_id, $id, false);
		$data['frequencies'] = array('monthly'=>'Monthly','quarterly'=>'Quarterly','annually'=>'Annually');
		return view('memberships/view', $data);
	}

	public function saveItems(Request $req)
	{
		$auth = $this->auth->user();

		$data = [];

		$msg = '';
		if (\Request::ajax()):

			$membership_id = $req->input('mem_id');
			//$service_id = $req->input('service_id');
			$service_limit = $req->input('service_limit');
			$item_id = $req->input('item_id');

			$service_id = intval($req->input('data'));

			if ($service_id > 0):		

				MembershipItem::checkAndSave($auth->shop_id, $membership_id,$service_id, 0,0);
			endif;

			$items = MembershipItem::getItems($auth->shop_id, $membership_id);
			if (count($items) > 0 ) :
				$html = \Form::model(new Membership, array('url' => 'memberships/saveItemsDetails', 'method' => 'post','class'=>'form-horizontal','id'=>'form-memItems'));
				$html .= '<input type="hidden" name="membership_id">';
				$html .= '<h3>Services for this Membership</h3>';
				$html .= '<table class="table table-striped">';
				$html .= "<tr><th>Service Name</th><th width=\"15\">Counts</th><th>Action</th></tr>";
				foreach ($items as $item):
					$html .= '<tr><td>'.$item->getService()->servicename.'</td><td><input name="item_id['.$item->id.']" type="text" value="'.$item->service_limit.'" class="form-control"></td><td><a href="/memberships/deleteitem/'.$item->id.'" class="btn_deleteitem"><i class="fa fa-fw fa-remove"></i></a></td></tr>';
				endforeach;
				$html .= '</table>';

				$html .= '<div class="pull-left"><button id="btn_saveMemItems" type="submit" class="btn btn-primary btn-lg">Save Changes</button></div>';

				$html .= \Form::close() ;
			else:
				$html = '<strong>Drag items here</strong>';
			endif;

			
		endif;

		return Response::json(['data' => array('html' =>$html,'msg'=>$msg)]);
	}//end function


	public function saveItemsDetails(Request $req)
	{
		$auth = $this->auth->user();

		$data = [];

		$msg = '';
		$html = '';
		if (\Request::ajax()):
			$itemIDs = $req->input('item_id');
			foreach ($itemIDs as $id=>$val):
				$membershipItem = MembershipItem::find($id);
				$membershipItem->service_limit = $val;
				$membershipItem->save();
			endforeach;
			$msg = 'ok';
		endif;
		return Response::json(['data' => array('html' =>$html,'msg'=>$msg)]);
	}

	public function deleteitem($id)
	{
		$auth = $this->auth->user();
		$data = [];

		$membershipItem = MembershipItem::find($id);		
		$membershipItem->delete();

		$msg = 'ok';
		return Response::json(['data' => array('html' =>'','msg'=>$msg)]);
	}
}
