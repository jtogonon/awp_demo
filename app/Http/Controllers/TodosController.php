<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use Response,Asset;

use App\Models\Todo;
use App\Helpers\Helper;


class TodosController extends Controller {

	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth');

	}

	

	public function save(Request $request)
	{
		$auth = $this->auth->user();

		if (\Request::ajax()):
			
			$todo = new Todo;
			$todo->shop_id = $auth->shop_id;			
			$todo->user_id = $auth->id;
			$todo->todo = $request->input('newtodo');			
			$todo->save();

		endif;

		$html = '<li>
    <!-- drag handle -->
        <span class="handle">
          <i class="fa fa-ellipsis-v"></i>
          <i class="fa fa-ellipsis-v"></i>
        </span>
    <!-- checkbox -->
    <input type="checkbox" value="'.$todo->id.'" name="" class="checkTodoItem">
    <!-- todo text -->
    <span class="text">'.$todo->todo.'</span>
    <!-- Emphasis label -->
    <small class="label label-danger"><i class="fa fa-clock-o"></i> '.Helper::formatDate($todo->created_at,5).'</small>
    <!-- General tools such as edit or delete-->
    <div class="tools">
      <i class="fa fa-edit"></i>
      <i class="fa fa-trash-o"></i>
    </div>
  </li>';

		return Response::json(['data' => array('html' =>$html,'msg'=>'New Todo Item saved!')]);
	}


	public function check($id)
	{
		$t = Todo::find($id);
		$t->finished = date('Y-m-d H:i:s');
		$t->save();
	}

	public function uncheck($id)
	{
		$t = Todo::find($id);
		$t->finished = '0000-00-00 00:00:00';
		$t->save();
	}
}
