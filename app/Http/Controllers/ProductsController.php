<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use Response,Asset,Html;

use App\Models\Vendor, App\Models\Product, App\Models\ProductManufacturer;

class ProductsController extends Controller {

	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth');

	}

	/**
	 * Show the payments listing page
	 *
	 * @return Response
	 */
	public function index()
	{
		$auth = $this->auth->user();
		if (!$auth->hasRole('superadmin') && !$auth->hasRole('shopadmin')){
			 return redirect('/login');
		}  

		//JS
		$cssArray = ['/components/bootstrap-datepicker/css/datepicker3.css','/plugins/datatables/dataTables.bootstrap.css'
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/back/js/products.js','/plugins/datatables/jquery.dataTables.min.js','/plugins/datatables/dataTables.bootstrap.js'];
		Asset::add($jsArray, 'footer');	

		//get the vendors
		$products= Product::getProductsByShopId($auth->shop_id);
		$data['products'] = $products;

		//get manufacturers for dropdown
		$manufacturers = ProductManufacturer::lists('name','id');
		$data['manufacturers'] = $manufacturers;


		return view('products/index', $data);
	}

	public function save(Request $request)
	{
		$auth = $this->auth->user();

		$validator = \Validator::make($request->all(), [
            'product_name' => 'required',
            'unit_price' => 'required',
        ]);

        if ($validator->fails()) {
        	$msg = '';        	
        	foreach($validator->messages()->toArray() as $key):
        		$msg .= $key[0]."\n";
        	endforeach;
        	return Response::json(['data' => array('html' =>'failed','errormsg'=>$msg)]);
        }

		if (\Request::ajax()):
			if ($request->input('id')):
				$product= Product::find($request->input('id'));
			
			else:
				$product = new Product;
				$product->shop_id = $auth->shop_id;
			endif;
			
			$product->product_name = $request->input('product_name');
			$product->sku = $request->input('sku');
			$product->in_stock = $request->input('in_stock');
			$product->unit_price = $request->input('unit_price');
			$product->manufacturer_id = $request->input('manufacturer_id');
			$product->save();

		endif;

		return Response::json(['data' => array('html' =>'saved','redirectTo'=>'/products','msg'=>'New Product successfully saved!')]);
	}

	public function delete($id)
	{
		$man = Product::find($id);
		$res = $man->delete();

		if ($res):
			$msg = 'Product successfully deleted!';
			$status = 'deleted';
		else:
			$msg = 'Failed deleting!';
			$status = 'failed';
		endif;

		return Response::json(['data' => array('html' =>$status,'redirectTo'=>'/products','msg'=>$msg)]);

	}

}
