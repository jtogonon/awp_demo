<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use Response,Asset,Html;

use App\User, App\Models\Invoice, App\Models\CarManufacturer;
use App\Models\Shop;
use App\Helpers\Helper;

class InvoicesController extends Controller {

	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth');

	}

	/**
	 * Show the users listing page
	 *
	 * @return Response
	 */
	public function index()
	{
		$auth = $this->auth->user();
		if (!$auth->hasRole('superadmin') && !$auth->hasRole('shopadmin')){
			 return redirect('/login');
		}  

		//JS
		$cssArray = ['/components/bootstrap-datepicker/css/datepicker3.css','/plugins/datatables/dataTables.bootstrap.css'
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/back/js/invoices.js','/plugins/datatables/jquery.dataTables.min.js','/plugins/datatables/dataTables.bootstrap.js'];
		Asset::add($jsArray, 'footer');	

		//get the cars
		$invoices = Invoice::geInvoicesByShopId($auth->shop_id);

		$data['invoices'] = $invoices;

		return view('invoices/index', $data);
	}

	public function save(Request $request)
	{
		$auth = $this->auth->user();

		if (\Request::ajax()):
			if ($request->input('id')):
				$invoice= Invoice::find($request->input('id'));
			
			else:
				$invoice = new Invoice;
				$invoice->shop_id = $auth->shop_id;
			endif;
			/*TODO
			add details of invoice
			*/		
			$invoice->amount = $request->input('amount');
			$invoice->save();

		endif;

		return Response::json(['data' => array('html' =>'saved',)]);
	}
	
	public function create()
	{
		$auth = $this->auth->user();

		//JS
		$cssArray = ['/plugins/datepicker/datepicker3.css','/plugins/select2/select2.min.css','/plugins/timepicker/bootstrap-timepicker.min.css'
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/back/js/invoices.js','/plugins/datepicker/bootstrap-datepicker.js'];
		Asset::add($jsArray, 'footer');	

		$data = [];

		//get customers for dropdown
		$users = User::getCustomersList($auth->shop_id,$auth->id);
		$customerArr =array(''=>'Select a Customer');		

		foreach ($users as $customer =>$c):
			$customerArr[$c['id']] = $c['firstname']. ' '.$c['lastname'];
		endforeach;
		$data['customers'] = $customerArr;

		$data['shop'] = Shop::find($auth->shop_id);

		$invoicenumberVal = Invoice::getShopLastInvoiceNumber($auth->shop_id);
		$data['invoicenumber'] = $invoicenumberVal;
		return view('invoices/create', $data);
	}


	public function view($id)
	{
		$data = [];
		$auth = $this->auth->user();
		$data['auth'] = $auth;

		$invoice = Invoice::find($id);
		if (empty($invoice)){
			 return redirect('/invoices');
		}

		$data['invoice'] = $invoice;

		$items = Invoice::getItems($id);

		$data['invoice_items'] = $items;

		return view('invoices/view', $data);
	}

	public function printview($id)
	{
		$data = [];
		$auth = $this->auth->user();
		$data['auth'] = $auth;

		$invoice = Invoice::find($id);
		$data['invoice'] = $invoice;

		$items = Invoice::getItems($id);

		$data['invoice_items'] = $items;

		return view('invoices/print', $data);
	}

	public function getInvoicesByUserId($user_id)
	{
		$auth = $this->auth->user();
		$invoices = Invoice::geInvoicesByUserId($user_id, 'Outstanding');

		if (count($invoices)> 0 ):

		$items ='<table class="table table-striped table-bordered table-hover dataTables">
            <thead>
                <tr>
                    <th></th>                    
         			<th>Invoice Number</th>
                    <th>Invoice Date</th>
                    <th>Due Date</th>
                    <th>Total</th>
                    <th>Balance Due</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>';
            foreach ($invoices as $invoice) {
            	$items .= '
            	 <tr>
                    <td><input type="radio" data="'.$invoice->total.'" value="'.$invoice->invoice_id.'" name="invoice_id" class="btn_apply_payment"> Apply Payment</td>                    
                    <td><a original-title="View Invoice" class="tipS" href="/invoices/view/'.$invoice->invoice_id.'">'.$invoice->invoicenumber.'</a></td>
                    <td align="center">'.Helper::formatDate($invoice->invoice_date,5).'</td>
                    <td align="center">'.Helper::formatDate($invoice->due_date,5).'</td>
                    <td align="center">'.$auth->getShopCurrency()->symbol_html.$invoice->total.'</td>
                    <td align="center">'.$auth->getShopCurrency()->symbol_html.$invoice->balance_due.'</td>
                   <td align="center">
                        <span class="label label-warning">Outstanding</span>
                    </td>
                </tr>';
            }
               
        $items .= '    </tbody>
        </table>';

		$returnHTML = $items;

		else:
			$returnHTML = 'noresult';
		endif;
		return response()->json(array('success' => true, 'html'=>$returnHTML));
	}

	public function delete($id)
	{
		$invoice = Invoice::find($id);

		if (\Request::ajax()){
			$res = $invoice->delete();

			if ($res):
				$msg = 'Invoice successfully deleted!';
				$status = 'deleted';
			else:
				$msg = 'Failed deleting!';
				$status = 'failed';
			endif;

			return Response::json(['data' => array('html' =>$status,'redirectTo'=>'/invoices','msg'=>$msg)]);
		}

	}
}
