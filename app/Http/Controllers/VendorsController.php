<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use Response,Asset,Html;

use App\Models\Vendor, App\Models\Expense;

class VendorsController extends Controller {

	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth');

	}

	/**
	 * Show the payments listing page
	 *
	 * @return Response
	 */
	public function index()
	{
		$auth = $this->auth->user();
		if (!$auth->hasRole('superadmin') && !$auth->hasRole('shopadmin')){
			 return redirect('/login');
		}  

		//JS
		$cssArray = ['/components/bootstrap-datepicker/css/datepicker3.css','/plugins/datatables/dataTables.bootstrap.css'
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/back/js/cars.js','/plugins/datatables/jquery.dataTables.min.js','/plugins/datatables/dataTables.bootstrap.js'];
		Asset::add($jsArray, 'footer');	

		//get the vendors
		$vendors= Vendor::getVendorsByShopId($auth->shop_id);

		$data['vendors'] = $vendors;

		return view('vendors/index', $data);
	}

	public function save(Request $request)
	{
		$auth = $this->auth->user();

		$validator = \Validator::make($request->all(), [
            'name' => 'required',           
            
        ]);

        if ($validator->fails()) {
        	$msg = '';        	
        	foreach($validator->messages()->toArray() as $key):
        		$msg .= $key[0]."\n";
        	endforeach;
        	return Response::json(['data' => array('html' =>'failed','errormsg'=>$msg)]);
        }

		if (\Request::ajax()):
			if ($request->input('id')):
				$vendor = Vendor::find($request->input('id'));
			
			else:
				$vendor = new Vendor;
				$vendor->shop_id = $auth->shop_id;
			endif;
			
			$vendor->name = $request->input('name');
			$vendor->phone = $request->input('phone');
			$vendor->email = $request->input('email');
			$vendor->contact_name = $request->input('contact_name');
			$vendor->save();

		endif;

		return Response::json(['data' => array('html' =>'saved','redirectTo'=>'/vendors','msg'=>'New Vendor successfully saved!')]);
	}

	public function delete($id)
	{
		$v = Vendor::find($id);
		$res = $v->delete();

		if ($res):
			$msg = 'Vendor successfully deleted!';
			$status = 'deleted';
		else:
			$msg = 'Failed deleting!';
			$status = 'failed';
		endif;

		return Response::json(['data' => array('html' =>$status,'redirectTo'=>'/vendors','msg'=>$msg)]);

	}
}
