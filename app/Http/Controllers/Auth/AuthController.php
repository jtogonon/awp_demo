<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
/*use Illuminate\Contracts\Auth\Guard;*/
/*use Illuminate\Contracts\Auth\Registrar;*/
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;

use Mail;
use App\Models\Shop;
use App\User;

use Newsletter;
use Log, Auth;
use Validator;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	//public function __construct(Guard $auth, Registrar $registrar)
	public function __construct()
	{
		//$this->auth = $auth;
		//$this->registrar = $registrar;
		$this->redirectPath = '/dashboard';

		$this->middleware('guest', ['except' => 'getLogout']);

		$data['title'] = 'AutoWorksPro.com';
		$data['metaDesc'] = '';
		$data['metaTitle'] = '';


		view()->share('title','AutoWorksPro.com');
		view()->share('metaDesc','AutoWorksPro is a quick, easy to use, stress-free and organized auto shop management software to make your business grow. Web-based tracking of your new registrations, customers and scheduling appointments. Create invoices, track your expenses, and calculate your profits. AutoWorksPro can help you save time and money, and to run your auto shop, detailing and car wash business efficiently.');
		view()->share('metaTitle','AutoWorksPro - cloud-based car wash and auto detailers software management system');
	}

	public function postRegister(Request $request)
    {
		$data['metaTitle'] = '';

        $validator = $this->validator($request->all());

		if ($validator->fails())
		{
			$this->throwValidationException(
				$request, $validator
			);
		}


		//send mail
		Mail::send('emails.newregistrationnotify', array('name'=>$request->input('name'),'email'=>$request->input('email')), function($message)
	    {
	        $message->from( 'noreply@autoworkspro.com', 'AutoWorksPro' );

	        $message->to('jetogonon@gmail.com', 'Support')->subject( 'New User Registered');
	    });

	    //send welcome email
	    $datavars = array('publicUrl'=>url('/'),
	    			'shop_logo'=>url('/'),
	    			'username'=>$request->input('email'),	    			
	    			'main_sender'=>'autoworkspro'
	    			);
	   	$recipient = array(
	   		'email'=>$request->input('email'),
	   		'name'=>$request->input('name'));
		/*Mail::send('emails.signup', $datavars, function($msg) use ($recipient) 
	    {
	        $msg->from( 'support@autoworkspro.com', 'Support');

	        $msg->to($recipient['email'], $recipient['name'])->subject( 'Welcome to AutoWorksPro');
	        //$msg->to('jetogonon@gmail.com', \Input::get('name'))->subject( 'Welcome to AutoWorksPro');
	    });*/

		Auth::login($this->create($request->all()));

		//subscribe to mailchimp list
		$res = Newsletter::subscribe($recipient['email'], ['firstName'=>$recipient['name']], 'AWPList', ['status'=>'pending']);
		Log::info('MailChimp subscribed: '.$recipient['email']);

		$newuser = Auth::user();
		

		$shop = new Shop;
		$shop->name = $request->input('shop_name');
		$shop->user_id = $newuser->id;
		$shop->status_id = 2;

		$todate=strtotime(date('Y-m-d')); 		
		$shop->expiration_date = date('Y-m-d',strtotime('+1 month',$todate));
		$shop->save();

		$names = explode(' ',$request->input('name'));
		$user = User::find($newuser->id);
		$user->shop_id = $shop->id;
		$user->firstname = $names[0];
		if (isset($names[1])){
			$user->lastname = $names[1];
		}
		$user->save();

		

		return redirect($this->redirectPath());
    }

    public function postLogin(Request $request)
    {
    	$data['metaTitle'] = '';
    	
    	$this->validate($request, [
			'email' => 'required|email', 'password' => 'required',
		]);

		$credentials = $request->only('email', 'password');

		
		if (Auth::attempt($credentials, $request->has('remember')))
		{
			//set timezone
			$user =  Auth::user();
	    	if ($user->shop):
	    		if ($user->shop->timezone !=''):
	    			\Session::put('timezone', $user->shop->timezone);
				endif;
			endif;

			return redirect()->intended($this->redirectPath());
		}

		

		return redirect('/auth/login')
					->withInput($request->only('email', 'remember'))
					->withErrors([
						'email' => $this->getFailedLoginMessage(),
					]);
    }

    /**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
		$user =  User::create([
			'name' => $data['name'],
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
		]);
		$newuser = $user;

		$user= $user->attachRole(2); // parameter can be an Role object, array, or id

		return $newuser;
	}

}
