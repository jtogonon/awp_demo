<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use Response,Asset,Html;

use App\User, App\Models\Invoice, App\Models\Expense, App\Models\ExpenseCategory;
use App\Models\Vendor, App\Helpers\Helper;

class ExpensesController extends Controller {

	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth');

	}

	/**
	 * Show the payments listing page
	 *
	 * @return Response
	 */
	public function index()
	{
		$auth = $this->auth->user();
		if (!$auth->hasRole('superadmin') && !$auth->hasRole('shopadmin')){
			 return redirect('/login');
		}  

		//JS
		$cssArray = ['/plugins/datatables/dataTables.bootstrap.css','/plugins/datepicker/datepicker3.css'
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/back/js/expenses.js','/plugins/datatables/jquery.dataTables.min.js','/plugins/datatables/dataTables.bootstrap.js','/plugins/datepicker/bootstrap-datepicker.js'];
		Asset::add($jsArray, 'footer');	

		//get the payments
		$expenses = Expense::geExpensesByShopId($auth->shop_id);

		$data['expenses'] = $expenses;

		//get categories for dropdown
		$categories = ExpenseCategory::getLists($auth->shop_id);
		$expCategories =array(''=>'-select-');
		
		foreach ($categories as $categ =>$cat):			
			$expCategories[$cat['id']] = $cat['name'];
		endforeach;
		$data['expCategories'] = $expCategories;

		//get vendors for dropdown
		$vends = Vendor::getLists($auth->shop_id);
		$vendors =array(''=>'-select-');		

		foreach ($vends as $vendor =>$v):			
			$vendors[$v['id']] = $v['name'];
		endforeach;
		$data['vendors'] = $vendors;

		return view('expenses/index', $data);
	}

	public function save(Request $request)
	{
		$auth = $this->auth->user();

		$validator = \Validator::make($request->all(), [
            'name' => 'required',
            'expense_category_id' => 'required',
            'amount' => 'required',
            'expense_date' => 'required',
        ]);

        if ($validator->fails()) {
        	$msg = '';        	
        	foreach($validator->messages()->toArray() as $key):
        		$msg .= $key[0]."\n";
        	endforeach;
        	return Response::json(['data' => array('html' =>'failed','errormsg'=>$msg)]);
        }

		if (\Request::ajax()):
			if ($request->input('id')):
				$exp= Expense::find($request->input('id'));
			
			else:
				$exp = new Expense;
				$exp->shop_id = $auth->shop_id;
			endif;
			$exp->expense_category_id = $request->input('expense_category_id');
			$exp->vendor_id = $request->input('vendor_id');
			$exp->name = $request->input('name');
			$exp->amount = $request->input('amount');
			$exp->description = $request->input('description');
			$exp->expense_date = Helper::formatDate($request->input('expense_date'),1);
			$exp->save();

		endif;

		return Response::json(['data' => array('html' =>'saved','redirectTo'=>'/expenses','msg'=>'New Expense successfully saved!')]);
	}

	public function delete($id)
	{
		$exp = Expense::find($id);
		$res = $exp->delete();

		if ($res):
			$msg = 'Expense successfully deleted!';
			$status = 'deleted';
		else:
			$msg = 'Failed deleting!';
			$status = 'failed';
		endif;

		return Response::json(['data' => array('html' =>$status,'redirectTo'=>'/expenses','msg'=>$msg)]);

	}
	

}
