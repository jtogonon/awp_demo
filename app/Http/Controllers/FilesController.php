<?php namespace App\Http\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Auth;
use Entrust, Illuminate\Support\Facades\Session;
use Response,Asset,Html;

use App\Models\Package, App\Models\File;
use App\User,App\Models\History;

class FilesController extends Controller {

	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth');

	}

	/**
	 * Show the payments listing page
	 *
	 * @return Response
	 */
	public function index()
	{
		$auth = $this->auth->user();
		if (!$auth->hasRole('superadmin') && !$auth->hasRole('shopadmin')){
			 return redirect('/login');
		}  

		//JS
		$cssArray = ['/components/bootstrap-datepicker/css/datepicker3.css','/plugins/datatables/dataTables.bootstrap.css'
		];
		Asset::add($cssArray, 'headerCss');

		$jsArray = ['/back/js/cars.js','/plugins/datatables/jquery.dataTables.min.js','/plugins/datatables/dataTables.bootstrap.js'];
		Asset::add($jsArray, 'footer');	

		//get the packages
		$packages= Package::gePackagesByShopId($auth->shop_id);

		$data['packages'] = $packages;

		return view('packages/index', $data);
	}

	public function upload(Request $request)
	{
		$auth = $this->auth->user();

		if (\Request::ajax()):
			 $file = array('image' => \Input::file('profilepic'));
				  // setting up rules
				  $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
				  // doing the validation, passing post data, rules and the messages
				  $validator = \Validator::make($file, $rules);
				  if ($validator->fails()) {
				    // send back to the page with the input data and errors
				    return \Redirect::to('upload')->withInput()->withErrors($validator);
				  }
				  else {
				    // checking file is valid.
				    if (\Input::file('profilepic')->isValid()) {
				    	$image = \Input::file('profilepic');
				      	$destinationPath = '/uploads/profile/'; // upload path
				      	$extension = \Input::file('profilepic')->getClientOriginalExtension(); // getting image extension
				      	$fileName = $auth->shop_id .'_'.rand(11111,99999).'.'.$extension; // renameing image
				      	//\Input::file('profilepic')->move($destinationPath, $fileName); // uploading file to given path
				       //$img = \Image::make($destinationPath .'/'. $fileName)->resize(128, 128);
				      	 $path = public_path($destinationPath . $fileName);
				       \Image::make($image->getRealPath())->resize(128, 128)->save($path);

				       //TODO save image data as profile pic
				       $user = User::find($auth->id);
				       $user->profile_pic = $fileName;
				       $user->save();
				      	
				      	// sending back with message
					    \Session::flash('success', 'Upload successfully'); 
				      //return \Redirect::to('upload');
				    }
				    else {
				      // sending back with error message.
				      Session::flash('error', 'uploaded file is not valid');
				      //return \Redirect::to('upload');
				    }
				  }

		endif;

		return \Response::json(['data' => array('html' =>'saved','redirectTo'=>'/my_account')]);
	}

	public function uploadjobpics(Request $request)
	{
		$auth = $this->auth->user();
		$job_id = $request->input('job_id');
		$car_id = $request->input('car_id');

		$resfiles = [];
		$currentImgs = [];
		$uploadedfiles = [];

		$car_id = ($_GET['car_id']) ? $_GET['car_id'] : $car_id;
		$job_id = ($_GET['job_id']) ? $_GET['job_id'] : $job_id;

		

		if (\Request::ajax()):
			//pull images of this car and job
			$images = File::getImagesByJobId($job_id, $car_id, $auth->shop_id);
			
			foreach($images as $img):
				$fileURL = url('/').'/uploads/images/'.$img->file_name;
				$thumbfileURL= url('/').'/uploads/images/'.$img->file_name_thumb;

				$currentImgs[] = array('url'=>$fileURL,'thumbnailUrl'=>$thumbfileURL,'name'=>$img->title,'size'=>$img->original_filesize,'type'=>$img->file_type);
			endforeach;

	



			$files = \Input::file('images');
			$file_count = count($files);

			$uploadcount = 0;
			if (\Input::file('images')):
    			foreach($files as $file) {

					
					$rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
     				$validator = \Validator::make(array('file'=> $file), $rules);

					if($validator->passes()){
					   
				    	$image = $file;
				    	$file_name = $file->getClientOriginalName();
				      	$destinationPath = base_path().'/public/uploads/images/'; // upload path
				      	$extension = $file->getClientOriginalExtension(); // getting image extension
				      	$randName = $auth->shop_id .'_'.rand(11111111,99999999); // renameing image				      
				      	$fileName = $randName .'.'.$extension;

				        
				      	$thumbNail = $randName .'_th.'.$extension;
				      	$resizedName = $randName .'_r.'.$extension;
				      	$newImage = \Image::make($file->getRealPath())
						// original
						->save($destinationPath.$fileName)
						// thumbnail
						->fit('120', '120')
						->save($destinationPath.$thumbNail);
						$fileURL = url('/').'/uploads/images/'.$fileName;

						list($width, $height) = getimagesize($file->getRealPath());
						if ($width > 2000):
							// resize
							$curImage = \Image::make(base_path().'/public/uploads/images/'.$fileName);
							$curImage->fit('1632', '1224')->save($destinationPath.$resizedName);
							$fileURL = url('/').'/uploads/images/'.$resizedName;
						endif;
						

				        
				      	/*$path = public_path($destinationPath .$thumbNail);
				        $newImage = \Image::make($image->getRealPath())->resize(128, 128)->save($path);//make a thumbnail*/
				       	$size =  $newImage->filesize();
				       	$filetype = $newImage->mime();

				       //TODO save image data as profile pic
				       $newfile = new File;
				       $newfile->file_type = 'image';
				       $newfile->title = $file_name;
				       $newfile->shop_id = $auth->shop_id;
				       $newfile->user_id = $auth->id;
				       $newfile->car_id = $car_id;
				       $newfile->job_id = $job_id;
				       $newfile->file_name = $fileName;
				       $newfile->file_name_thumb = $thumbNail;
				       $newfile->description = '';
				        $newfile->original_filesize = $size ;
				       $newfile->save();

				       $thumbfileURL = url('/').'/uploads/images/'.$thumbNail;
				       
				      	
				      	$uploadcount ++;
					    $uploadedfiles[] = array('url'=>"$fileURL",'thumbnailUrl'=>$thumbfileURL,'name'=>$fileName,'size'=>$size,'type'=>$filetype);
					    History::write($auth->shop_id, $auth->id, 'job', "Uploaded image for Car ID <a href=\"/cars/view/".$car_id."\">".$car_id."</a>", $uploadedfiles, $job_id, $car_id);
					}
				}//endforeach file

				if($uploadcount == $file_count){

					
				    Session::flash('success', 'Upload successfully'); 
				     
				} 
				  else {
				      return \Redirect::to('upload')->withInput()->withErrors($validator);
				}

				

				
			endif;
		endif;

		$resfiles = array_merge($uploadedfiles,$currentImgs);

		$response = response()->json(['files'=>$resfiles],200, [], JSON_UNESCAPED_UNICODE);
		$response->header('Content-Type', 'application/json');
		$response->header('charset', 'utf-8');

		return $response;
	}

	public function jobimages()
	{
		echo '1';
	}


	public function delete($id)
	{
		$pak = Package::find($id);
		$res = $pak->delete();

		if ($res):
			$msg = 'Package successfully deleted!';
			$status = 'deleted';
		else:
			$msg = 'Failed deleting!';
			$status = 'failed';
		endif;

		return Response::json(['data' => array('html' =>$status,'redirectTo'=>'/packages','msg'=>$msg)]);

	}

	

}
